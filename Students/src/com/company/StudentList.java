package com.company;

public class StudentList {
    //Attributes
    private Student[] students;

    //Constructors
    public StudentList() {
        this.students = new Student[0];
    }

    public StudentList(Student[] students) {
        if (students == null) {
            throw new IllegalArgumentException("Students array should not be null");
        }
        this.students = copyStudentsFromArray(students, students.length);
    }

    //Methods, Business methods.
    public void sortByNumberAsc() {
        Student tempStudent = null;
        //Sort the students in ascending order using two for loops
        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.students.length; index2++) {
                if (this.students[index1].compareToByNumber(this.students[index2]) > 0) {
                    //swap elements if not in order.
                    tempStudent = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempStudent;
                }

            }
        }

    }

    public void sortByGradeDesc() {
        Student tempGrade = null;
        //Sort the students in ascending order using two for loops
        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.students.length; index2++) {
                if (this.students[index1].compareToByNumber(this.students[index2]) < 0) {
                    //swap elements if not in order.
                    tempGrade = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempGrade;
                }

            }
        }
    }

    public Student[] toArray() {
        return this.copyStudentsFromArray(this.students, this.students.length);
    }

    private Student[] copyStudentsFromArray(Student[] students, int size) {
        Student[] copyArray = new Student[size];
        for (int count = 0; count < size && count < students.length; count++) {
            copyArray[count] = students[count];
        }
        return copyArray;
    }
}
