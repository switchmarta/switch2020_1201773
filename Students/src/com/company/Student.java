package com.company;

public class Student {
    //Attributes
    private int number;
    private String name;
    private int grade = -1;

    //Constructor
    public Student(int a, String b) {
        if (!isNumberValid(a)) {
            throw new IllegalArgumentException("Invalid number!");
        }
        this.number = a;
        if (!isNameValid(b)) {
            throw new IllegalArgumentException("Invalid name!");
        }
        this.name = b;
        this.grade = -1;
    }


    //Business Methods
    private boolean isNameValid(String name) {
        if (name == null || name.length() < 5) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isNumberValid(int number) {
        if (number < 1000000 || number > 9999999) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isEvaluated() {
        if (this.grade < 0) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isGradeValid(int grade) {
        if (grade < 0 || grade > 20) {
            return false;
        } else {
            return true;
        }
    }

    public void doEvaluation(int grade) {
        if (isEvaluated()) {
            throw new IllegalArgumentException("Already evaluated!");
        } else if (!isGradeValid(grade)) {
            throw new IllegalArgumentException("Interval from 0 to 20.");
        } else {
            this.grade = grade;
        }
    }

    public void setGrade(int grade) {
        if (!isGradeValid(grade)) {
            throw new IllegalArgumentException("Interval from 0 to 20.");
        } else {
            this.grade = grade;
        }
    }

    public int compareToByNumber(Student other) {
        if (this.number < other.number) {
            return -1;//less than other
        } else if (this.number > other.number) {
            return 1;//greater than other
        }
        return 0;//equal
    }
}
