import com.company.Student;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {
    @Test
    public void numberGivenIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(19, "Marcus");
        });
    }

    @Test
    public void nameGivenIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(1900123, "Mar");
        });
    }

    @Test
    public void nameGivenIsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(1900123, " ");
        });
    }

    @Test
    public void createValidStudent() {
        Student student = new Student(1234567, "Marta");
        assertNotNull(student);
    }

    @Test
    public void doEvaluationInvalid() {
        Student student = new Student(1900123, "Marcus");
        assertThrows(IllegalArgumentException.class, () -> {
            student.doEvaluation(22);
        });
    }

    @Test
    public void doEvaluationWithGradeAlreadyDefined() {
        Student student1 = new Student(1900123, "Marcus");
        student1.setGrade(15);
        assertThrows(IllegalArgumentException.class, () -> {
            student1.doEvaluation(17);
        });

    }
}