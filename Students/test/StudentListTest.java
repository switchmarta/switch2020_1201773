import com.company.Student;
import com.company.StudentList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StudentListTest {
@Test
    public void createStudentListEmpty() {
    //Arrange
    StudentList stList = new StudentList(); //list is empty!
    int expected = 0;
    Student[]result = {};
    //Act
    result = stList.toArray();
    //Assert
    assertEquals(expected,result.length); //check array
}

@Test
public void createStudentListWithSomeElements() {
    //Arrange
    Student student1 = new Student(1234567, "Marta");
    Student student2 = new Student(9112900, "Paulo");
    Student student3 = new Student(7652356, "Beatriz");
    Student[] students = {student1,student2,student3}; //some order
    Student[] expected = {student1,student2,student3}; // copy list of students
    //Act
    StudentList stList = new StudentList(students);
    students[2] = student1; // change the original array
    Student[] result = stList.toArray();
    //Assert
    assertArrayEquals(expected,result); //check students are the same
    assertNotSame(students,result); //check if its the original array


}

@Test
public void sortByGradeDescWithSeveralElementsIncorrectlyOrdered() {
    //Arrange
    Student student1 = new Student(1234567, "Marta");
    student1.doEvaluation(12);
    Student student2 = new Student(9112900, "Paulo");
    student2.doEvaluation(17);
    Student student3 = new Student(7652356, "Beatriz");
    student3.doEvaluation(15);

    Student[] students = {student1,student2,student3}; //Unordered
    StudentList stList = new StudentList(students);
    Student[] expected = {student2,student3,student1}; //Ordered
    //Act
    stList.sortByGradeDesc();
    Student[] result = stList.toArray();
    //Assert
    assertArrayEquals(expected,result);

}
}
