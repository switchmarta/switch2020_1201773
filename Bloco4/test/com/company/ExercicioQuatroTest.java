package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ExercicioQuatroTest {
    //Testes exercício 4
    @Test
    public void testeObterArrayPares() {
        //Arrange
        int[] num = {4, 33, 5, 40, 8, 1};
        int[] expected = {4, 40, 8};
        //Act
        int[] result = ExercicioQuatro.obterParesImparDeArrayDado(num, 0);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeObterArraySoPares() {
        //Arrange
        int[] num = {4, 8, 2, 40, 8, 2};
        int[] expected = {4, 8, 2, 40, 8, 2};
        //Act
        int[] result = ExercicioQuatro.obterParesImparDeArrayDado(num, 0);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeObterArrayImpares() {
        //Arrange
        int[] num = {1, 2, 5, 6, 9, 11, 16, 33, 26};
        int[] expected = {1, 5, 9, 11, 33};
        //Act
        int[] result = ExercicioQuatro.obterParesImparDeArrayDado(num, 1);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeObterArraySoImpares() {
        //Arrange
        int[] num = {1, 5, 5, 11, 9, 13, 19, 33, 51};
        int[] expected = {1, 5, 5, 11, 9, 13, 19, 33, 51};
        //Act
        int[] result = ExercicioQuatro.obterParesImparDeArrayDado(num, 1);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testeObterArraySoUmNum() {
        //Arrange
        int[] num = {1};
        int[] expected = {};
        //Act
        int[] result = ExercicioQuatro.obterParesImparDeArrayDado(num, 0);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testeObterArrayNulo() {
        //Arrange
        int[] num = null;
        //Act
        int[] result = ExercicioQuatro.obterParesImparDeArrayDado(num, 0);
        //Assert
        assertNull(result);
    }
}
