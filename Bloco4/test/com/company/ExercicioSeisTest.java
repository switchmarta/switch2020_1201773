package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ExercicioSeisTest {
    //Testes exercicio 6
    @Test
    public void testeObterArrayQuatroNumeros() {
        //Arrange
        int [] num = {3,4,6,8,1,23,89,12};
        int n = 4;
        int [] expected = {3,4,6,8};
        int [] result = {};
        //Act
        result = ExercicioSeis.obterApenasNElementosVetor(num, n);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testeObterArrayDezNumeros() {
        //Arrange
        int [] num = {3,4,6,8,1,23,89,12,101,12,67,87};
        int n = 10;
        int [] expected = {3,4,6,8,1,23,89,12,101,12};
        int [] result = {};
        //Act
        result = ExercicioSeis.obterApenasNElementosVetor(num, n);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testeObterArrayNulo() {
        //Arrange
        int [] num = null;
        int n = 1;
        int [] result = {};
        //Act
        result = ExercicioSeis.obterApenasNElementosVetor(num, n);
        //Assert
        assertNull(result);
    }
}
