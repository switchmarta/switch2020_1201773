package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ExercicioTresTest {
    //Testes exercício 3
    @Test
    public void testSomaUm() {
        //Arrange
        int[] num = {1, 4, 5, 5};
        int expected = 15;
        //Act
        int result = ExercicioTres.somaElementosArray(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSomaDois() {
        //Arrange
        int[] num = {4, 40, -5, 33};
        int expected = 72;
        //Act
        int result = ExercicioTres.somaElementosArray(num);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testSomaDeUmElemento() {
        //Arrange
        int[] num = {4};
        int expected = 4;
        //Act
        int result = ExercicioTres.somaElementosArray(num);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testSomaDeElementoNegativoEZero() {
        //Arrange
        int[] num = {4, -5, 0, 0};
        int expected = -1;
        //Act
        int result = ExercicioTres.somaElementosArray(num);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testSomaArrayNulo() {
        //Arrange
        int[] num = null;
        int expected = -1;
        int result = 0;
        //Act
        result = ExercicioTres.somaElementosArray(num);
        //Assert
        assertEquals(expected, result);
    }
}
