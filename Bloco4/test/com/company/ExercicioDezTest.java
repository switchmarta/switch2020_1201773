package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class ExercicioDezTest {

    //Alínea A
    @Test
    public void testMenorValorUm() {
        //Arrange
        int[] num = {11, 1, 6, 8, 12, 5, 3};
        int expected = 1;
        //Act
        int result = ExercicioDez.obterMenorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMenorValorComNumerosNegativos() {
        //Arrange
        int[] num = {-11, 1, 6, -8, 12, -5, 0};
        int expected = -11;
        //Act
        int result = ExercicioDez.obterMenorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMenorValorComApenasUmNumeroNoVetor() {
        //Arrange
        int[] num = {1}; // eu quero que ele retorne à mesma o valor que tem lá, mesmo que só seja um.
        int expected = 1;
        //Act
        int result = ExercicioDez.obterMenorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMenorValorComZeros() {
        //Arrange
        int[] num = {0, 0, 1};
        int expected = 0;
        //Act
        int result = ExercicioDez.obterMenorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMenorValorComZero() {
        //Arrange
        int[] num = {0}; // eu quero que ele retorne à mesma o valor que tem lá, mesmo que só seja um.
        int expected = 0;
        //Act
        int result = ExercicioDez.obterMenorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    // Testes alínea B
    @Test
    public void testMaiorValorDoze() {
        //Arrange
        int[] num = {11, 1, 6, 8, 12, 5, 3};
        int expected = 12;
        //Act
        int result = ExercicioDez.obterMaiorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMaiorValorUm() {
        //Arrange
        int[] num = {0, 0, 1};
        int expected = 1;
        //Act
        int result = ExercicioDez.obterMaiorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMaiorValorMesmoComApenasUmValor() {
        //Arrange
        int[] num = {0};
        int expected = 0;
        //Act
        int result = ExercicioDez.obterMaiorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMaiorValorComVetorNumerosNegativos() {
        //Arrange
        int[] num = {-1, -8, -32, -4, -80};
        int expected = -1;
        //Act
        int result = ExercicioDez.obterMaiorValor(num);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 10 alínea C
    @Test
    public void testMediaValoresUm() {
        //Arrange
        int[] num = {2, 3, 4};
        double expected = 3.0;
        //Act
        double result = ExercicioDez.obterValorMedioElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaElementosNegativos() {
        //Arrange
        int[] num = {-1, -8, -32, -4, -80};
        double expected = -25.0;
        //Act
        double result = ExercicioDez.obterValorMedioElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaElementoZero() {
        //Arrange
        int[] num = {0};
        double expected = 0;
        //Act
        double result = ExercicioDez.obterValorMedioElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaElementosNegativosPositivos() {
        //Arrange
        int[] num = {2, -3, 4, -11, -1, 6, -18};
        double expected = -3.0;
        //Act
        double result = ExercicioDez.obterValorMedioElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    // Testes exercício 10 alínea D
    @Test
    public void testProdutoElementosZero() {
        //Arrange
        int[] num = {2, -3, 0, -11, -1, 6, -18};
        double expected = 0;
        //Act
        double result = ExercicioDez.obterValorProdutoElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testProdutoElementosPositivos() {
        //Arrange
        int[] num = {2, 10, 4, 9};
        double expected = 720;
        //Act
        double result = ExercicioDez.obterValorProdutoElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testProdutoElementosPositivosNegativos() {
        //Arrange
        int[] num = {-2, 10, 4, -9, -7};
        double expected = -5040;
        //Act
        double result = ExercicioDez.obterValorProdutoElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testProdutoElementoZero() {
        //Arrange
        int[] num = {0};
        double expected = 0;
        //Act
        double result = ExercicioDez.obterValorMedioElementos(num);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Testes exercicio 10 e
    @Test
    public void testNumNaoRepetidosQuatroCinco() {
        //Arrange
        int[] num = {2, 3, 2, 3, 2, 4, 5};
        int[] expected = {4, 5};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testSoNumerosRepetidos() {
        //Arrange
        int[] num = {2, 2, 2};
        int[] expected = {};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testComNumeroNegativo() {
        //Arrange
        int[] num = {-2, 2, 2};
        int[] expected = {-2};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testSoZero() {
        //Arrange
        int[] num = {0};
        int[] expected = {0};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testDevolveDoisZero() {
        //Arrange
        int[] num = {1,1,1,2,0};
        int[] expected = {2,0};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testSoNumDiferentes() {
        //Arrange
        int[] num = {0, 1, 2, 3, 4, 5, 6};
        int[] expected = {0, 1, 2, 3, 4, 5, 6};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testSoNumRepetidoSeguidos() {
        //Arrange
        int[] num = {2, 3, 3, 3, 4, 5, 6};
        int[] expected = {2, 4, 5, 6};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testSoArrayVazio() {
        //Arrange
        int[] num = {};
        int[] expected = {};
        //Act
        int[] result = ExercicioDez.obterVectorComValoresNaoRepetidos(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    // Teste exercicio 10 f
    @Test
    public void obterVetorInvertidoUm() {
        //Arrange
        int[] num = {6, 7, 8, 9};
        int[] expected = {9, 8, 7, 6};
        //Act
        int[] result = ExercicioDez.obterVetorInvertido(num);
        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    public void obterVetorInvertidoDois() {
        //Arrange
        int[] num = {0, 80, 13, -10};
        int[] expected = {-10, 13, 80, 0};
        //Act
        int[] result = ExercicioDez.obterVetorInvertido(num);
        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    public void obterVetorInvertidoTres() {
        //Arrange
        int[] num = {0, -2, -2, -2};
        int[] expected = {-2, -2, -2, 0};
        //Act
        int[] result = ExercicioDez.obterVetorInvertido(num);
        //Assert
        assertArrayEquals(expected, result);

    }

    //Testes exercicio 10 alínea G
    @Test
    public void obterVetorComZeroNumPrimos() {
        //Arrange
        int[] num = {0, -2, -2, -2};
        int[] expected = {};
        //Act
        int[] result = ExercicioDez.obterNumPrimos(num);
        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    public void obterVetorComNumPrimosUm() {
        //Arrange
        int[] num = {0, 11, -2, 2};
        int[] expected = {11, 2};
        //Act
        int[] result = ExercicioDez.obterNumPrimos(num);
        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    public void obterVetorComNumPrimosDois() {
        //Arrange
        int[] num = {2, 11, 7, 2, 13};
        int[] expected = {2, 11, 7, 2, 13};
        //Act
        int[] result = ExercicioDez.obterNumPrimos(num);
        //Assert
        assertArrayEquals(expected, result);

    }

}
