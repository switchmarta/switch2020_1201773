package com.company;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioDozeTest {
    @Test
    public void verificarQuandoColunasDiferentes() {
        //Arrange
        int [][] matriz = {{2,3},{1},{3,5}};
        int expected = -1;
        int result = 0;
        //Act
        result = ExercicioDoze.verificarNumColunasMatriz(matriz);
        //Assert
        assertEquals(expected,result);
    }
    @Test
    public void verificarQuandoColunasDiferentesDois() {
        //Arrange
        int [][] matriz = {{3},{1},{3,5,6,8}};
        int expected = -1;
        int result = 0;
        //Act
        result = ExercicioDoze.verificarNumColunasMatriz(matriz);
        //Assert
        assertEquals(expected,result);
    }
    @Test
    public void verificarQuandoColunasIguais() {
        //Arrange
        int [][] matriz = {{2,3},{1,7},{3,5}};
        int expected = 2;
        int result = 0;
        //Act
        result = ExercicioDoze.verificarNumColunasMatriz(matriz);
        //Assert
        assertEquals(expected,result);
    }
    @Test
    public void verificarQuandoColunasTresIguais() {
        //Arrange
        int [][] matriz = {{2,3,20},{-1,7,0},{3,5,-6}};
        int expected = 3;
        int result = 0;
        //Act
        result = ExercicioDoze.verificarNumColunasMatriz(matriz);
        //Assert
        assertEquals(expected,result);
    }
    @Test
    public void verificarQuandoColunasVazias() {
        //Arrange
        int [][] matriz = {};
        int expected = -1;
        int result = 0;
        //Act
        result = ExercicioDoze.verificarNumColunasMatriz(matriz);
        //Assert
        assertEquals(expected,result);
    }
}
