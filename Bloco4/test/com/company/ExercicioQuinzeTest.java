package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioQuinzeTest {

    //Testes exercício 15 Alínea A
    @Test
    public void testMenorValorMatriz() {
        //Arrange
        int[][] matriz = {{50, -1}, {11, 1}};
        int expected = -1;
        //Act
        int result = ExercicioQuinze.obterMenorValorDeMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMenorValorMatrizComNegativosEZero() {
        //Arrange
        int[][] matriz = {{0}, {-90, -1}, {0, 0, 1}};
        int expected = -90;
        //Act
        int result = ExercicioQuinze.obterMenorValorDeMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMenorValorMatrizSegunda() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 4}, {0, 6, 7, 8}, {0, 9, 1, 1}};
        int expected = 0;
        //Act
        int result = ExercicioQuinze.obterMenorValorDeMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMenorValorMatrizComNumerosMaiores() {
        //Arrange
        int[][] matriz = {{560, 700, 100, 800}, {345, 678, 123}, {978, 13661, 18263}};
        int expected = 100;
        //Act
        int result = ExercicioQuinze.obterMenorValorDeMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    //testar matriz vazia não poderei retornar valor inteiro. como ultrapassar isto?
    //Testes exercício 15 Alínea B
    @Test
    public void testMaiorValorMatriz() {
        //Arrange
        int[][] matriz = {{50, -1}, {11, 1}};
        int expected = 50;
        //Act
        int result = ExercicioQuinze.obterMaiorValorNumaMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMaiorValorMatrizComNegativosEZero() {
        //Arrange
        int[][] matriz = {{0}, {-90, -1}, {0, 0, 1}};
        int expected = 1;
        //Act
        int result = ExercicioQuinze.obterMaiorValorNumaMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMaiorValorMatrizSegunda() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 4}, {0, 6, 7, 8}, {0, 9, 1, 1}};
        int expected = 9;
        //Act
        int result = ExercicioQuinze.obterMaiorValorNumaMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMaiorValorMatrizComNumerosMaiores() {
        //Arrange
        int[][] matriz = {{560, 700, 100, 800}, {345, 678, 123}, {978, 13661, 18263}};
        int expected = 18263;
        //Act
        int result = ExercicioQuinze.obterMaiorValorNumaMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    //Testes exercício 15 Alínea C
    @Test
    public void testNumElementosMatriz() {
        //Arrange
        int[][] matriz = {{560, 700, 100, 800}, {345, 678, 123}, {978, 13661, 18263}};
        int expected = 10;
        //Act
        int result = ExercicioQuinze.obterTotalElementosMatriz(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testMediaElementosMatriz() {
        //Arrange
        int[][] matriz = {{560, 700, 100, 800}, {345, 678, 123}, {978, 13661, 18263}};
        double expected = 3620.8;
        //Act
        double result = ExercicioQuinze.obterValorMedioElementosMatriz(matriz);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaElementosMatrizSegundo() {
        //Arrange
        int[][] matriz = {{0}, {-90, -1}, {0, 0, 1}};
        double expected = -15;
        //Act
        double result = ExercicioQuinze.obterValorMedioElementosMatriz(matriz);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaElementosMatrizTerceiro() {
        //Arrange
        int[][] matriz = {{50, -1}, {11, 1}};
        double expected = 15.3;
        //Act
        double result = ExercicioQuinze.obterValorMedioElementosMatriz(matriz);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Testes exercício 15 alínea D
    @Test
    public void testProdutoElementosMatriz() {
        //Arrange
        int[][] matriz = {{50, -1}, {11, 1}};
        double expected = -550;
        //Act
        double result = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testProdutoElemntosComNegativosEZero() {
        //Arrange
        int[][] matriz = {{0}, {-90, -1}, {0, 0, 1}};
        double expected = 0;
        //Act
        double result = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testProdutoElementosMatrizSegundo() {
        //Arrange
        int[][] matriz = {{3}, {3, 7}, {1, 5, 1}};
        double expected = 315;
        //Act
        double result = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Testes exercício 15 alínea E
    @Test
    public void testRetornarNumNaoRepetidos() {
        //Arrange
        int[][] matriz = {{3}, {3, 7}, {1, 5, 1}};
        int[] expected = {3, 7, 1, 5};
        int[] result = {};
        //Act
        result = ExercicioQuinze.devolverElementosNaoRepetidos(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornarNumNaoRepetidosComZero() {
        //Arrange
        int[][] matriz = {{3, 0}, {3, 0, 7}, {0, 5, 1}};
        int[] expected = {3, 0, 7, 5, 1};
        int[] result = {};
        //Act
        result = ExercicioQuinze.devolverElementosNaoRepetidos(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornarNumNaoRepetidosComNegativos() {
        //Arrange
        int[][] matriz = {{-3, 0, 3}, {3, -1, 7}, {-90, 5, 1}};
        int[] expected = {-3, 0, 3, -1, 7, -90, 5, 1};
        int[] result = {};
        //Act
        result = ExercicioQuinze.devolverElementosNaoRepetidos(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornarNull() {
        //Arrange
        int[][] matriz = null;
        int[] result = {};
        //Act
        result = ExercicioQuinze.devolverElementosNaoRepetidos(matriz);
        //Assert
        assertNull(result);
    }

    //Testes exercício 15 alínea F
    @Test
    public void testRetornarNumPrimosMatriz() {
        //Arrange
        int[][] matriz = {{3}, {3, 7}, {1, 5, 1}};
        int[] expected = {3, 3, 7, 5};
        //Act
        int[] result = ExercicioQuinze.devolverElementosNumPrimos(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornarQuandoNaoExistemNumPrimos() {
        //Arrange
        int[][] matriz = {{0, 0}, {-10, -7}, {1, 4, 1}};
        int[] expected = {};
        //Act
        int[] result = ExercicioQuinze.devolverElementosNumPrimos(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornarNumPrimosMatrizSegunda() {
        //Arrange
        int[][] matriz = {{0, 1, 1, -10}, {11, 13}, {5, 3, 1, 2, 3}};
        int[] expected = {11, 13, 5, 3, 2, 3};
        //Act
        int[] result = ExercicioQuinze.devolverElementosNumPrimos(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornarNumPrimosMaiores() {
        //Arrange
        int[][] matriz = {{20}, {21, 37, 31, 53}, {101, 77, 1, 2, 0}};
        int[] expected = {37, 31, 53, 101, 2};
        //Act
        int[] result = ExercicioQuinze.devolverElementosNumPrimos(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    //Testes exercício 15 alínea G
    @Test
    public void testDiagonalMatrizQuadrada() {
        //Arrange
        int[][] matriz = {{20, 3, 4}, {21, 31, 53}, {101, 77, 1}};
        int[] expected = {20, 31, 1};
        //Act
        int[] result = ExercicioQuinze.retornarDiagonalMatriz(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testDiagonalMatrizRetangular() {
        //Arrange
        int[][] matriz = {{20, 3}, {21, 31}, {101, 77}};
        int[] expected = {20, 31};
        //Act
        int[] result = ExercicioQuinze.retornarDiagonalMatriz(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizNaoRetangularNemQuadrada() {
        //Arrange
        int[][] matriz = {{20, 3, 1}, {21}, {101, 77}};
        int[] expected = null;
        //Act
        int[] result = ExercicioQuinze.retornarDiagonalMatriz(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    //Testes exercício 15 alínea H
    @Test
    public void testDiagonalSecundariaMatrizQuadrada() {
        //Arrange
        int[][] matriz = {{20, 3, 4}, {21, 31, 53}, {101, 77, 1}};
        int[] expected = {4, 31, 101};
        //Act
        int[] result = ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testDiagonalSecundariaMatrizRetangular() {
        //Arrange
        int[][] matriz = {{20, 3}, {4, 5}, {-1, 0}};
        int[] expected = {3, 4};
        //Act
        int[] result = ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizNaoRegular() {
        //Arrange
        int[][] matriz = {{20, 3}, {0}, {-1, 0}, {1, -100, 1}};
        //Act
        int[] result = ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz);
        //Assert
        assertNull(result);
    }

    //Testes exercício 15 alínea I
    @Test
    public void testNaoEMatrizQuadrada() {
        //Arrange
        int[][] matriz = {{1, 0, 0}, {0, 1, 0}};
        boolean result = true;
        //Act
        result = ExercicioQuinze.eMatrizIdentidade(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testEMatrizIdentidade() {
        //Arrange
        int[][] matriz = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        boolean result = false;
        //Act
        result = ExercicioQuinze.eMatrizIdentidade(matriz);
        //Assert
        assertTrue(result);
    }

    @Test
    public void testNaoMatrizIdentidadeQuandoDiagonalTrue() {
        //Arrange
        int[][] matriz = {{1, 0, 10}, {9, 1, 0}, {0, 0, 1}};
        boolean result = true;
        //Act
        result = ExercicioQuinze.eMatrizIdentidade(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testNaoMatrizIdentidadeQuandoRestoElementosTrue() {
        //Arrange
        int[][] matriz = {{9, 0, 0}, {0, 1, 0}, {0, 0, 30}};
        boolean result = true;
        //Act
        result = ExercicioQuinze.eMatrizIdentidade(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testEMatrizIdentidadeSegunda() {
        //Arrange
        int[][] matriz = {{1, 0}, {0, 1}};
        boolean result = false;
        //Act
        result = ExercicioQuinze.eMatrizIdentidade(matriz);
        //Assert
        assertTrue(result);
    }
    //TESTES MATRIZ INVERSA - exercício 15 alínea J.

    @Test
    public void testMatrizReduzidaI() {
        //Arrange
        int[][] matriz = {{15, 20, 30, 5}, {4, 5, 6, 9}, {12, 14, 16, 18}, {0, 0, 0, 0}};
        int[][] expected = {{5, 6, 9}, {14, 16, 18}, {0, 0, 0}};
        int[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizReduzida(matriz, 0, 0);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizReduzidaII() {
        //Arrange
        int[][] matriz = {{15, 20, 30, 5},
                {4, 5, 6, 9},
                {12, 14, 16, 18},
                {0, 0, 0, 0}};
        int[][] expected = {{4, 6, 9}, {12, 16, 18}, {0, 0, 0}};
        int[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizReduzida(matriz, 0, 1);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testMatrizReduzidaIII() {
        //Arrange
        int[][] matriz = {{15, 20, 30, 5},
                {4, 5, 6, 9},
                {12, 14, 16, 18},
                {0, 0, 0, 0}};
        int[][] expected = {{15, 30, 5}, {4, 6, 9}, {0, 0, 0}};
        int[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizReduzida(matriz, 2, 1);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizReduzidaNula() {
        //Arrange
        int[][] matriz = null;
        int[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizReduzida(matriz, 0, 0);
        //Assert
        assertNull(result);
    }

    @Test
    public void testMatrizReduzidaTamanho2X2() {
        //Arrange
        int[][] matriz = {{1, 2}, {3, 4}};
        int[][] expected = {{1, 2}, {3, 4}};
        int[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizReduzida(matriz, 0, 0);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizReduzidaVazia() {
        //Arrange
        int[][] matriz = {};
        int[][] expected = {};
        int[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizReduzida(matriz, 0, 0);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizNaoInvertivel() {
        //Arrange
        int[][] matriz = {{15, 20, 30, 5}, {4, 5, 6, 9}, {12, 14, 16, 18}, {0, 0, 0, 0}};
        double[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizInversa(matriz);
        //Assert
        assertNull(result); //matriz não é invertivel
    }

    @Test
    public void testMatrizInversaNula() {
        //Arrange
        int[][] matriz = null;
        //Act
        double[][] result = ExercicioQuinze.obterMatrizInversa(matriz);
        //Assert
        assertNull(result);


    }
    @Test
    public void testMatriz4X4Inversa() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 4}, {2, 1, 1, 2}, {3, 2, 1, 1}, {7, 3, 2, 2}};
        double[][] expected = {{-0.1, -0.2, -1.2, 0.3}, {-0.1, 0.1, 1.6, -0.7},
                {0.7, -1.7, -1.2, 0.9}, {-0.2, 1.2, 0.2, -0.4}};
        //Act
        double[][] result = ExercicioQuinze.obterMatrizInversa(matriz);
        //Assert
        assertArrayEquals(expected, result);


    }

    @Test
    public void testMatriz3X3Inversa() {
        //Arrange
        int[][] matriz = {{1, 2, 3},
                {2, 1, 1},
                {3, 2, 1}};
        double[][] expected = {{-0.25, 1, -0.25}, {0.25, -2, 1.25}, {0.25, 1, -0.75}};
        //Act
        double[][] result = ExercicioQuinze.obterMatrizInversa(matriz);
        //Assert


    }


    //Testes exercício 15 alínea K
    @Test
    public void testMatrizRetangularTransposta() {
        //Arrange
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}};
        double[][] expected = {{1, 4}, {2, 5}, {3, 6}};
        double[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizTransposta(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizRetangularTranspostaSegunda() {
        //Arrange
        int[][] matriz = {{15, 20, 30, 5}, {4, 5, 6, 9}, {12, 14, 16, 18}, {0, 0, 0, 0}};
        double[][] expected = {{15, 4, 12, 0}, {20, 5, 14, 0}, {30, 6, 16, 0}, {5, 9, 18, 0}};
        double[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizTransposta(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testMatrizNaoRetangularQuadrada() {
        //Arrange
        int[][] matriz = {{1, 2, 0}, {0, 5, 6}, {0}};
        double[][] expected = null;
        double[][] result = {};
        //Act
        result = ExercicioQuinze.obterMatrizTransposta(matriz);
        //Assert
        assertArrayEquals(expected, result);
    } //Faltam mais testes
}
