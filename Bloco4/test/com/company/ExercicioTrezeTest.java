package com.company;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ExercicioTrezeTest {
    @Test
    public void testMatrizQuadradaTrue() {
        //Arrange
        int[][] matriz = {{1,2,3,4},{11, 12, 5, 3}, {-1,-2,-10,-9}, {0,0,1,0}};
        //Act
        boolean result = ExercicioTreze.verificarMatrizQuadrada(matriz);
        //Assert
        assertTrue(result);
    }
    @Test
    public void testMatrizQuadradaTrueSegunda() {
        //Arrange
        int[][] matriz = {{1}};
        //Act
        boolean result = ExercicioTreze.verificarMatrizQuadrada(matriz);
        //Assert
        assertTrue(result);
    }
    @Test
    public void testMatrizQuadradaFalse() {
        //Arrange
        int[][] matriz = {{1}, {11}, {0}};
        //Act
        boolean result = ExercicioTreze.verificarMatrizQuadrada(matriz);
        //Assert
        assertFalse(result);
    }
    @Test
    public void testMatrizQuadradaFalseSegunda() {
        //Arrange
        int[][] matriz = {{1,2,3,6,8,9}, {11,2,9,10,30,68}, {0,1}, {1,2,3,3,4}, {23,21,-10,-14}, {-50,12,-56} };
        //Act
        boolean result = ExercicioTreze.verificarMatrizQuadrada(matriz);
        //Assert
        assertFalse(result);
    }
    @Test
    public void testMatrizVazia() {
        //Arrange
        int[][] matriz = {};
        //Act
        boolean result = ExercicioTreze.verificarMatrizQuadrada(matriz);
        //Assert
        assertFalse(result);
    }
}
