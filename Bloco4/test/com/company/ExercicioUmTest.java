package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTest {

    //Testes exercicio 1
    @Test
    public void testRetornaOitoDeComprimento() {
        //Arrange
        int num = 74635984;
        int expected = 8;
        int result = -1;
        //Act
        result = ExercicioUm.retonarTamanhoNum(74635984);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRetornaDois() {
        //Arrange
        int num = 37;
        int expected = 2;
        int result = -1;
        //Act
        result = ExercicioUm.retonarTamanhoNum(num);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testRetornaErroDevidoNumeroNegativo() {
        //Arrange
        int num = -74635984;
        int expected = -1;
        int result = -1;
        //Act
        result = ExercicioUm.retonarTamanhoNum(num);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testRetornaComprimentoContandoComZeros() {
        //Arrange
        int num = 700;
        int expected = 3;
        int result = -1;
        //Act
        result = ExercicioUm.retonarTamanhoNum(num);
        //Assert
        assertEquals(expected, result);
    }
}