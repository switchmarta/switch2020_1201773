package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioDezasseisTest {
    @Test
    public void testRetornaArrayComPrimeiraLinha() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 1, 2}, {3, 4, 5, 6}};
        int[] expected = {1, 2, 3, 4};
        int[] result = {-1};
        //Act
        result = ExercicioDezasseis.obterPrimeiraLinhaMatriz(matriz);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornaArrayNull() {
        //Arrange
        int[][] matriz = null;
        int[] result = {-1};
        //Act
        result = ExercicioDezasseis.obterPrimeiraLinhaMatriz(matriz);
        //Assert
        assertNull(result);
    }

    @Test
    public void testRetornaMatrizReduzida() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 1, 2}, {3, 4, 5, 6}};

        int j = 0;
        int[][] expected = {{6, 7, 8}, {10, 1, 2}, {4, 5, 6}};
        int[][] result = {};
        //Act
        result = ExercicioDezasseis.obterMatrizReduzida(matriz, j);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornaMatrizComDoisTamanho() {
        //Arrange
        int[][] matriz = {{1, 2}, {5, 6}};
        int j = 0;
        int[][] result = {};
        //Act
        result = ExercicioDezasseis.obterMatrizReduzida(matriz, j);
        //Assert
        assertNull(result);
    }

    @Test
    public void testRetornaMatrizReduzidaQuandoJIgualUm() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 1, 2},
                {3, 4, 5, 6}};
        int j = 1;
        int[][] expected = {{5, 7, 8}, {9, 1, 2}, {3, 5, 6}};
        int[][] result = {};
        //Act
        result = ExercicioDezasseis.obterMatrizReduzida(matriz, j);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testcalculoDeterminanteMatrizNull() {
        //Arrange
        int[][] matriz = null;
        Integer result = 0;
        //Act
        result = ExercicioDezasseis.calcularDeterminante(matriz);
        //Assert
        assertNull(result);
    }

    @Test
    public void testcalculoDeterminanteMatrizNaoQuadrada() {
        //Arrange
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}};
        Integer result = 0;
        //Act
        result = ExercicioDezasseis.calcularDeterminante(matriz);
        //Assert
        assertNull(result);
    }

    @Test
    public void testRetornaDeterminanteMatrizTresPorTres() {
        //Arrange
        int[][] matriz = {{1, 2, 3},
                {2, 1, 1},
                {3, 2, 1}};
        Integer expected = 4;
        Integer result = 0;
        //Act
        result = ExercicioDezasseis.calcularDeterminante(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRetornaDeterminanteMatrizQuatroPorQuatro() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 1},
                {2, 1, 1, 2},
                {3, 2, 1, 3},
                {1, 2, 3, 1}};
        Integer expected = 0;
        Integer result = 0;
        //Act
        result = ExercicioDezasseis.calcularDeterminante(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRetornaDeterminanteMatrizQuatroPorQuatroSegunda() {
        //Arrange
        int[][] matriz = {{3, 1, 4, 5},
                {7, 1, 1, 2},
                {3, 2, 1, 3},
                {1, 2, 3, 1}};
        Integer expected = -148;
        Integer result = 0;
        //Act
        result = ExercicioDezasseis.calcularDeterminante(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRetornaDeterminanteMatrizCincoPorCinco() {
        //Arrange
        int[][] matriz = {{3, 1, 4, 5, 1},
                {7, 1, 1, 2, 1},
                {3, 2, 1, 3, 1},
                {1, 2, 3, 1, 3},
                {0, 0, -1, 1, 2}};
        Integer expected = -456;
        Integer result = 0;
        //Act
        result = ExercicioDezasseis.calcularDeterminante(matriz);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void determinanteMatriz4X4() {
        int[][] matriz = {{1, 2, 3, 4}, {2, 1, 1, 2}, {3, 2, 1, 1}, {7, 3, 2, 2}};
        Integer expected = -10;
        Integer result = 0;
        //Act
        result = ExercicioDezasseis.calcularDeterminante(matriz);
        //Assert
        assertEquals(expected, result);
    }


}
