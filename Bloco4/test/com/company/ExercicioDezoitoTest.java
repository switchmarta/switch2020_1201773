package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioDezoitoTest {
    //Exercício 18 alínea a
    @Test
    public void testQuandoNaoExisteLetra() {
        //Arrange
        int[][] sopaLetras =
                {{'C', 'Y', 'V', 'E', 'O', 'K', 'L'},
                        {'O', 'Z', 'O', 'T', 'N', 'E', 'A'},
                        {'V', 'A', 'A', 'G', 'C', 'I', 'T'},
                        {'I', 'G', 'U', 'O', 'A', 'M', 'A'},
                        {'D', 'P', 'I', 'Z', 'Z', 'A', 'N'},
                        {'O', 'B', 'Y', 'I', 'U', 'R', 'P'},
                        {'H', 'A', 'L', 'E', 'N', 'A', 'C'}};
        char letra = 'j';
        int[][] expected = {{0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}};
        int[][] resultado = {};
        //Act
        resultado = ExercicioDezoito.obterMatrizMascaraComLetraPretendida(sopaLetras, letra);
        //Assert
        assertArrayEquals(expected, resultado);
    }

    @Test
    public void testProcurarLetraC() {
        //Arrange
        int[][] sopaLetras =
                {{'C', 'Y', 'V', 'E', 'O', 'K', 'L'},
                        {'O', 'Z', 'O', 'T', 'N', 'E', 'A'},
                        {'V', 'A', 'A', 'G', 'C', 'I', 'T'},
                        {'I', 'G', 'U', 'O', 'A', 'M', 'A'},
                        {'D', 'P', 'I', 'Z', 'Z', 'A', 'N'},
                        {'O', 'B', 'Y', 'I', 'U', 'R', 'P'},
                        {'H', 'A', 'L', 'E', 'N', 'A', 'C'}};
        char letra = 'c';
        int[][] expected = {{1, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1}};
        int[][] resultado = {};
        //Act
        resultado = ExercicioDezoito.obterMatrizMascaraComLetraPretendida(sopaLetras, letra);
        //Assert
        assertArrayEquals(expected, resultado);
    }

    @Test
    public void testQuandoLetraVazia() {
        //Arrange
        int[][] sopaLetras =
                {{'C', 'Y', 'V', 'E', 'O', 'K', 'L'},
                        {'O', 'Z', 'O', 'T', 'N', 'E', 'A'},
                        {'V', 'A', 'A', 'G', 'C', 'I', 'T'},
                        {'I', 'G', 'U', 'O', 'A', 'M', 'A'},
                        {'D', 'P', 'I', 'Z', 'Z', 'A', 'N'},
                        {'O', 'B', 'Y', 'I', 'U', 'R', 'P'},
                        {'H', 'A', 'L', 'E', 'N', 'A', 'C'}};
        char letra = ' ';
        int[][] resultado = {};
        //Act
        resultado = ExercicioDezoito.obterMatrizMascaraComLetraPretendida(sopaLetras, letra);
        //Assert
        assertNull(resultado);
    }

    //Exercício 18 alínea b
    @Test
    public void testProcurarPalavraPizza() {
        //Arrange
        int[][] sopaLetras =
                {{'C', 'Y', 'V', 'E', 'O', 'K', 'L'},
                        {'O', 'Z', 'O', 'T', 'N', 'E', 'A'},
                        {'V', 'A', 'A', 'G', 'C', 'I', 'T'},
                        {'I', 'G', 'U', 'O', 'A', 'M', 'A'},
                        {'D', 'P', 'I', 'Z', 'Z', 'A', 'N'},
                        {'O', 'B', 'Y', 'I', 'U', 'R', 'P'},
                        {'H', 'A', 'L', 'E', 'N', 'A', 'C'}};
        String palavra = "pizza";
        boolean resultado = false;
        //Act
        resultado = ExercicioDezoito.existeNumaDirecao(sopaLetras, palavra);
        //Assert
        assertTrue(resultado);
    }
    @Test
    public void testProcurarPalavraQueNaoExiste() {
        //Arrange
        int[][] sopaLetras =
                {{'C', 'Y', 'V', 'E', 'O', 'K', 'L'},
                        {'O', 'Z', 'O', 'T', 'N', 'E', 'A'},
                        {'V', 'A', 'A', 'G', 'C', 'I', 'T'},
                        {'I', 'G', 'U', 'O', 'A', 'M', 'A'},
                        {'D', 'P', 'I', 'Z', 'Z', 'A', 'N'},
                        {'O', 'B', 'Y', 'I', 'U', 'R', 'P'},
                        {'H', 'A', 'L', 'E', 'N', 'A', 'C'}};
        String palavra = "MOCHILA";
        boolean resultado = false;
        //Act
        resultado = ExercicioDezoito.existeNumaDirecao(sopaLetras, palavra);
        //Assert
        assertFalse(resultado);
    }
    @Test
    public void testProcurarSopaLetrasVazia() {
        //Arrange
        int[][] sopaLetras = {};
        String palavra = "mar";
        boolean resultado = false;
        //Act
        resultado = ExercicioDezoito.existeNumaDirecao(sopaLetras, palavra);
        //Assert
        assertFalse(resultado);
    }
    @Test
    public void testProcurarPalavraVazia() {
        //Arrange
        int[][] sopaLetras =
                {{'C', 'Y', 'V', 'E', 'O', 'K', 'L'},
                        {'O', 'Z', 'O', 'T', 'N', 'E', 'A'},
                        {'V', 'A', 'A', 'G', 'C', 'I', 'T'},
                        {'I', 'G', 'U', 'O', 'A', 'M', 'A'},
                        {'D', 'P', 'I', 'Z', 'Z', 'A', 'N'},
                        {'O', 'B', 'Y', 'I', 'U', 'R', 'P'},
                        {'H', 'A', 'L', 'E', 'N', 'A', 'C'}};
        String palavra = " ";
        boolean resultado = false;
        //Act
        resultado = ExercicioDezoito.existeNumaDirecao(sopaLetras, palavra);
        //Assert
        assertFalse(resultado);
    }
    @Test
    public void testProcurarPalavraNull() {
        //Arrange
        int[][] sopaLetras =
                {{'C', 'Y', 'V', 'E', 'O', 'K', 'L'},
                        {'O', 'Z', 'O', 'T', 'N', 'E', 'A'},
                        {'V', 'A', 'A', 'G', 'C', 'I', 'T'},
                        {'I', 'G', 'U', 'O', 'A', 'M', 'A'},
                        {'D', 'P', 'I', 'Z', 'Z', 'A', 'N'},
                        {'O', 'B', 'Y', 'I', 'U', 'R', 'P'},
                        {'H', 'A', 'L', 'E', 'N', 'A', 'C'}};
        String palavra = null;
        boolean resultado = false;
        //Act
        resultado = ExercicioDezoito.existeNumaDirecao(sopaLetras, palavra);
        //Assert
        assertFalse(resultado);
    }
}
