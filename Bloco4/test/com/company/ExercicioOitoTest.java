package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ExercicioOitoTest {
    //Testes exercicio 8
    @Test
    public void testRetornaArrayMultiplosDoisTres() {
        //Arrange
        int limit1 = 4;
        int limit2 = 12;
        int[]n = {2,3};
        int[] expected = {6,12};
        int [] result = {};
        //Act
        result = ExercicioOito.obterMultiplosNumComunsDadoNoIntervalo(limit1, limit2,n);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testRetornaArrayMultiplosDoisTresComLimiteZero() {
        //Arrange
        int limit1 = 0;
        int limit2 = 12;
        int[]n = {2,3};
        int[] expected = {6,12};
        int [] result = {};
        //Act
        result = ExercicioOito.obterMultiplosNumComunsDadoNoIntervalo(limit1, limit2,n);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testRetornaArrayMultiplosDoisTresQuatro() {
        //Arrange
        int limit1 = 4;
        int limit2 = 12;
        int[]n = {2,3,4};
        int[] expected = {12};
        int [] result = {};
        //Act
        result = ExercicioOito.obterMultiplosNumComunsDadoNoIntervalo(limit1, limit2,n);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testRetornaArrayMultiplosDoisTresQuatroComNumerosNegativos() {
        //Arrange
        int limit1 = -12;
        int limit2 = 12;
        int[]n = {2,3,4};
        int[] expected = {-12,12};
        int [] result = {};
        //Act
        result = ExercicioOito.obterMultiplosNumComunsDadoNoIntervalo(limit1, limit2,n);
        //Assert
        assertArrayEquals(expected, result);
    }
}
