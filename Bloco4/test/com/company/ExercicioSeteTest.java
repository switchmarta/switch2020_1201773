package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ExercicioSeteTest {
    //Testes exercicio 7
    @Test
    public void testeObterArrayMultiplosTres() {
        //Arrange
        int limit1 = 0;
        int limit2 = 10;
        int algarismo = 3;
        int [] expected = {3,6,9};
        int [] result = {};
        //Act
        result = ExercicioSete.obterMultiplosNumeroDadoNoIntervalo(limit1, limit2, algarismo);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testeObterArrayMultiplosCinco() {
        //Arrange
        int limit1 = 5;
        int limit2 = 20;
        int algarismo = 5;
        int [] expected = {5,10,15,20};
        int [] result = {};
        //Act
        result = ExercicioSete.obterMultiplosNumeroDadoNoIntervalo(limit1, limit2, algarismo);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testeObterArrayMultiplosSete() {
        //Arrange
        int limit1 = 5;
        int limit2 = 30;
        int algarismo = 7;
        int [] expected = {7,14,21,28};
        int [] result = {};
        //Act
        result = ExercicioSete.obterMultiplosNumeroDadoNoIntervalo(limit1, limit2, algarismo);
        //Assert
        assertArrayEquals(expected, result);
    }
}
