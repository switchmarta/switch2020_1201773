package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioCatorzeTest {
    @Test
    public void testeTrueRaizRetangular() {
        //Arrange
        int[][] matriz = {{1}, {11}, {0}};
        boolean result = false;
        //Act
        result = ExercicioCatorze.eMatrizRetangular(matriz);
        //Assert
        assertTrue(result);
    }

    @Test
    public void testFalseMatrizRetangularDiferente() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 6, 8, 9}, {11, 2, 9, 10, 30, 68}, {0, 1}, {1, 2, 3, 3, 4}, {23, 21, -10, -14}, {-50, 12, -56}};
        boolean result = true;
        //Act
        result = ExercicioCatorze.eMatrizRetangular(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testeFalseRaizRetangular() {
        //Arrange
        int[][] matriz = {{1}};
        boolean result = true;
        //Act
        result = ExercicioCatorze.eMatrizRetangular(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testeFalseRaizRetangularSegunda() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 4}, {11, 12, 5, 3}, {-1, -2, -10, -9}, {0, 0, 1, 0}};
        boolean result = true;
        //Act
        result = ExercicioCatorze.eMatrizRetangular(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testTrueMatrizRetangularSegunda() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 6}, {11, 2, 9, 10}, {1, 2, 3, 3}};
        boolean result = false;
        //Act
        result = ExercicioCatorze.eMatrizRetangular(matriz);
        //Assert
        assertTrue(result);
    }

    @Test
    public void testMatrizRetangularVazia() {
        //Arrange
        int[][] matriz = {};
        boolean result = true;
        //Act
        result = ExercicioCatorze.eMatrizRetangular(matriz);
        //Assert
        assertFalse(result);
    }
}
