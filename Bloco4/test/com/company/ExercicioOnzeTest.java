package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioOnzeTest {
    @Test
    public void verificarVetoresTamanhoDiferente() {
        //Arrange
        int[] vetorUm = {2, 3, 4, 5};
        int[] vetorDois = {2, 3};
        double expected = -1; //retorna -1 quando vetor são diferentes tamanhos.
        //Act
        double result = ExercicioOnze.obterProdutoEscalarVetores(vetorUm, vetorDois);
        //Arrange
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void verificarVetoresNull() {
        //Arrange
        int[] vetorUm = {2, 3, 4, 5};
        int[] vetorDois = null;
        Integer result = 1;
        //Act
        result = ExercicioOnze.obterProdutoEscalarVetores(vetorUm, vetorDois);
        //Arrange
        assertNull(result);

    }
    @Test
    public void verificarProdutoVetoresTamanhoDois() {
        //Arrange
        int[] vetorUm = {2, 3};
        int[] vetorDois = {4, 5};
        Integer expected = 23;
        Integer result = 1;
        //Act
        result = ExercicioOnze.obterProdutoEscalarVetores(vetorUm, vetorDois);
        //Arrange
        assertEquals(expected, result);

    }
    @Test
    public void verificarProdutoVetoresTamanhoTres() {
        //Arrange
        int[] vetorUm = {-1, 3, 8};
        int[] vetorDois = {4, 5, -2};
        Integer expected = -5;
        Integer result = 1;
        //Act
        result = ExercicioOnze.obterProdutoEscalarVetores(vetorUm, vetorDois);
        //Arrange
        assertEquals(expected, result);

    }

}
