package com.company;

import com.sun.tools.javac.util.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ExercicioDoisTest {
    //Testes exercício 2
    @Test
    public void testRetornaArrayCatorze() {
        //Arrange
        int num = 14;
        int[] expected = {1, 4};
        int [] result = {-1};
        //Act
        result = ExercicioDois.obterArray(num);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testRetornaArrayMilQuatrocentosCinquentaCinco() {
        //Arrange
        int num = 1455;
        int[] expected = {1, 4, 5, 5};
        int [] result = {-1};
        //Act
        result = ExercicioDois.obterArray(num);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testRetornaArrayCentoSetenta() {
        //Arrange
        int num = 170;
        int[] expected = {1, 7, 0};
        int [] result = {-1};
        //Act
        result = ExercicioDois.obterArray(num);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testRetornaArrayNegativo() {
        //Arrange
        int num = -14;
        int [] expected = null;
        int [] result = {-1};
        //Act
        result = ExercicioDois.obterArray(num);
        //Assert
        assertArrayEquals(expected,result);
    }
}
