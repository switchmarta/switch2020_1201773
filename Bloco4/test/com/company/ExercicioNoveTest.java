package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioNoveTest {
    @Test
    public void testCapicuaTrue() {
        //Arrange
        int[]num = {1,2,1};
        boolean result = false;
        //Act
        result = ExercicioNove.obterCapicua(num);
        //Assert
        assertTrue(result);
    }
    @Test
    public void testCapicuaNumeroNegativo() {
        //Arrange
        int[]num = {-1,2,1}; //por ser número negativo;
        boolean result = true;
        //Act
        result = ExercicioNove.obterCapicua(num);
        //Assert
        assertFalse(result);
    }
    @Test
    public void testCapicuaQuandoComprimentoEZero() {
        //Arrange
        int[]num = {1};
        boolean result = true;
        //Act
        result = ExercicioNove.obterCapicua(num);
        //Assert
        assertFalse(result);//por ter apenas 1 número no Array;
    }
    @Test
    public void testTrueCapicuaOnze() {
        //Arrange
        int[]num = {11};
        boolean result = true;
        //Act
        result = ExercicioNove.obterCapicua(num);
        //Assert
        assertFalse(result);
    }
    @Test
    public void testCapicuaTrueDois() {
        //Arrange
        int[]num = {2,3,3,2};
        boolean result = false;
        //Act
        result = ExercicioNove.obterCapicua(num);
        //Assert
        assertTrue(result);
    }
    @Test
    public void testCapicuaFalse() {
        //Arrange
        int[]num = {2,3,4,2};
        boolean result = true;
        //Act
        result = ExercicioNove.obterCapicua(num);
        //Assert
        assertFalse(result);
    }

}
