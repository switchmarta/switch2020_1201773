package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExercicioDezasseteTest {

    @Test
    public void testeProdutoMatrizComConstanteTres() {
        //Arrange
        int[][] matriz = {{1}, {11}, {0}};
        int constante = 3;
        int[][] expected = {{3}, {33}, {0}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizComConstante(matriz, constante);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeProdutoMatrizComConstanteNove() {
        //Arrange
        int[][] matriz = {{1, 2, 4}, {-10, 9, 3}, {0, -5, -1}};
        int constante = 9;
        int[][] expected = {{9, 18, 36}, {-90, 81, 27}, {0, -45, -9}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizComConstante(matriz, constante);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeProdutoMatrizRetangularComConstanteCinco() {
        //Arrange
        int[][] matriz = {{1, 2, 4}, {-10, 9, 3}};
        int constante = 5;
        int[][] expected = {{5, 10, 20}, {-50, 45, 15}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizComConstante(matriz, constante);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeProdutoMatrizNull() {
        //Arrange
        int[][] matriz = {{1, 1, 3}, {9, 0}, {0, 0, 1}};
        int constante = 9;
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizComConstante(matriz, constante);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeProdutoMatrizVazia() {
        //Arrange
        int[][] matriz = {};
        int constante = 9;
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizComConstante(matriz, constante);
        //Assert
        assertNull(result);
    }

    //Testes para o método de matriz quadrada ou retangular
    @Test
    public void testeMatrizVazia() {
        //Arrange
        int[][] matriz = {};
        boolean result = true;
        //Act
        result = ExercicioDezassete.eMatrizRetangularOuQuadrada(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testMatrizFalse() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 6, 8, 9}, {11, 2, 9, 10, 30, 68}, {0, 1}, {1, 2, 3, 3, 4}, {23, 21, -10, -14}, {-50, 12, -56}};
        //Act
        boolean result = ExercicioDezassete.eMatrizRetangularOuQuadrada(matriz);
        //Assert
        assertFalse(result);
    }

    @Test
    public void testMatrizQuadradaTrue() {
        //Arrange
        int[][] matriz = {{1, 2}, {3, 4}};
        //Act
        boolean result = ExercicioDezassete.eMatrizRetangularOuQuadrada(matriz);
        //Assert
        assertTrue(result);
    }

    @Test
    public void testTrueMatrizRetangular() {
        //Arrange
        int[][] matriz = {{1, 2, 3, 6}, {11, 2, 9, 10}, {1, 2, 3, 3}};
        boolean result = false;
        //Act
        result = ExercicioDezassete.eMatrizRetangularOuQuadrada(matriz);
        //Assert
        assertTrue(result);
    }

    // Testes alínea B
    @Test
    public void testeSomaMatrizesQuandoNull() {
        //Arrange
        int[][] matrizUm = {{1, 1, 3}, {9, 0}, {0, 0, 1}};
        int[][] matrizDois = null;
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularSomaMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeSomaMatrizesQuandoDuasNull() {
        //Arrange
        int[][] matrizUm = null;
        int[][] matrizDois = null;
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularSomaMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeSomaMatrizesComColunasDiferentes() {
        //Arrange
        int[][] matrizUm = {{1, 1, 3}, {9, 0, 1}, {0, 0, 1}};
        int[][] matrizDois = {{1, 2}, {9, 8}, {7, -1}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularSomaMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeSomaMatrizesComLinhasDiferentes() {
        //Arrange
        int[][] matrizUm = {{1, 1, 3}, {9, 0, 1}};
        int[][] matrizDois = {{1, 2, 9}, {9, 8, -3}, {7, -1, 6}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularSomaMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeSomaMatrizesRetangulares() {
        //Arrange
        int[][] matrizUm = {{1, 1, 3}, {9, 0, 1}};
        int[][] matrizDois = {{1, 2, 9}, {9, 8, -3}};
        int[][] expected = {{2, 3, 12}, {18, 8, -2}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularSomaMatrizes(matrizUm, matrizDois);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeSomaMatrizesQuadradas() {
        //Arrange
        int[][] matrizUm = {{1, 1, 3}, {9, 0, 1}, {-2, 8, 1}};
        int[][] matrizDois = {{1, 2, 9}, {9, 8, -3}, {0, 23, 53}};
        int[][] expected = {{2, 3, 12}, {18, 8, -2}, {-2, 31, 54}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularSomaMatrizes(matrizUm, matrizDois);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeSomaMatrizesVazias() {
        //Arrange
        int[][] matrizUm = {};
        int[][] matrizDois = {};

        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularSomaMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    // Testes alínea C
    @Test
    public void testeProdutoMatrizesPrimeiraVazia() {
        //Arrange
        int[][] matrizUm = {};
        int[][] matrizDois = {{1, 2, 3, 4}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeProdutoMatrizesSegundaMatrizNull() {
        //Arrange
        int[][] matrizUm = {{12, 12, 12, 45}};
        int[][] matrizDois = null;
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeProdutoMatrizesNaoQuadradasNemRetangulares() {
        //Arrange
        int[][] matrizUm = {{12, 1, 1}, {12, 45}, {0}};
        int[][] matrizDois = {{0, 0, 1}, {1, 2}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeMatrizesComColunasELinhasDiferentes() {
        //Arrange
        int[][] matrizUm = {{12, 1, 1}, {12, 45, 56}, {0, 0, 1}};
        int[][] matrizDois = {{0, 0}, {1, 2}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizes(matrizUm, matrizDois);
        //Assert
        assertNull(result);
    }

    @Test
    public void testeMatrizesProduto() {
        //Arrange
        int[][] matrizUm = {{2, 3}, {5, 6}};
        int[][] matrizDois = {{1, 3}, {4, 2}};
        int[][] expected = {{14, 12}, {29, 27}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizes(matrizUm, matrizDois);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeMatrizesProdutoDois() {
        //Arrange
        int[][] matrizUm = {{2, 3}, {0, 1}, {-1, 4}};
        int[][] matrizDois = {{1,2,3}, {-2,0,4}};
        int[][] expected = {{-4, 4,18}, {-2,0,4}, {-9,-2,13}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizes(matrizUm, matrizDois);
        //Assert
        assertArrayEquals(expected, result);
    }
    @Test
    public void testeMatrizesProdutoTres() {
        //Arrange
        int[][] matrizUm = {{2, 3}, {0, 1}, {-1, 4}};
        int[][] matrizDois = {{1,2,3}, {-2,0,4}};
        int[][] expected = {{-4, 4,18}, {-2,0,4}, {-9,-2,13}};
        int[][] result = {};
        //Act
        result = ExercicioDezassete.calcularProdutoMatrizes(matrizUm, matrizDois);
        //Assert
        assertArrayEquals(expected, result);
    }

}
