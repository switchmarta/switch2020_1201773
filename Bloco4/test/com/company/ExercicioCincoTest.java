package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExercicioCincoTest {
    //Testes exercicio 5
    @Test
    public void testeObterSomaPares() {
        //Arrange
        int expected = 14;
        //Act
        int result = ExercicioCinco.obterSomaDigitos(6251423, 0);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testeObterSomaSoPares() {
        //Arrange
        int num = 688;
        int algarismo = 0;
        int expected = 22;
        int result = -1;
        //Act
        result = ExercicioCinco.obterSomaDigitos(num, algarismo);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testeObterSomaImpares() {
        //Arrange
        int num = 6251423;
        int algarismo = 1;
        int expected = 9;
        int result = -1;
        //Act
        result = ExercicioCinco.obterSomaDigitos(num, algarismo);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testeObterSomaSoImpares() {
        //Arrange
        int num = 75139;
        int algarismo = 1;
        int expected = 25;
        int result = -1;
        //Act
        result = ExercicioCinco.obterSomaDigitos(num, algarismo);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testeObterMenosUmDevidoNumNegativo() {
        //Arrange
        int num = -67;
        int algarismo = 1;
        int expected = -1;
        int result = -1;
        //Act
        result = ExercicioCinco.obterSomaDigitos(num, algarismo);
        //Assert
        assertEquals(expected, result);
    }

}
