package com.company;

public class ExercicioDoze {

    /**
     * Exercicio 12 : verificar se todas as linhas da matriz têm as mesmas colunas.
     * @param matriz
     * @return
     */
    public static int verificarNumColunasMatriz(int matriz[][]) {
        if (matriz.length == 0) {
            return -1;
        }
        int countColunas = matriz[0].length;
        for (int linha = 0; linha < matriz.length; linha++) {
            if(matriz[linha].length != countColunas) {
                return -1;
            }
        }
        return countColunas;
    }
    }
