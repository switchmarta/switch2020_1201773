package com.company;

public class ExercicioDois {
    public static void main(String[] args) {
        obterArray(14);

    }

    public static int[] obterArray(int num) {
        int tamanho = ExercicioUm.retonarTamanhoNum(num);
        int[] arr = new int[tamanho];
        if (num < 0) {
            return null;
        }
        for (int i= (tamanho-1) ; i >=0  ; i--) {
            arr[i] = num % 10;
            num /= 10;
        }
        return arr;
    }

}
