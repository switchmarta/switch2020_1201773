package com.company;

public class ExercicioDez {

    /**
     * Exercício 10 alínea A: Método para retornar menor valor de um vetor.
     *
     * @param num
     * @return int do menor valor do vetor.
     */
    public static int obterMenorValor(int[] num) {
        int menorValor = num[0];
        for (int i = 0; i < num.length; i++) {
            if (num[i] < menorValor) {
                menorValor = num[i];
            }
        }
        return menorValor;
    }

    /**
     * Exercício 10 alínea B: Método para retornar maior valor de um vetor.
     *
     * @param num
     * @return int do maior valor do vetor.
     */
    public static int obterMaiorValor(int[] num) {
        int maiorValor = num[0];
        for (int i = 0; i < num.length; i++) {
            if (num[i] > maiorValor) {
                maiorValor = num[i];
            }
        }
        return maiorValor;
    }

    /**
     * Exercicio 10 alínea C: retornar valor médio de todos elementos do vetor.
     *
     * @param num
     * @return double do valor médio do vetor.
     */
    public static double obterValorMedioElementos(int[] num) {
        double resultado, soma = 0;
        int tamanho = num.length;
        for (int i = 0; i < tamanho; i++) {
            soma += num[i];
        }
        resultado = soma / (tamanho * 1.0);
        return resultado;
    }

    /**
     * Exercicio 10 alínea D: retornar valor do produto de todos elementos do vetor.
     *
     * @param num
     * @return double produto dos elementos do vetor
     */
    public static double obterValorProdutoElementos(int[] num) {
        double produto = 1;
        int tamanho = num.length;
        for (int i = 0; i < tamanho; i++) {
            produto *= num[i];
        }
        return produto;
    }

    /**
     * Exercicio 10 alínea E: retornar elementos do vetor não repetidos.
     *
     * @param num
     * @return vetor com os elementos que nunca repetem
     */
    public static int[] obterVectorComValoresNaoRepetidos(int[] num) {
        int j = 0;
        int[] numUnicos = new int[num.length];
        for (int i = 0; i < num.length; i++) {
            int count = 0;
            for (int k = 0; k < num.length; k++) {
                if (num[i] == num[k] && i != k) {
                    count++; //continuar se número for repetido.
                }

            }
            if (count == 0) {//caso não seja repetido, guardar esse num.
                numUnicos[j] = num[i];
                j++;
            }
        }
        return ExercicioSeis.obterApenasNElementosVetor(numUnicos, j);
    }

    /**
     * Exercício 10 alínea F: Método para retornar um vector com os números invertidos.
     *
     * @param num
     * @return vetor invertido
     */
    public static int[] obterVetorInvertido(int[] num) {
        int k = 0;
        int[] numRevertido = new int[num.length];
        for (int i = (num.length - 1); i >= 0; i--) {
            numRevertido[k] = num[i];
            k++;
        }
        return numRevertido;
    }

    /**
     * Exercício 10 Alínea G - obter números primos. Ter em atenção que, os números primos só podem ser
     * inteiros positivos!
     * @param num
     * @return retorna vetor com números primos
     */
    public static int[] obterNumPrimos(int[] num) {
        int k = 0;
        int[] numPrimos = new int[num.length];
        for (int i = 0; i < num.length; i++) {
            int count = 0;
            if (num[i] <= 1) {
                count++;
            } else {
                for (int j = 2; j < num[i]; j++) { //procurar desde o 2 até ao número do array.
                    if (num[i] % j == 0) { //se isto acontece, é porque é divisivel por outro nº.
                        count++;
                    }
                }
            }
            if (count == 0) {//se não se verificar nenhuma das outras condições, é porque é nº primo.
                numPrimos[k] = num[i];
                k++;
            }
        }
        return ExercicioSeis.obterApenasNElementosVetor(numPrimos, k);
    }
}
