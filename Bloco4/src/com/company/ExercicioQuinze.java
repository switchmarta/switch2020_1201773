package com.company;

public class ExercicioQuinze {
    /**
     * Exercício 15 alínea A: verificar qual menor valor dentro da matriz.
     *
     * @param matriz
     * @return
     */
    public static int obterMenorValorDeMatriz(int[][] matriz) {
        int menorValor = matriz[0][0];
        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] < menorValor) {
                    menorValor = matriz[i][j];
                }
            }
        return menorValor;
    }

    /**
     * Exercício 15 Alínea B - obter maior valor de uma matriz.
     *
     * @param matriz
     * @return
     */
    public static int obterMaiorValorNumaMatriz(int[][] matriz) {
        int maiorValor = matriz[0][0];
        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] > maiorValor) {
                    maiorValor = matriz[i][j];
                }
            }
        return maiorValor;
    }

    /**
     * Exercício 15 Alínea C - calcular valor médio dos elementos da matriz, dividido em dois métodos.
     *
     * @param matriz
     * @return
     */

    public static double obterValorMedioElementosMatriz(int[][] matriz) {
        int soma = 0;
        double valorMedio;
        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; j < matriz[i].length; j++) {
                soma += matriz[i][j];
            }
        valorMedio = soma / (double) (obterTotalElementosMatriz(matriz));
        return valorMedio;
    }

    public static int obterTotalElementosMatriz(int[][] matriz) {
        int totalElementos = 0;

        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; j < matriz[i].length; j++) {
                totalElementos++;
            }
        return totalElementos;
    }

    /**
     * Exercício 15 Alínea D - obter produto dos elementos de uma matriz.
     *
     * @param matriz
     * @return
     */
    public static double obterProdutoElementosMatriz(int[][] matriz) {
        int produto = 1;
        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; j < matriz[i].length; j++) {
                produto *= matriz[i][j];
            }
        return produto;
    }

    /**
     * Exercício 15 alínea E, parte Um. Para verificar se existe o nº no vetor.
     *
     * @param vetor
     * @param num
     * @return true ou false se existe valor ou não.
     */
    public static boolean existeValor(int[] vetor, int num) {

        if (vetor == null) {
            return false;
        }
        boolean valor = false;
        int pos = 0;
        while (pos < vetor.length && !valor) {
            if (vetor[pos] == num) {
                valor = true;
            }
            pos++;
        }
        return valor;
    }

    /**
     * Exercício 15 alínea E - parte Dois. Adicionar um número ao vetor.
     *
     * @param vetor
     * @param num
     * @return array com o resultado de números adicionados.
     */
    public static int[] adicionarVetor(int[] vetor, int num) {
        int tamanho = 1;
        if (vetor != null) {
            tamanho += vetor.length;
        }
        int[] resultado = new int[tamanho];
        for (int pos = 0; pos < tamanho - 1; pos++) {
            resultado[pos] = vetor[pos];
        }
        resultado[tamanho - 1] = num;
        return resultado;
    }

    /**
     * Exercício 15 alínea E - obter o conjunto de números repetidos de uma matriz
     *
     * @param matriz
     * @return array com resultado números não repetidos.
     */
    public static int[] devolverElementosNaoRepetidos(int[][] matriz) {
        if (matriz == null) {
            return null;
        }
        int[] numUnicos = new int[0];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (!existeValor(numUnicos, matriz[i][j])) {
                    numUnicos = adicionarVetor(numUnicos, matriz[i][j]);

                }
            }
        }
        return numUnicos;
    }

    /**
     * Exercício 15 alínea F. Verificar números primos.
     *
     * @param matriz
     * @return vetor com números primos que foram encontrados na matriz.
     */
    public static int[] devolverElementosNumPrimos(int[][] matriz) {
        int l = 0;
        int size = obterTotalElementosMatriz(matriz);
        int[] numPrimos = new int[size];
        if (matriz == null) {
            return null;
        }
        for (int i = 0; i < matriz.length; i++) {
            for (int k = 0; k < matriz[i].length; k++) {
                int count = 0;
                if (matriz[i][k] <= 1) {
                    count++;
                } else {
                    for (int j = 2; j < matriz[i][k]; j++) {
                        if (matriz[i][k] % j == 0) {
                            count++;
                        }
                    }
                }
                if (count == 0) {//se não se verificar nenhuma das outras condições, é porque é nº primo.
                    numPrimos[l] = matriz[i][k];
                    l++;
                }
            }
        }
        return ExercicioSeis.obterApenasNElementosVetor(numPrimos, l);
    }

    /**
     * Exercício 15 alínea G - retorna apenas a diagonal principal da matriz.
     *
     * @param matriz
     * @return vetor com valores da diagonal.
     */

    public static int[] retornarDiagonalMatriz(int[][] matriz) {
        int size = obterTotalElementosMatriz(matriz);
        int[] diagonal = new int[size];
        int k = 0;
        if (ExercicioCatorze.eMatrizRetangular(matriz) == false && ExercicioTreze.verificarMatrizQuadrada(matriz) == false) {
            return null;
        }
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (i == j) {
                    diagonal[k] = matriz[i][j];
                    k++;
                }
            }
        }
        return ExercicioSeis.obterApenasNElementosVetor(diagonal, k);
    }

    /**
     * Exercício 15 alínea H - obter diagonal secundária de matriz.
     *
     * @param matriz
     * @return array com diagonal secundária.
     */
    public static int[] obterDiagonalSecundariaMatriz(int[][] matriz) {
        if (!ExercicioCatorze.eMatrizRetangular(matriz) && !ExercicioTreze.verificarMatrizQuadrada(matriz)) {
            return null;
        }
        int size = obterTotalElementosMatriz(matriz);
        int[] diagonalSecundaria = new int[size];
        int k = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if ((i + j) == (matriz[i].length - 1)) {//quando a soma dos indices dá igual
                    diagonalSecundaria[k] = matriz[i][j];//ao tamanho da coluna -1, equivale aos nº da diagonal.
                    k++;
                }
            }
        }
        return ExercicioSeis.obterApenasNElementosVetor(diagonalSecundaria, k);
    }

    /**
     * Exercício 15 alínea I - verificar se matriz dada é matriz identidade ou não.
     *
     * @param matriz
     * @return true ou falsa, caso seja matriz identidade ou não.
     */
    public static boolean eMatrizIdentidade(int[][] matriz) {
        boolean matrizIdentidade = true;
        if (ExercicioTreze.verificarMatrizQuadrada(matriz) == false) {
            matrizIdentidade = false;
        }
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (eDiagonalComUm(matriz) == false || eMatrizComZerosExcetoDiagonal(matriz) == false) {
                    matrizIdentidade = false;

                }
            }
        }
        return matrizIdentidade;
    }

    /**
     * Exercício 15 alínea I - PARTE UM. para verificar se a diagonal preenchida com um's é verdadeira ou falsa.
     *
     * @param matriz
     * @return true ou false se tiver diagonal 1 ou não.
     */
    public static boolean eDiagonalComUm(int[][] matriz) {
        boolean umDiagonal = true;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (i == j && matriz[i][j] != 1) {
                    umDiagonal = false;
                }
            }
        }
        return umDiagonal;
    }

    /**
     * Exercício 15 alínea I. PARTE DOIS. verificar se restantes elementos à exceção da diagonal, são todos zeros.
     *
     * @param matriz
     * @return true ou false, se são todos zeros ou não.
     */
    public static boolean eMatrizComZerosExcetoDiagonal(int[][] matriz) {
        boolean zeroMatriz = true;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (i != j && matriz[i][j] != 0) {
                    zeroMatriz = false;
                }
            }
        }
        return zeroMatriz;
    }

    /**
     * Exercício 15 alínea J - determinar matriz inversa.
     * @param matriz
     * @return a matriz inversa.
     */
    public static double[][] obterMatrizInversa(int[][]matriz) {
        if (!ExercicioDezasseis.eMatrizQuadradaPreenchida(matriz)) {
            return null;
        }
        double [][] matrizAdjunta = obterMatrizAdjunta(matriz);
        double[][]matrizInversa = new double[matriz.length][matriz[0].length];
        if(ExercicioDezasseis.calcularDeterminante(matriz) == 0) {
            return null; //matriz não é invertível.
        } else {
            for (int index = 0; index < matrizAdjunta.length; index++) {
                for (int col = 0; col < matrizAdjunta[index].length; col++) {
                    matrizInversa[index][col] = matrizAdjunta[index][col]/(ExercicioDezasseis.calcularDeterminante(matriz));
                }

            }
        }
        return matrizInversa;
    }
    public static double[][] obterMatrizAdjunta(int[][]matriz) {
        if (!ExercicioDezasseis.eMatrizQuadradaPreenchida(matriz)) {
            return null;
        }
        int[][] matrizCofatores = new int[matriz.length][matriz[0].length];
        for (int index = 0; index < matriz.length; index++) {
            for (int col = 0; col < matriz[index].length; col++) {
                matrizCofatores[index][col] = (int)Math.pow(-1, (1 + (col+1)))*matriz[index][col]*ExercicioDezasseis.calcularDeterminante(ExercicioQuinze.obterMatrizReduzida(matriz,index,col));
            }
        }
        double[][]matrizAdjunta = obterMatrizTransposta(matrizCofatores);
        return matrizAdjunta;
    }

    public static int[][] obterMatrizReduzida(int[][]matriz, int i, int j) {
        if (!ExercicioDezasseis.eMatrizQuadradaPreenchida(matriz) || matriz.length <=2) {
            return matriz;
        }
        int m=0;
        boolean trocaLinha = false;
        int tamanho = matriz.length;
        int[][] matrizReduzida = new int[tamanho - 1][tamanho - 1];
        for (int index = 0; index < tamanho; index++) {
            int k = 0;
            trocaLinha = false;
            for (int col = 0; col < tamanho; col++) {
                if (col !=  j && index!= i) { // se for igual ao número que estamos a calcular determinante na coluna.
                    matrizReduzida[m][k] = matriz[index][col];
                    k++;
                    trocaLinha = true;
                }
                }
            if(trocaLinha) {
                m++;
            }
            }
        return matrizReduzida;
    }

    /**
     * Exercício 15 alínea K. obter matriz transposta
     *
     * @param matriz
     * @return matriz transposta.
     */
    public static double[][] obterMatrizTransposta(int[][] matriz) {

        if (ExercicioCatorze.eMatrizRetangular(matriz) == false && ExercicioTreze.verificarMatrizQuadrada(matriz) == false) {
            return null;
        }
        double[][] resultado = new double[matriz[0].length][matriz.length];
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                resultado[coluna][linha] = matriz[linha][coluna];
            }
        }
        return resultado;
    }
}


