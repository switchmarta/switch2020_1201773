package com.company;

public class ExercicioCatorze {

    public static boolean eMatrizRetangular(int[][] matriz) {
        boolean matrizRetangular = true;
        if (matriz.length == 0) {
            matrizRetangular = false;
        } else if (ExercicioDoze.verificarNumColunasMatriz(matriz) == -1) {
            matrizRetangular = false;
        } else if (ExercicioDoze.verificarNumColunasMatriz(matriz) == matriz.length) {
            matrizRetangular = false;
            return matrizRetangular;
        }
        return matrizRetangular;
    }
}
