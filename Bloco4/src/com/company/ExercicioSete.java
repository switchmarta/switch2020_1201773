package com.company;

public class ExercicioSete {
    public static void main(String[] args) {
        //obterMultiplosNumeroDadoNoIntervalo(0,10,3);

    }
    public static int[] obterMultiplosNumeroDadoNoIntervalo(int limit1, int limit2, int algarismo) {
        int countMultiplos = 0;
        for (int i = limit1; i <= limit2; i++) {
            if (i % algarismo == 0 && i!=0) {
                countMultiplos++;
            }
        }
        int [] multiplos = new int[countMultiplos];
        int countMultiplosArray = 0;
        for (int i = limit1; i <= limit2; i++) {
            if (i % algarismo == 0 && i != 0) {
                multiplos[countMultiplosArray] = i;
                countMultiplosArray++;
            }
        }
        return multiplos;
    }
}
