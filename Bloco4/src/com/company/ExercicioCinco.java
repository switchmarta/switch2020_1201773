package com.company;

public class ExercicioCinco {

    public static void obterSomaParesImpares(int num, int algarismo) {
        if (num <0) {
            System.out.println("Número Inválido!");
        }
        int somaPares = obterSomaDigitos(num, 0);
        int somaImpares = obterSomaDigitos(num, 1);
        System.out.println(somaPares);
        System.out.println(somaImpares);
    }
    public static int obterSomaDigitos(int num, int algarismo) {
        int somaDigitosNum = 0;
        if (num <0) {
            return -1;
        }
        while (num > 0) {
            if ((num % 10) % 2 == algarismo) {
                somaDigitosNum += (num % 10);
            }
            num /= 10;
        }
        return somaDigitosNum;
    }
    }
