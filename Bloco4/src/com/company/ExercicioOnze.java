package com.company;

public class ExercicioOnze {
    /**
     * Exercicio 11 retornar produto escalar de dois vetores.
     * Os vetores têm que ter o mesmo tamanho.
     * @param vetorUm
     * @param vetorDois
     * @return inteiro do produto escalar de dois vetores.
     */
    public static Integer obterProdutoEscalarVetores(int[] vetorUm, int[] vetorDois) {
        int produtoEscalar = 0;
        if (vetorUm == null || vetorDois == null) {
            return null;
        } else if (vetorUm.length != vetorDois.length) {
            return -1;//Não deveria de ser -1 , porque pode ser resultado de um produto escalar.
            //Deveria retornar excepção ?
        }
        for (int i = 0; i < vetorUm.length; i++) {
            produtoEscalar += (vetorUm[i] * vetorDois[i]);
        }
        return produtoEscalar;
    }
}