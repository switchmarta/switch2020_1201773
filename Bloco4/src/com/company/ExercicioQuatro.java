package com.company;

public class ExercicioQuatro {
    public static void main(String[] args) {
        //obterArrayNumParEImpar();

    }

    public static void obterArrayNumPar(int[] num) {
        int []parArray = obterParesImparDeArrayDado(num, 0);
        int []imparArray = obterParesImparDeArrayDado(num, 1);
    }


    public static int[] obterParesImparDeArrayDado(int[] num, int algarismo) {
        int countPar = 0;
        if (num == null) {
            return null;
        }
        for (int i = 0; i < num.length; i++) {
            if (num[i] % 2 == algarismo) {
                countPar++;
            }
        }
        int[] parArray = new int[countPar];
        int countParArray = 0;
        for (int i = 0; i < num.length; i++) {
            if (num[i] % 2 == algarismo) {
                parArray[countParArray] = num[i];
                countParArray++; //para poder gravar o nº na posição seguinte.

            }
        }
        return parArray;
    }

}
