package com.company;

public class ExercicioUm {

    public static int retonarTamanhoNum(int num) {
        int quantidade = 1;
        if (num < 0) {
            return -1;
        }
        while (num > 9) {
            num /= 10;

            quantidade += 1;
        }
        return quantidade;
    }
}
