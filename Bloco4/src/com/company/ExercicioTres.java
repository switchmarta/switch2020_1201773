package com.company;

public class ExercicioTres {

    public static int somaElementosArray(int[] num) {
        int soma = 0;
        if (num == null) {
            return -1;
        }
        for (int i=0; i < num.length; i++) {
            soma += num[i];
        }
        return soma;
    }
}
