package com.company;

public class ExercicioOito {
    public static void main(String[] args) {
        int[] n = {2, 3};
        obterMultiplosNumComunsDadoNoIntervalo(4, 12, n);

    }

    public static int[] obterMultiplosNumComunsDadoNoIntervalo(int limit1, int limit2, int[] n) {
        int[] multiploArray = new int[limit2-limit1];

        int k = 0;
        for (int i = limit1; i <= limit2; i++) {
            int countMultiploArray = 0;
            for (int j = 0; j < n.length; j++) {
                if (i % n[j] == 0 && i!=0) {
                    countMultiploArray++;

                }
            }

            if (countMultiploArray == n.length) { //se for igual a quantidade de multiplos do n.
                multiploArray[k] = i; //guardar os multiplos
                k++;

            }
        }
        return ExercicioSeis.obterApenasNElementosVetor(multiploArray, k);

    }
}

