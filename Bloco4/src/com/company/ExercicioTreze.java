package com.company;

public class ExercicioTreze {
    /**
     * Exercício 13: método para verificar se matriz é quadrada.
     * @param matriz
     * @return
     */
    public static boolean verificarMatrizQuadrada(int[][] matriz) {
        boolean matrizQuadrada = true;
        if(matriz.length == 0) {
            matrizQuadrada = false;
        }
        for (int linha = 0; linha < matriz.length && matrizQuadrada == true; linha++) {
            if (ExercicioDoze.verificarNumColunasMatriz(matriz) != matriz.length) {
                matrizQuadrada = false;
            }
        }
        return matrizQuadrada;
    }
}
