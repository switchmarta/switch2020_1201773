package com.company;

public class ExercicioNove {
    /**
     * Exercício 9 - verificar se array é capicua ou não.
     * @param num
     * @return booleano, caso seja capicua ou não.
     */
    public static boolean obterCapicua(int[] num) {
        boolean capicua = true;
        int[] numRevertido = new int[num.length];
        int k = 0;
        if (num[0] < 0) {
            capicua = false;
        } else if (num.length == 1) {
            return capicua = false;
        } else {
            for (int i = (num.length - 1); i >= 0; i--) {
                numRevertido[k] = num[i];
                k++;
            }
            for (int i = 0; i < num.length; i++) {
                if (numRevertido[i] != num[i]) {
                    capicua = false;
                }
            }

        }
        return capicua;
    }
}
