package com.company;

public class ExercicioDezoito {
    public static void main(String[] args) {
        // String palavra = "gato";
        // existeDirecaoDireita(palavra);
    }

    /**
     * Exercício 18 alínea A - procurar uma letra e devolver matriz máscara com a posição dessa letra na matriz.
     *
     * @param sopaLetras
     * @param letra
     * @return matriz máscara. 0 significa que não existe letra. 1 significa que existe.
     */
    public static int[][] obterMatrizMascaraComLetraPretendida(int[][] sopaLetras, char letra) {
        if (letra == ' ' || sopaLetras == null) {
            return null;
        }
        int[][] matrizMascara = new int[sopaLetras.length][sopaLetras[0].length];
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, letra)) {
                    matrizMascara[linha][coluna] = 1;
                } else {
                    matrizMascara[linha][coluna] = 0;
                }
            }
        }
        return matrizMascara;
    }

    /**
     * Exercício 18 alínea A - Método para verificar se existe letra na matriz ou não.
     *
     * @param sopaLetras
     * @param linha
     * @param coluna
     * @param letra
     * @return true ou false caso exista ou não.
     */
    public static boolean existeLetraMatriz(int[][] sopaLetras, int linha, int coluna, char letra) {
        boolean existeLetra = false;
        char letraRegistada = Character.toUpperCase(letra);
        if (letraRegistada == ' ') {
            existeLetra = false;
        } else if (sopaLetras[linha][coluna] == letraRegistada) {
            existeLetra = true;
        }
        return existeLetra;
    }
    /**
     * Exercício 18 alínea B - encontrar palavra na sopa letra em várias direções.
     * Foram feitos vários métodos para cada direção. No total são 8 direções.
     * @param sopaLetras
     * @param palavra
     * @return true ou false se encontrar palavra.
     */
    public static boolean existePalavraMatriz(int[][] sopaLetras, String palavra) {
        boolean existePalavra = false;
        if (sopaLetras == null || palavra == null) {
            existePalavra = false;
        }
        if(existeNumaDirecao(sopaLetras,palavra)) {
            existePalavra = true;
        }
        return existePalavra;
    }

    /**
     * Exercício 18 alínea b
     *
     * Este método é para verificar em cada método de direção se existe a palavra.
     * @param sopaLetras
     * @param palavra
     * @return true ou false se encontrar palavra.
     */

    public static boolean existeNumaDirecao(int[][] sopaLetras, String palavra) {
        if (sopaLetras == null || palavra == null) {
            return false;
        }
        boolean existeDirecao = false;
        String palavraRegistada = palavra.toUpperCase();
        if (existeDirecaoDireita(sopaLetras, palavraRegistada)) {
            existeDirecao = true;
        } else if (existeDirecaoEsquerda(sopaLetras, palavraRegistada)) {
            existeDirecao = true;
        } else if(existeDirecaoCima(sopaLetras, palavraRegistada)) {
            existeDirecao = true;
        } else if (existeDirecaoBaixo(sopaLetras, palavraRegistada)) {
            existeDirecao = true;
        } else if (existeDirecaoEsquerdaBaixo(sopaLetras, palavraRegistada)) {
            existeDirecao = true;
        } else if (existeDirecaoDireitaBaixo(sopaLetras, palavraRegistada)) {
            existeDirecao = true;
        } else if (existeDirecaoDireitaCima(sopaLetras, palavraRegistada)) {
            existeDirecao = true;
        } else if (existeDirecaoEsquerdaCima(sopaLetras,palavraRegistada)) {
            existeDirecao = true;
        } else {
            existeDirecao = false;
        }
        return existeDirecao;
    }


    public static boolean existeDirecaoDireita(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoDireita = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoDireita = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {//Se existir a primeira letra na sopa letras,
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) { //vamos procurar o resto das letras à direita.
                    for (int index = 0; index < palavra.length() && (index + coluna) < sopaLetras[linha].length && !existeDirecaoDireita; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha][coluna + index]) {
                            contador++; //Se encontrar a letra, continuar na direção.
                            if (contador == arrayPalavra.length) { //Se o contador for igual ao tamanho da palavra, palavra encontrada!
                                existeDirecaoDireita = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoDireita;
    }

    public static boolean existeDirecaoEsquerda(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoEsquerda = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoEsquerda = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) {
                    for (int index = 0; index < palavra.length() && (coluna - index) >= 0 && !existeDirecaoEsquerda; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha][coluna - index]) {
                            contador++;
                            if (contador == arrayPalavra.length) {
                                existeDirecaoEsquerda = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoEsquerda;
    }

    public static boolean existeDirecaoCima(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoCima = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoCima = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) {
                    for (int index = 0; index < palavra.length() && (linha - index) >= 0 && !existeDirecaoCima; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha - index][coluna]) {
                            contador++;
                            if (contador == arrayPalavra.length) {
                                existeDirecaoCima = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoCima;
    }

    public static boolean existeDirecaoBaixo(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoBaixo = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoBaixo = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) {
                    for (int index = 0; index < palavra.length() && (linha + index) < sopaLetras.length && !existeDirecaoBaixo; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha + index][coluna]) {
                            contador++;
                            if (contador == arrayPalavra.length) {
                                existeDirecaoBaixo = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoBaixo;
    }

    public static boolean existeDirecaoEsquerdaCima(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoEsquerdaCima = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoEsquerdaCima = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) {
                    for (int index = 0; index < palavra.length() && (linha - index) >= 0 && (coluna - index) >= 0 && !existeDirecaoEsquerdaCima; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha - index][coluna - index]) {
                            contador++;
                            if (contador == arrayPalavra.length) {
                                existeDirecaoEsquerdaCima = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoEsquerdaCima;
    }

    public static boolean existeDirecaoDireitaCima(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoDireitaCima = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoDireitaCima = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) {
                    for (int index = 0; index < palavra.length() && (linha - index) >= 0 && (coluna + index) < sopaLetras[linha].length && !existeDirecaoDireitaCima; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha - index][coluna + index]) {
                            contador++;
                            if (contador == arrayPalavra.length) {
                                existeDirecaoDireitaCima = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoDireitaCima;
    }

    public static boolean existeDirecaoEsquerdaBaixo(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoEsquerdaBaixo = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoEsquerdaBaixo = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) {
                    for (int index = 0; index < palavra.length() && (linha + index) < sopaLetras.length && (coluna - index) >= 0 && !existeDirecaoEsquerdaBaixo; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha + index][coluna - index]) {
                            contador++;
                            if (contador == arrayPalavra.length) {
                                existeDirecaoEsquerdaBaixo = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoEsquerdaBaixo;
    }

    public static boolean existeDirecaoDireitaBaixo(int[][] sopaLetras, String palavra) {
        boolean existeDirecaoDireitaBaixo = false;
        if (sopaLetras == null || palavra == null) {
            existeDirecaoDireitaBaixo = false;
        }
        char[] arrayPalavra = palavra.toCharArray();
        int contador = 0;
        for (int linha = 0; linha < sopaLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaLetras[linha].length; coluna++) {
                if (existeLetraMatriz(sopaLetras, linha, coluna, arrayPalavra[0])) {
                    for (int index = 0; index < palavra.length() && (linha + index) < sopaLetras.length && (coluna + index) < sopaLetras[linha].length && !existeDirecaoDireitaBaixo; index++) {
                        if (arrayPalavra[index] == sopaLetras[linha + index][coluna + index]) {
                            contador++;
                            if (contador == arrayPalavra.length) {
                                existeDirecaoDireitaBaixo = true;
                            }
                        }
                    }
                }
            }
        }
        return existeDirecaoDireitaBaixo;
    }
}
