package com.company;

public class ExercicioDezassete {
    /**
     * Exercício 17 alínea a - calcular produto matriz por uma constante.
     *
     * @param matriz
     * @param constante
     * @return matriz com cálculo do produto com constante.
     */
    public static int[][] calcularProdutoMatrizComConstante(int[][] matriz, int constante) {
        if (matriz.length == 0) {
            return null;
        }
        int[][] resultado = new int[matriz.length][matriz[0].length];
        if (eMatrizRetangularOuQuadrada(matriz)) {
            //presumi que matriz só pode ser quadrada ou retangular.
            for (int linha = 0; linha < matriz.length; linha++) {
                for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                    resultado[linha][coluna] = matriz[linha][coluna] * constante;
                }
            }
        } else {
            return null;
        }
        return resultado;
    }

    /**
     * Método para saber se matriz é quadrada ou retangular
     *
     * @param matriz
     * @return true ou false se for quadrada ou retangular ou nenhuma das duas.
     */
    public static boolean eMatrizRetangularOuQuadrada(int[][] matriz) {
        boolean matrizRetangularQuadrada = true;
        if (matriz == null || matriz.length == 0) {
            matrizRetangularQuadrada = false;
        }
        if (ExercicioCatorze.eMatrizRetangular(matriz) || ExercicioTreze.verificarMatrizQuadrada(matriz)) {
            matrizRetangularQuadrada = true;
        } else {
            matrizRetangularQuadrada = false;
        }
        return matrizRetangularQuadrada;
    }

    /**
     * Exercício 17 alínea b - soma de matrizes.
     *
     * @param matrizUm
     * @param matrizDois
     * @return matriz resultante da soma.
     */
    public static int[][] calcularSomaMatrizes(int[][] matrizUm, int[][] matrizDois) {
        if (matrizUm == null || matrizDois == null || matrizUm.length == 0 || matrizDois.length == 0) {
            return null;
        }
        int colunasMatrizUm = ExercicioDoze.verificarNumColunasMatriz(matrizUm);
        int colunasMatrizDois = ExercicioDoze.verificarNumColunasMatriz(matrizDois);
        int[][] somaMatrizes = new int[matrizUm.length][matrizUm[0].length];
        if (colunasMatrizUm == colunasMatrizDois && matrizUm.length == matrizDois.length) {
            for (int linha = 0; linha < matrizUm.length; linha++) {
                for (int coluna = 0; coluna < matrizUm[linha].length; coluna++) {
                    somaMatrizes[linha][coluna] = matrizUm[linha][coluna] + matrizDois[linha][coluna];
                }
            }
        } else {
            return null;
        }
        return somaMatrizes;
    }

    /**
     * Exercício 17 alínea c - calcular produto de duas matrizes.
     * @param matrizUm
     * @param matrizDois
     * @return matriz com o resultado do produto.
     */
    public static int[][] calcularProdutoMatrizes(int[][] matrizUm, int[][] matrizDois) {
        if (matrizUm == null || matrizDois == null || matrizUm.length == 0 || matrizDois.length == 0) {
            return null;
        }
        //o número das colunas da primeira matriz tem que ser igual ao número de linhas da segunda matriz.
        int[][] produtoMatrizes = new int[matrizUm.length][matrizDois[0].length];
        if (eMatrizRetangularOuQuadrada(matrizUm) && eMatrizRetangularOuQuadrada(matrizDois)) {
            if (matrizUm[0].length == matrizDois.length) {
                for (int linha = 0; linha < matrizUm.length; linha++) {
                    for (int coluna = 0; coluna < matrizDois[0].length; coluna++) { //matrizDois[0], porque tem que ler a última coluna.
                        //Cada espaço da matriz resultado, vai ser a soma dos produtos elementos da linha matrizUm com coluna matrizDois
                        for (int i = 0; i < matrizUm[linha].length; i++) { //até à coluna da matrizUm que está a multiplicar.
                            produtoMatrizes[linha][coluna] += (matrizUm[linha][i] * matrizDois[i][coluna]);
                        }
                    }
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
        return produtoMatrizes;
    }
}
