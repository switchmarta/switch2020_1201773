package com.company;

public class ExercicioSeis {

    public static int[] obterApenasNElementosVetor(int[] num, int n) {
        int [] resultado = new int[n];
        int countDigito = 0;
        if (num == null) {
            return null;
        }
        for (int i = 0; i <n; i++) {
            resultado[countDigito]= num[i];
            countDigito++;
        }
      return resultado;
    }
}
