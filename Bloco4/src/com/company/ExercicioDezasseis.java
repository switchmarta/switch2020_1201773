package com.company;

public class ExercicioDezasseis {
    public static boolean eMatrizQuadradaPreenchida(int[][] matriz) {
        boolean matrizQuadradaPreenchida = true;
        if (matriz == null || matriz.length == 0 || !ExercicioTreze.verificarMatrizQuadrada(matriz)) {
            matrizQuadradaPreenchida = false;
        }
        return matrizQuadradaPreenchida;
    }

    public static Integer calcularDeterminante(int[][] matriz) {
        if (!eMatrizQuadradaPreenchida(matriz)) {
            return null;
        }
        int tamanho = matriz.length;
        int determinante = 0;
        int k = 0, j=1;
        int[] primeiraLinhaNumero = obterPrimeiraLinhaMatriz(matriz);
        int[][] matrizReduzida;

        for (int i = 0; i < tamanho; i++) {
            if (tamanho == 2) {
                determinante = (matriz[0][0] * matriz[1][1]) - (matriz[0][1] * matriz[1][0]);
            } else {
                matrizReduzida = obterMatrizReduzida(matriz, k);
                determinante += (int)(Math.pow(-1, (1 + j)) * primeiraLinhaNumero[i] * calcularDeterminante(matrizReduzida));
                k++;
                j++;
            }
        }
        return determinante;
    }

    public static int[][] obterMatrizReduzida(int[][] matriz, int j) {
        if (!eMatrizQuadradaPreenchida(matriz) || matriz.length <=2) {
            return null;
        }
        int tamanho = matriz.length;
        int[][] matrizReduzida = new int[tamanho - 1][tamanho - 1];
            for (int index = 1; index < tamanho; index++) { //vamos calcular sempre a partir da primeira linha.
                int k = 0;
                for (int col = 0; col < tamanho; col++) {
                    if (col != j) { // se for igual ao número que estamos a calcular determinante na coluna.
                        matrizReduzida[index - 1][k] = matriz[index][col];
                        k++;
                    }
                }
            }
        return matrizReduzida;
    }

    public static int[] obterPrimeiraLinhaMatriz(int[][] matriz) {
        if (!eMatrizQuadradaPreenchida(matriz)) {
            return null;
        }
        int[] primeiraLinha = matriz[0];

        return primeiraLinha;
    }
}
