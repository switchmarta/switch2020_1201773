package com.company;

import java.util.Scanner;

public class Bloco2 {

    public static void main(String[] args) {
        //exercicio1();
        //exercicio2();
        //exercicio3();
        //exercicio4();
        //exercicio5();
        //exercicio6();
        //exercicio2_V2(130);
        //exercicio1_V2(10, 15, 20, 10, 12, 14);
        //exercicio3_V2(1, 2, -4, 2);
        //exercicio4_V2(10);
        //obterVolumeCubo(20);
        //obterClassificacaoCubo(10);
        //obterHorasMinutosSeg(4000);
        //obterSaudacao(4000);
        //exercicio7();
        //exercicio8();
        //exercicio9();
        //exercicio10();
        //exercicio11();
        //exercicio12();
        //obterSuspensaoGrupoConsoanteIndicePoluicao(0.5);
        //exercicio13();
        //exercicio14();
        //exercicio15(1.5, 2, 3);
        //exercicio16();
        //exercicioDezassete(40);
        //exercicioVinte("Segunda-feira", "C", 4);
    }

    public static void exercicioUm() {
        double nota1, nota2, nota3, peso1, peso2, peso3;
        double mediapesada;
        //Relação com o teclado/utilizador.
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir nota");
        nota1 = ler.nextInt();
        System.out.println("Introduzir peso");
        peso1 = ler.nextInt();
        System.out.println("Introduzir nota");
        nota2 = ler.nextInt();
        System.out.println("Introduzir peso");
        peso2 = ler.nextInt();
        System.out.println("Introduzir nota");
        nota3 = ler.nextInt();
        System.out.println("Introduzir peso");
        peso3 = ler.nextInt();

        mediapesada = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

        if (mediapesada >= 8) {
            System.out.println("Teve " + mediapesada + "\n Cumpre os requisitos mínimos!");
        } else
            System.out.println("Teve " + mediapesada + "\n Tem que estudar mais!");

    }

    //Apenas com processamento
    public static double exercicioUmVersaoDois(double nota1, double peso1, double nota2, double peso2, double nota3, double peso3) {
        double mediapesada;
        mediapesada = ((nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);
        return mediapesada;
    }

    public static void exercicioDois() {
        int num, digito1, digito2, digito3;

        //Relação com o teclado/utilizador.
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir número");
        num = ler.nextInt();

        if (num < 100 || num > 999) {
            System.out.println("O número não tem 3 dígitos!");
        } else {
            digito3 = (num % 10);
            digito2 = (num / 10) % 10;
            digito1 = (num / 100) % 10;
            if (num % 2 == 0) {
                System.out.println("O número inserido é " + digito1 + " " + digito2 + " " + digito3 + " e é um número par");
            } else {
                System.out.println("O número inserido é " + digito1 + " " + digito2 + " " + digito3 + " e é um número ímpar");
            }
        }
    }

    public static int exercicioDoisVersaoDois(int num) {
        int result;
        int digito3, digito2, digito1;

        // Se o nº tiver menos de 3 dígitos ou mais de 3 dígitos, retorna 0.
        // Se tiver 3 dígitos, retorna 1.
        if (num < 100 || num > 999) {
            result = 0;
        } else {
            digito3 = (num % 10);
            digito2 = (num / 10) % 10;
            digito1 = (num / 100) % 10;
            result = 1;
        }
        return result;
    }

    public static void exercicioTres() {
        double x1, x2, y1, y2;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir x1");
        x1 = ler.nextDouble();
        System.out.println("Introduzir y1");
        y1 = ler.nextDouble();
        System.out.println("Introduzir x2");
        x2 = ler.nextDouble();
        System.out.println("Introduzir y2");
        y2 = ler.nextDouble();

        double distance, p1, p2;
        p1 = Math.pow(x1 - x2, 2);
        p2 = Math.pow(y1 - y2, 2);

        distance = Math.sqrt(p1 + p2);
        System.out.println("A distância entre os dois pontos é " + distance);

    }

    public static double exercicioTresVersaoDois(double x1, double y1, double x2, double y2) {
        double distance, p1, p2;
        p1 = Math.pow(x1 - x2, 2);
        p2 = Math.pow(y1 - y2, 2);
        distance = Math.sqrt(p1 + p2);

        return distance;

    }

    public static void exercicioQuatro() {
        double x, f_x;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir x");
        x = ler.nextDouble();

        if (x < 0) {
            f_x = x;
            System.out.println("O valor é " + f_x);
        } else if (x == 0) {
            f_x = 0;
            System.out.println("O valor é " + f_x);
        } else {
            f_x = Math.pow(x, 2) - 2 * x;
            System.out.println("O valor é " + f_x);
        }
    }

    public static double exercicioQuatroVersaoDois(double x) {
        double f_x;
        if (x < 0) {
            f_x = x;
        } else if (x == 0) {
            f_x = 0;
        } else {
            f_x = Math.pow(x, 2) - 2 * x;
        }
        return f_x;
    }

    //podemos utilizar os métodos dos processamentos que utilizamos para teste,
    //para este exercicio ficar mais pequeno! por exemplo, em vez de escrever os cálculos todos,
    //poderíamos colocar double volume = obterVolumeCubo(area);

    public static void exercicioCinco() {
        double area, aresta, volume;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir área");
        area = ler.nextDouble();

        if (area > 0) {
            aresta = Math.sqrt(area / 6.0);
            volume = Math.pow(aresta, 3);
            if (volume <= 1) {
                System.out.println("O volume do cubo é " + volume + " e é considerado pequeno");
            } else if (volume > 1 && volume <= 2) {
                System.out.println("O volume do cubo é " + volume + " e é considerado médio");
            } else
                System.out.println("O volume do cubo é " + volume + " e é considerado grande");
        } else {
            System.out.println("Valor da área incorreto!");
        }
    }

    //Para obter o volume dando uma area sem ser pelo utilizador.
    public static double obterVolumeCubo(double area) {
        if (area > 0) {
            double aresta = Math.sqrt(area / 6.0);
            double volume = Math.pow(aresta, 3);
            return volume;
        } else {
            return -1; //Se a área for negativa, retorna sempre -1.
        }
    }

    //Para obter a classificação do volume dando já um volume.
    public static String obterClassificacaoCubo(double volume) {
        double volumeDm = volume / 1000; //Passar de cm3 para dm3
        if (volumeDm < 0) {
            return "inválido";
        } else if (volumeDm <= 1) {
            return "pequeno";
        } else if (volumeDm > 1 && volumeDm <= 2) {
            return "médio";
        } else
            return "grande";
    }

    public static void exercicioSeis() {
        int hours, minutes, totalSeconds, seconds, minutesPartial;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir segundos");
        totalSeconds = ler.nextInt();
        // ex: 4000s
        hours = totalSeconds / 3600; //1h (.111111)
        minutesPartial = totalSeconds - (hours * 3600); //400
        minutes = minutesPartial / 60;//6 (.66666)
        seconds = minutesPartial - (minutes * 60); //40s
        System.out.println(hours + "h:" + minutes + "m:" + seconds + "s");
    }

    public static String obterHorasMinutosSeg(int totalSeconds) {
        int hours, minutes, seconds, minutesPartial;

        // ex: 4000
        hours = totalSeconds / 3600; //1 (.111111)
        minutesPartial = totalSeconds - (hours * 3600); //400
        minutes = minutesPartial / 60;//6 (.66666)
        seconds = minutesPartial - (minutes * 60); //40s
        String result = hours + "h:" + minutes + "m:" + seconds + "s";

        return result;
    }

    public static String obterSaudacao(int totalSeconds) {
        //Ex 4000s. Forma mais fácil de resolver será comparar strings (?) mas ainda não aprendemos.
        String saudacao = " ";
        if (totalSeconds <= 21600 && totalSeconds > 43201) {
            return "Bom dia";
        } else if (totalSeconds <= 43201 && totalSeconds > 72001) {
            return "Boa tarde";
        } else
            return "Boa noite";
    }

    public static void exercicioSete() {
        double area, salarioPintor, custoTinta, rendimentoLitro;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir área");
        area = ler.nextDouble();
        System.out.println("Introduzir salário pintor");
        salarioPintor = ler.nextDouble();
        System.out.println("Introduzir custo da tinta");
        custoTinta = ler.nextDouble();
        System.out.println("Introduzir rendimento da tinta por litro");
        rendimentoLitro = ler.nextDouble();

        double horasPintorArea = area / 2.0;
        double custoMaoObra = (horasPintorArea * salarioPintor) / 8.0;
        double litrosUsadosArea = (area / rendimentoLitro);
        double custoTintaArea = litrosUsadosArea * custoTinta;
        double custoTotal = 0;
        if (area <= 0 || salarioPintor < 0 || custoTinta < 0 || rendimentoLitro < 0) {
            System.out.println("Dados inválidos!");
        } else if (area > 0 && area < 100) {
            custoTotal = custoMaoObra + custoTintaArea;
            System.out.println("O custo total será de " + custoTotal + " euros");
        } else if (area >= 100 && area < 300) {
            custoTotal = (custoMaoObra * 2) + custoTintaArea;
            System.out.println("O custo total será de " + custoTotal + " euros");
        } else if (area >= 300 && area < 1000) {
            custoTotal = (custoMaoObra * 3) + custoTintaArea;
            System.out.println("O custo total será de " + custoTotal + " euros");
        } else {
            custoTotal = (custoMaoObra * 4) + custoTintaArea;
            System.out.println("O custo total será de " + custoTotal + " euros");
        }

    }

    public static double obterCustoTotalPintura(double area, double salarioPintor, double custoTinta, double rendimentoLitro) {
        double custoTintaArea = (area / rendimentoLitro) * custoTinta;
        double custoMaoObra = ((area / 2.0) * salarioPintor) / 8.0;
        double custoTotal = 0;
        if (area <= 0 || salarioPintor < 0 || custoTinta < 0 || rendimentoLitro < 0) {
            return -1; //Quando retorna -1 é porque foi inserido algum número negativo.
        } else if (area > 0 && area < 100) {
            return custoTotal = custoMaoObra + custoTintaArea;
        } else if (area >= 100 && area < 300) {
            return custoTotal = (custoMaoObra * 2) + custoTintaArea;
        } else if (area >= 300 && area < 1000) {
            return custoTotal = (custoMaoObra * 3) + custoTintaArea;
        } else {
            return custoTotal = (custoMaoObra * 4) + custoTintaArea;
        }
    }

    public static double obterCustoMaoObra(double area, double salarioPintor) {
        double custoMaoObra = ((area / 2.0) * salarioPintor) / 8.0;
        if (area <= 0 || salarioPintor < 0) {
            return -1; //Quando retorna -1 é porque foi inserido algum número negativo.
        } else if (area > 0 && area < 100) {
            return custoMaoObra;
        } else if (area >= 100 && area < 300) {
            return custoMaoObra * 2;
        } else if (area >= 300 && area < 1000) {
            return custoMaoObra * 3;
        } else {
            return custoMaoObra * 4;
        }
    }

    public static double obterCustoTintaPorArea(double area, double custoTinta, double rendimentoLitro) {
        double custoTintaArea = (area / rendimentoLitro) * custoTinta;
        if (area <= 0 || custoTinta < 0 || rendimentoLitro < 0) {
            return -1; //Quando retorna -1 é porque foi inserido algum número negativo.
        } else {
            return custoTintaArea;
        }

    }

    public static void exercicioOito() {
        int x, y;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir valor x");
        x = ler.nextInt();
        System.out.println("Introduzir valor y");
        y = ler.nextInt();
        if (x <= 0 || y <= 0) {
            System.out.println("Coloque número válido positivo!");
        } else if (x % y == 0) {
            System.out.println("X é múltiplo de Y.");
        } else if (y % x == 0) {
            System.out.println("Y é múltiplo de X.");
        } else {
            System.out.println("X não é múltiplo nem divisor de Y.");
        }
    }

    public static String verificarMultiploDivisor(int x, int y) {
        String resultado = " ";
        if (x <= 0 || y <= 0) {
            return resultado = "Coloque número válido positivo!";
        } else if (x % y == 0) {
            return resultado = "X é múltiplo de Y.";
        } else if (y % x == 0) {
            return resultado = "Y é múltiplo de X.";
        } else {
            return resultado = "X não é múltiplo nem divisor de Y.";
        }
    }

    public static void exercicioNove() {
        int num, digito1, digito2, digito3;

        //Relação com o teclado/utilizador.
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir número");
        num = ler.nextInt();

        if (num < 100 || num > 999) {
            System.out.println("O número não é válido!");
        } else {
            digito3 = (num % 10);
            digito2 = (num / 10) % 10;
            digito1 = (num / 100) % 10;
            if (digito1 < digito2 && digito2 < digito3) {
                System.out.println("Sequência de algarismos crescente.");
            } else {
                System.out.println("Sequência de algarismos não crescente.");
            }
        }
    }

    public static String obterNumCrescente(int num) {
        int digito1, digito2, digito3;
        String result = " ";

        if (num < 100 || num > 999) {
            return result = "Não válido";
        } else {
            digito3 = (num % 10);
            digito2 = (num / 10) % 10;
            digito1 = (num / 100) % 10;
            if (digito1 < digito2 && digito2 < digito3) {
                return result = "Sequência de algarismos crescente.";
            } else {
                return result = "Sequência de algarismos não crescente.";
            }
        }
    }

    public static void exercicioDez() {
        double price, discount;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir preço artigo");
        price = ler.nextDouble();
        if (price <= 0) {
            System.out.println("Dado inválido!");
        } else if (price > 200) {
            discount = price * 0.4;
            System.out.println("O preço do artigo com desconto fica " + discount + " euros");
        } else if (price > 100) {
            discount = price * 0.6;
            System.out.println("O preço do artigo com desconto fica " + discount + " euros");
        } else if (price > 50) {
            discount = price * 0.7;
            System.out.println("O preço do artigo com desconto fica " + discount + " euros");
        } else {
            discount = price * 0.8;
            System.out.println("O preço do artigo com desconto fica " + discount + " euros");
        }

    }

    public static double obterDescontoArtigo(double price) {
        double discount;
        if (price <= 0) {
            return -1; //Quando retorna -1, é porque foi colocado número negativo ou o 0.
        } else if (price > 200) {
            discount = price * 0.4;
            return discount;
        } else if (price > 100) {
            discount = price * 0.6;
            return discount;
        } else if (price > 50) {
            discount = price * 0.7;
            return discount;
        } else {
            discount = price * 0.8;
            return discount;
        }

    }

    public static void exercicioOnze() {
        double aprovados;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor");
        aprovados = ler.nextDouble();
        if (aprovados < 0 || aprovados > 1) {
            System.out.println("Valor inválido!");
        } else if (aprovados < 0.2) {
            System.out.println("Turma má!");
        } else if (aprovados < 0.5) {
            System.out.println("Turma fraca!");
        } else if (aprovados < 0.7) {
            System.out.println("Turma razoável!");
        } else if (aprovados < 0.9) {
            System.out.println("Turma boa!");
        } else {
            System.out.println("Turma excelente!");
        }
    }

    public static String obterClassificacaoTurma(double aprovados) {
        String resultado = " ";
        if (aprovados < 0 || aprovados > 1) {
            return resultado = "Valor inválido!";
        } else if (aprovados < 0.2) {
            return resultado = "Turma má!";
        } else if (aprovados < 0.5) {
            return resultado = "Turma fraca!";
        } else if (aprovados < 0.7) {
            return resultado = "Turma razoável!";
        } else if (aprovados < 0.9) {
            return resultado = "Turma boa!";
        } else {
            return resultado = "Turma excelente!";
        }

    }

    public static void exercicioOnze_limitesFlexiveis() {
        double aprovados, limite1, limite2, limite3, limite4;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor");
        aprovados = ler.nextDouble();
        System.out.println("Introduza o limite");
        limite1 = ler.nextDouble();
        System.out.println("Introduza o segundo limite");
        limite2 = ler.nextDouble();
        System.out.println("Introduza o terceiro limite");
        limite3 = ler.nextDouble();
        System.out.println("Introduza o quarto limite");
        limite4 = ler.nextDouble();
        if (aprovados < 0 || aprovados > 1) {
            System.out.println("Valor inválido!");
        } else if (aprovados < limite1) {
            System.out.println("Turma má!");
        } else if (aprovados < limite2) {
            System.out.println("Turma fraca!");
        } else if (aprovados < limite3) {
            System.out.println("Turma razoável!");
        } else if (aprovados < limite4) {
            System.out.println("Turma boa!");
        } else {
            System.out.println("Turma excelente!");
        }
    }

    public static void exercicioDoze() {
        double indicePoluicao;
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique índice poluição: ");
        indicePoluicao = ler.nextDouble();

        if (indicePoluicao < 0) {
            System.out.println("Índice inválido!");
        } else if (indicePoluicao >= 0 && indicePoluicao <= 0.3) {
            System.out.println("Índice poluição aceitável.");
        } else if (indicePoluicao > 0.3 && indicePoluicao <= 0.4) {
            System.out.println("Grupo 1 terá que suspender atividade.");
        } else if (indicePoluicao > 0.4 && indicePoluicao <= 0.5) {
            System.out.println("Grupo 1 e 2 terão que suspender atividade.");
        } else {
            System.out.println("Todos os grupos terão que suspender atividade.");
        }
    }

    public static String obterSuspensaoGrupoConsoanteIndicePoluicao(double indicePoluicao) {
        String resultado = " ";
        if (indicePoluicao < 0) {
            return resultado = "Índice inválido!";
        } else if (indicePoluicao >= 0 && indicePoluicao <= 0.3) {
            return resultado = "Índice poluição aceitável.";
        } else if (indicePoluicao > 0.3 && indicePoluicao <= 0.4) {
            return resultado = "Grupo 1 terá que suspender atividade.";
        } else if (indicePoluicao > 0.4 && indicePoluicao <= 0.5) {
            return resultado = "Grupo 1 e 2 terão que suspender atividade.";
        } else {
            return resultado = "Todos os grupos terão que suspender atividade.";
        }

    }

    public static void exercicioTreze() {
        int arvore, arbusto, grama;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a grama a colocar: ");
        grama = ler.nextInt();
        System.out.println("Indique as árvores a colocar: ");
        arvore = ler.nextInt();
        System.out.println("Indique os arbustos a colocar: ");
        arbusto = ler.nextInt();
        int totalHoras = (((grama * 300) + (600 * arvore) + (400 * arbusto)) / 60) / 60; //tem que dar 2h. com grama 20, arvore 2, arbusto 4
        int custoTotal = (10 * grama) + (20 * arvore) + (15 * arbusto) + (10 * totalHoras);
        System.out.println("As horas necessárias para este trabalho são " + totalHoras + "h" + " o que dá um custo de " + custoTotal + " €");
    }

    public static String getTotalHorasServico(int grama, int arvore, int arbusto) {
        if (grama < 0 || arvore < 0 || arbusto < 0) {
            return "Dado inválido"; //Quando algum dos nº inserido é negativo.
        } else {
            int totalHoras = (((grama * 300) + (600 * arvore) + (400 * arbusto)) / 60) / 60;
            return "A quantidade de horas necessárias é " + totalHoras;
        }
    }

    public static String getCustoTotalServico(int grama, int arvore, int arbusto, int totalHoras) {
        int custoTotal;
        if (grama < 0 || arvore < 0 || arbusto < 0 || totalHoras < 0) {
            return "Valor inválido";
        } else
            custoTotal = (10 * grama) + (20 * arvore) + (15 * arbusto) + (10 * totalHoras);
        return "O custo total é " + custoTotal;
    }

    public static void exercicioCatorze() {
        double d1, d2, d3, d4, d5;
        double distanciaTotal, mediaDistancia;
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a distância 1: ");
        d1 = ler.nextDouble();
        System.out.println("Indique a distância 2: ");
        d2 = ler.nextDouble();
        System.out.println("Indique a distância 3: ");
        d3 = ler.nextDouble();
        System.out.println("Indique a distância 4: ");
        d4 = ler.nextDouble();
        System.out.println("Indique a distância 5: ");
        d5 = ler.nextDouble();
        distanciaTotal = ((d1 + d2 + d3 + d4 + d5) * 1609) * 0.001; //Converter milhas em metros e metros em km.
        mediaDistancia = distanciaTotal / 5;

        System.out.println("A distância média equivale a " + mediaDistancia + " km");
    }

    public static double obterDistanciaMedia(double d1, double d2, double d3, double d4, double d5) {
        double distanciaTotal, mediaDistancia;
        distanciaTotal = ((d1 + d2 + d3 + d4 + d5) * 1609) * 0.001; //Converter milhas em metros e metros em km.

        return mediaDistancia = distanciaTotal / 5;
    }

    public static String exercicioQuinze(double a, double b, double c) {

        if (a <= 0 || b <= 0 || c <= 0 || a >= b + c || b >= a + c || c >= a + b) {
            return "Não é considerado triângulo.";
        } else if (a == b && b == c) {
            return "Triângulo é equilátero.";
        } else if ((a == b && b != c) || (a == c && a != b) || (b == c && c != a)) {
            return "Triângulo é isósceles.";
        } else
            return "Triângulo é escaleno.";
    }

    public static String exercicioDezasseis(double a, double b, double c) {
        if (a <= 0 || b <= 0 || c <= 0 || (a + b + c) != 180) {
            return "Não é possível ser triângulo.";
        } else if (a == 90 || b == 90 || c == 90) {
            return "Triângulo rectângulo.";
        } else if (a > 90 || b > 90 || c > 90) {
            return "Triângulo obtusângulo.";
        } else {
            return "Triângulo acutângulo.";
        }
    }

    public static String exercicioDezassete(int hPartida, int mPartida, int hDuracao, int mDuracao) {
        String dia = "Hoje";
        int chegada = (hPartida * 60) + mPartida + (hDuracao * 60) + mDuracao;
        int hChegada = chegada / 60;
        int mChegada = chegada % 60;
        if (hChegada > 23) {
            hChegada = hChegada - 24;
            dia = "Amanhã";
        }
        return hChegada + ":" + mChegada + " - " + dia;
    }

    public static String exercicioDezoito(int hInicial, int mInicial, int sInicial, int sDuracao) {
        int inicioProcessamento = (hInicial * 3600) + (mInicial * 60) + sInicial;
        int finalProcessamento = inicioProcessamento + sDuracao;
        int hFinal = finalProcessamento / 3600;
        int mFinal = (finalProcessamento % 3600) / 60;
        int sFinal = (finalProcessamento % 3600) % 60;
        return hFinal + ":" + mFinal + ":" + sFinal;
    }


    public static double exercicioDezanove(int hours) {
        double salario;
        if (hours <= 36) {
            salario = (7.5) * hours;
            return salario;
        }
        if (hours > 36 && hours <= 41) {
            salario = 270 + (-10 * (36 - hours));
            return salario;
        } else {
            salario = 270 + (-15 * (36 - hours));
            return salario;
        }
    }

    public static double exercicioVinte(String dia, String kit, double distancia) {
        if (dia == "Sábado" || dia == "Domingo" || dia == "Feriado") {
            if (kit == "A") {
                return 40 + (distancia * 2);
            } else if (kit == "B") {
                return 70 + (distancia * 2);
            } else if (kit == "C") {
                return 140 + (distancia * 2);
            } else {
                return 0; //retorna 0 quando kit não é A, B OU C.
            }
        } else {
            if (kit == "A") {
                return 30 + (distancia * 2);
            } else if (kit == "B") {
                return 50 + (distancia * 2);
            } else if (kit == "C") {
                return 100 + (distancia * 2);
            } else {
                return 0;
            }
        }
    }
}




