package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Bloco2Test {
    //testes exercício 1
    @Test
    public void testeMediaPesada1() {
        double nota1 = 10;
        double peso1 = 7;
        double nota2 = 15;
        double peso2 = 10;
        double nota3 = 19;
        double peso3 = 15;
        double expected = 15.8;
        double result = Bloco2.exercicioUmVersaoDois(nota1, peso1, nota2, peso2, nota3, peso3);

        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeMediaPesada2() {
        double nota1 = 2;
        double peso1 = 7;
        double nota2 = 8;
        double peso2 = 9;
        double nota3 = 6;
        double peso3 = 9;
        double expected = 5.6;
        double result = Bloco2.exercicioUmVersaoDois(nota1, peso1, nota2, peso2, nota3, peso3);

        assertEquals(expected, result, 0.1);
    }

    // testes exercício 2
    @Test
    public void exercicio2_V2() {
        //Arrange
        int num = 400;
        int expected = 1;
        // Act
        int result = Bloco2.exercicioDoisVersaoDois(num);

        assertEquals(expected, result);

    }

    @Test
    public void exercicio2_V2_2() {
        //Arrange
        int num = 10;
        int expected = 0;
        // Act
        int result = Bloco2.exercicioDoisVersaoDois(num);

        assertEquals(expected, result);

    }

    // testes exercicio 3
    @Test
    public void exercicio3_V2() {
        //Arrange
        double x1 = 1;
        double y1 = 2;
        double x2 = 1;
        double y2 = 4;
        double expected = 2;
        // Act
        double result = Bloco2.exercicioTresVersaoDois(x1, y1, x2, y2);

        assertEquals(expected, result, 0.01);

    }

    // testes exercício 4
    @Test
    public void exercicio4_V2() {
        double expected = -1;
        double result = Bloco2.exercicioQuatroVersaoDois(-1);
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void exercicio4_V2_2() {
        double expected = 80;
        double result = Bloco2.exercicioQuatroVersaoDois(10);
        assertEquals(expected, result, 0.1);
    }

    //testes exercício 5
    @Test
    public void obterVolumeCubo() {
        double expected = 6.08;
        double result = Bloco2.obterVolumeCubo(20);

        assertEquals(expected, result, 0.01);


    }

    @Test
    public void obterVolumeCubo2() {
        double expected = 1;
        double result = Bloco2.obterVolumeCubo(6);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void obterClassificacaoCubo() {
        String expected = "grande";
        String result = Bloco2.obterClassificacaoCubo(6080);
        assertEquals(expected, result);
    }

    @Test
    public void obterClassificacaoCubo2() {
        String expected = "médio";
        String result = Bloco2.obterClassificacaoCubo(1300);
        assertEquals(expected, result);
    }

    //exercício 6
    @Test
    public void obterHorasMinutosSegTeste() {
        String expected = "1h:6m:40s";
        String result = Bloco2.obterHorasMinutosSeg(4000);
        assertEquals(expected, result);
    }

    @Test
    public void testeBoaNoiteTotalQuatroMilSegundos() {
        //Arrange
        String expected = "Boa noite";
        //Act
        String result = Bloco2.obterSaudacao(4000);
        //Assert
        assertEquals(expected, result);
    }

    //Exercicio 7 - Custo total da Pintura
    @Test
    public void testObterCustoTotalPinturaComAreaVinte() {
        //Arrange
        double expected = 35;
        //Act
        double result = Bloco2.obterCustoTotalPintura(20, 20, 5, 10);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterCustoTotalPinturaComAreaCem() {
        //Arrange
        double expected = 300;
        //Act
        double result = Bloco2.obterCustoTotalPintura(100, 20, 5, 10);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterCustoTotalPinturaComAreaQuatrocentosCinquenta() {
        //Arrange
        double expected = 1912.5;
        //Act
        double result = Bloco2.obterCustoTotalPintura(450, 20, 5, 10);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterCustoTotalPinturaComAreaMilTrezentos() {
        //Arrange
        double expected = 7150;
        //Act
        double result = Bloco2.obterCustoTotalPintura(1300, 20, 5, 10);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterMenosUmDevidoAreaNegativa() {
        //Arrange
        double expected = -1;
        //Act
        double result = Bloco2.obterCustoTotalPintura(0, 20, 5, 10);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Exercicio 7, apenas custo da mão de obra.
    @Test
    public void testObterCustoMaoObraSalarioNegativo() {
        //Arrange
        double expected = -1;
        //Act
        double result = Bloco2.obterCustoMaoObra(100, -5);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterCustoMaoObraAreaCem() {
        //Arrange
        double expected = 250;
        //Act
        double result = Bloco2.obterCustoMaoObra(100, 20);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterCustoMaoObraAreaVinte() {
        //Arrange
        double expected = 25;
        //Act
        double result = Bloco2.obterCustoMaoObra(20, 20);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterCustoMaoObraAreaQuatrocentos() {
        //Arrange
        double expected = 1500;
        //Act
        double result = Bloco2.obterCustoMaoObra(400, 20);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testObterCustoMaoObraAreaMilTrezentos() {
        //Arrange
        double expected = 6500;
        //Act
        double result = Bloco2.obterCustoMaoObra(1300, 20);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Exercicio 7, apenas o custo da tinta por área dada.
    @Test
    public void testObterCustoTintaAreaCem() {
        //Arrange
        double expected = 50;
        //Act
        double result = Bloco2.obterCustoTintaPorArea(100, 5, 10);
        //Assert
        assertEquals(expected, result, 0.1);

    }

    @Test
    public void testObterCustoTintaRendimentoNegativo() {
        //Arrange
        double expected = -1;//Quando retorna -1, é porque foi colocado número negativo.
        //Act
        double result = Bloco2.obterCustoTintaPorArea(100, 5, -10);
        //Assert
        assertEquals(expected, result, 0.1);

    }

    // Testes exercicio 8.
    @Test
    public void testVerificarXMultiploDoze() {
        //Arrange
        String expected = "X é múltiplo de Y.";
        //Act
        String result = Bloco2.verificarMultiploDivisor(12, 2);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testVerificarYIgualZero() {
        //Arrange
        String expected = "Coloque número válido positivo!";
        //Act
        String result = Bloco2.verificarMultiploDivisor(10, 0);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testVerificarXNaoMultiploY() {
        //Arrange
        String expected = "X não é múltiplo nem divisor de Y.";
        //Act
        String result = Bloco2.verificarMultiploDivisor(9, 67);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercicio 9
    @Test
    public void testNumNaoValido() {
        //Arrange
        String expected = "Não válido";
        //Act
        String result = Bloco2.obterNumCrescente(32);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testNumNaoCrescente() {
        //Arrange
        String expected = "Sequência de algarismos não crescente.";
        //Act
        String result = Bloco2.obterNumCrescente(534);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testNumCrescente() {
        //Arrange
        String expected = "Sequência de algarismos crescente.";
        //Act
        String result = Bloco2.obterNumCrescente(468);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testNumNaoCrescenteComNumIguais() {
        //Arrange
        String expected = "Sequência de algarismos não crescente.";
        //Act
        String result = Bloco2.obterNumCrescente(223);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testDescontoArtigoNegativo() {
        //Arrange
        double expected = -1;//Quando retorna -1, é porque foi colocado número negativo ou o 0.
        //Act
        double result = Bloco2.obterDescontoArtigo(0);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testDescontoArtigoComSessentaDesconto() {
        //Arrange
        double expected = 89.2;
        //Act
        double result = Bloco2.obterDescontoArtigo(223);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testDescontoArtigoComQuarentaDesconto() {
        //Arrange
        double expected = 91.8;
        //Act
        double result = Bloco2.obterDescontoArtigo(153);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testDescontoArtigoComTrintaDesconto() {
        //Arrange
        double expected = 53.9;
        //Act
        double result = Bloco2.obterDescontoArtigo(77);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testDescontoArtigoComVinteDesconto() {
        //Arrange
        double expected = 33.6;
        //Act
        double result = Bloco2.obterDescontoArtigo(42);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    // Testes exercicio 11
    @Test
    public void testAprovadoNegativo() {
        //Arrange
        String expected = "Valor inválido!";
        //Act
        String result = Bloco2.obterClassificacaoTurma(-1);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testAprovadoTurmaMa() {
        //Arrange
        String expected = "Turma má!";
        //Act
        String result = Bloco2.obterClassificacaoTurma(0.1);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testAprovadoTurmaFraca() {
        //Arrange
        String expected = "Turma fraca!";
        //Act
        String result = Bloco2.obterClassificacaoTurma(0.34);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testAprovadoTurmaRazoavel() {
        //Arrange
        String expected = "Turma razoável!";
        //Act
        String result = Bloco2.obterClassificacaoTurma(0.6);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testAprovadoTurmaBoa() {
        //Arrange
        String expected = "Turma boa!";
        //Act
        String result = Bloco2.obterClassificacaoTurma(0.82);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testAprovadoTurmaExcelente() {
        //Arrange
        String expected = "Turma excelente!";
        //Act
        String result = Bloco2.obterClassificacaoTurma(0.98);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercicio 12
    @Test
    public void testIndiceNegativo() {
        //Arrange
        String expected = "Índice inválido!";
        //Act
        String result = Bloco2.obterSuspensaoGrupoConsoanteIndicePoluicao(-2);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testIndiceAceitavel() {
        //Arrange
        String expected = "Índice poluição aceitável.";
        //Act
        String result = Bloco2.obterSuspensaoGrupoConsoanteIndicePoluicao(0.24);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testCessarAtividadeGrupoUm() {
        //Arrange
        String expected = "Grupo 1 terá que suspender atividade.";
        //Act
        String result = Bloco2.obterSuspensaoGrupoConsoanteIndicePoluicao(0.38);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testCessarAtividadeGrupoUmEDois() {
        //Arrange
        String expected = "Grupo 1 e 2 terão que suspender atividade.";
        //Act
        String result = Bloco2.obterSuspensaoGrupoConsoanteIndicePoluicao(0.5);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testCessarAtividadeTodosGrupos() {
        //Arrange
        String expected = "Todos os grupos terão que suspender atividade.";
        //Act
        String result = Bloco2.obterSuspensaoGrupoConsoanteIndicePoluicao(0.71);
        //Assert
        assertEquals(expected, result);
    }

    //exercicio 13
    @Test
    public void testValorNegativo() {
        //Arrange
        int grama = 1;
        int arbusto = 4;
        int arvore = -2;
        //Arrange
        String expected = "Dado inválido";
        //Act
        String result = Bloco2.getTotalHorasServico(grama, arbusto, arvore);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testValorAleatorio() {
        //Arrange
        int grama = 20;
        int arbusto = 4;
        int arvore = 2;
        //Arrange
        String expected = "A quantidade de horas necessárias é " + 2;
        //Act
        String result = Bloco2.getTotalHorasServico(grama, arbusto, arvore);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testCustoTotalNegativo() {
        //Arrange
        int grama = -20;
        int arvore = 2;
        int arbusto = 4;
        int totalHoras = 2;
        String expected = "Valor inválido";
        //Act
        String result = Bloco2.getCustoTotalServico(grama, arvore, arbusto, totalHoras);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testCustoTotalTrezentosEVinte() {
        //Arrange
        int grama = 20;
        int arvore = 2;
        int arbusto = 4;
        int totalHoras = 2;
        String expected = "O custo total é " + 320;
        //Act
        String result = Bloco2.getCustoTotalServico(grama, arvore, arbusto, totalHoras);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testDistanciaMedia() {
        //Arrange
        double expected = 9.65;
        //Act
        double result = Bloco2.obterDistanciaMedia(2,4,6,8,10);
        //Assert
        assertEquals(expected, result, 0.01);
    }
    // Teste exercicio 15
    @Test
    public void testValorNegativoTriangulo() {
        //Arrange
        String expected = "Não é considerado triângulo.";
        //Act
        String result = Bloco2.exercicioQuinze(-2,4,6);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testValorDeAMaiorQueBeC() {
        //Arrange
        String expected = "Não é considerado triângulo.";
        //Act
        String result = Bloco2.exercicioQuinze(12,2,6);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testTrianguloEquilatero() {
        //Arrange
        String expected = "Triângulo é equilátero.";
        //Act
        String result = Bloco2.exercicioQuinze(2,2,2);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testTrianguloIsosceles() {
        //Arrange
        String expected = "Triângulo é isósceles.";
        //Act
        String result = Bloco2.exercicioQuinze(2,2,3);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testTrianguloEscaleno() {
        //Arrange
        String expected = "Triângulo é escaleno.";
        //Act
        String result = Bloco2.exercicioQuinze(1.5,2,3);
        //Assert
        assertEquals(expected, result);
    }
    //Testes exercicio 16
    @Test
    public void testTrianguloImpossivel() {
        //Arrange
        String expected = "Não é possível ser triângulo.";
        //Act
        String result = Bloco2.exercicioDezasseis(60,15,10);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testTrianguloRectangulo() {
        //Arrange
        String expected = "Triângulo rectângulo.";
        //Act
        String result = Bloco2.exercicioDezasseis(90,50,40);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testTrianguloObtusangulo() {
        //Arrange
        String expected = "Triângulo obtusângulo.";
        //Act
        String result = Bloco2.exercicioDezasseis(30,120,30);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testTrianguloAcutangulo() {
        //Arrange
        String expected = "Triângulo acutângulo.";
        //Act
        String result = Bloco2.exercicioDezasseis(60,70,50);
        //Assert
        assertEquals(expected, result);
    }
    // Testes exercicio 17
    @Test
    public void testChegarAmanha() {
        //Arrange
        String expected = "0:30 - Amanhã";
        //Act
        String result = Bloco2.exercicioDezassete(22, 30, 2, 0);
        //Assert
        assertEquals(expected, result);
    }
    @Test
    public void testChegarHoje() {
        //Arrange
        String expected = "17:45 - Hoje";
        //Act
        String result = Bloco2.exercicioDezassete(16, 15, 1, 30);
        //Assert
        assertEquals(expected, result);
    }
    // Testes exercicio 18
    @Test
    public void testHoraQueTermina() {
        //Arrange
        String expected = "17:15:30";
        //Act
        String result = Bloco2.exercicioDezoito(16, 15, 30, 3600);
        //Assert
        assertEquals(expected, result);
    }
    // Testes exercicio 19
    @Test
    public void testHorasTrabalhoMenorQueTrintaSeis() {
        //Arrange
        double expected = 22.5;
        //Act
        double result = Bloco2.exercicioDezanove(3);
        //Assert
        assertEquals(expected, result, 0.1);
    }
    @Test
    public void testHorasTrabalhoQuarenta() {
        //Arrange
        double expected = 310;
        //Act
        double result = Bloco2.exercicioDezanove(40);
        //Assert
        assertEquals(expected, result, 0.1);
    }
    @Test
    public void testHorasTrabalhoQuarentaEOito() {
        //Arrange
        double expected = 450;
        //Act
        double result = Bloco2.exercicioDezanove(48);
        //Assert
        assertEquals(expected, result, 0.1);
    }
    // testes exercicio 20
    @Test
    public void testDiasEPrecoServicoQuandoKitD() {
        //Arrange
        double expected = 0;
        //Act
        double result = Bloco2.exercicioVinte("Segunda-feira", "D", 4);
        //Assert
        assertEquals(expected, result, 0.1);
    }
    @Test
    public void testSegundaEPrecoServicoQuandoKitC() {
        //Arrange
        double expected = 106;
        //Act
        double result = Bloco2.exercicioVinte("Segunda-feira", "C", 3);
        //Assert
        assertEquals(expected, result, 0.1);
    }
    @Test
    public void testSabadoEPrecoServicoQuandoKitC() {
        //Arrange
        double expected = 144;
        //Act
        double result = Bloco2.exercicioVinte("Sábado", "C", 2);
        //Assert
        assertEquals(expected, result, 0.1);
    }
}