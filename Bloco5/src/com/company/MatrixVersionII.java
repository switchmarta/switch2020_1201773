package com.company;

public class MatrixVersionII {
    //Attributes
    private Vector[] matrix;

    //Constructors
    public MatrixVersionII() { //inicializa vetor vazio.
        this.matrix = new Vector[0];
    }

    public MatrixVersionII(Vector[] initialVector) { //
        this.matrix = createCopy(initialVector);
    }

    public MatrixVersionII(int[][] matrix) {
        if (matrix == null) {
            this.matrix = new Vector[0];
        }
        this.matrix = copyInt(matrix);
    }

    //Methods
    private Vector[] createCopy(Vector[] initialVector) {
        Vector[] copy;
        if (initialVector == null) {
            copy = new Vector[0];
        } else {
            copy = new Vector[initialVector.length];
            for (int i = 0; i < initialVector.length; i++) {
                copy[i] = initialVector[i];
            }
        }
        return copy;
    }

    private Vector[] copyInt(int[][] matrix) {
        Vector[] copy;
        if (matrix == null) {
            copy = new Vector[0];          //para evitar throw - nunca é null;
        } else {
            copy = new Vector[matrix.length];
            for (int i = 0; i < matrix.length; i++) {
                copy[i] = new Vector(matrix[i]);            //primeiro convertes matrizInical para ArrayUnidimensional. E depois é que igualas ao copy
            }
        }
        return copy;
    }

    public Vector[] toVector() {
        Vector[] result = createCopy(this.matrix);
        return result;
    }

    public int[][] toArray() {
        int[][] resultMatrix = new int[0][];
        if (this.matrix.length == 0) {
            resultMatrix[0] = new int[0];
        } else {
            resultMatrix = createCopyII(this.matrix);
        }
        return resultMatrix;
    }

    private int[][] createCopyII(Vector[] initialMatrix) {
        int[][] copy;
        if (initialMatrix == null) {            //Not allow null arrays in the class
            copy = new int[0][0];
        } else {
            copy = new int[initialMatrix.length][];
            for (int i = 0; i < initialMatrix.length; i++) {
                copy[i] = matrix[i].toArray();
            }
        }
        return copy;
    }

    //Equals Method
    @Override
    public boolean equals(Object o) {
        if (this == o) {            //Serem da mesma classe
            return true;
        }
        if (!(o instanceof MatrixVersionII)) {          //se são do mesmo tipo de variável
            return false;
        }

        MatrixVersionII other = (MatrixVersionII) o;        //converter o objeto(o) no tipo de classe criada

        if (this.matrix.length == (other.matrix.length)) {
            for (int i = 0; i < matrix.length; i++) {                //verifica se o conteúdo dentro do objeto é igual ao conteúdo do this.matrix
                if (!this.matrix[i].equals(other.matrix[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    //Business Method

    /**
     * Exercício 3 alínea c) adicionar um elemento a uma das linhas dos vetores dentro da matriz.
     *
     * @param value
     * @param indexLine
     */
    public void addElement(int value, int indexLine) {
        if (this.matrix.length == 0) {
            Vector[] newVector = new Vector[1];//construtor da minha classe vetor.
            newVector[0] = new Vector(value);
            this.matrix = newVector;
        } else {
            if (indexLine < 0 || indexLine >= this.matrix.length) {
                throw new IndexOutOfBoundsException("Negative Index.");
            } else {
                Vector result = this.matrix[indexLine];
                result.addElement(value);
            }
        }

    }

    /**
     * Exercício 3 alínea d) remover valor da matriz.
     *
     * @param value
     */
    public void removeValue(int value) {
        boolean valueRemoved = false;
        for (int i = 0; i < this.matrix.length && !valueRemoved; i++) {
            Vector result = matrix[i].clone();
            result.removeFirstElement(value);
            if (!result.equals(this.matrix[i])) {
                valueRemoved = true;
            }
            this.matrix[i] = result;
        }

    }

    public int highest() {
        int highest;
        Vector highests = new Vector();
        for (Vector vector : this.matrix) {
            highests.addElement(vector.highestNumber());
        }
        highest = highests.highestNumber();
        return highest;
    }

    public int smallest() {
        int smallest;
        Vector smallests = new Vector();
        for (Vector vector : this.matrix) {
            smallests.addElement(vector.smallestNumber());
        }
        smallest = smallests.smallestNumber();
        return smallest;
    }

    /**
     * Exercício 3 alínea h) média dos valores do vários (Vector) vetores dentro da matriz.
     *
     * @return double com média.
     */
    public double avg() {
        int sum = 0;
        int count = 0;
        double avg;
        if (this.matrix.length == 0) {
            avg = 0;
        } else {
            for (Vector vector : this.matrix) {
                sum += vector.sumElements();
                count += vector.countElements();
            }
            avg = sum * 1.0 / count;

        }
        return avg;
    }

    /**
     * Exercício 3 alínea i)
     *
     * @return
     */

    public Vector linesSum() {
        Vector result = new Vector();
        for (Vector vector :
                this.matrix) {
            int sumOfEach = vector.sumElements();
            result.addElement(sumOfEach);
        }
        return result;
    }

    //falta aqui a alínea j)

    /**
     * Exercício 3 alínea k) retornar indice do somatório mais elevado.
     *
     * @return index.
     */
    public int indexOfHighestSum() {
        Vector sums = this.linesSum();
        int index;
        int highest = sums.highestNumber();
        index = sums.indexOfValue(highest);
        return index;
    }

    /**
     * Exercício 3 alínea l) - verificar se matriz é quadrada.
     *
     * @return true ou false
     */
    public boolean isSquare() {
        boolean isSquare = true;
        for (int i = 0; i < this.matrix.length && isSquare; i++) {
            if (this.matrix[i].numberOfLength() != this.matrix.length) {
                isSquare = false;
            }
        }
        return isSquare;
    }

    /**
     * Faz a transposta da matriz, uma vez que necessitamos para outros exercícios.
     *
     * @return MatrixVersionII da transposta
     */
    public MatrixVersionII transposedMatrix() {
        if (isSquare()) {
            int linesInMatrix = this.matrix[0].numberOfLength();
            Vector[] transposed = new Vector[linesInMatrix];
            for (int i = 0; i < linesInMatrix; i++) {
                transposed[i] = getColumns(i);
            }
            MatrixVersionII transposedMatrix = new MatrixVersionII(transposed);
            return transposedMatrix;
        }
        return null;
    }

    /**
     * Obtem a coluna por index das linhas.
     *
     * @param index
     * @return Vector com a coluna.
     */
    private Vector getColumns(int index) {

        int[] valuesInColumn = new int[this.matrix.length];

        for (int i = 0; i < this.matrix.length; i++) {
            valuesInColumn[i] = this.matrix[i].valueOnPosition(index);
        }

        Vector column = new Vector(valuesInColumn);

        return column;

    }

    /**
     * Exercício 5 alínea m) verificar se matriz é simétrica ou não.
     *
     * @return true or false.
     */
    public boolean isSymmetric() {
        boolean isSymmetric = false;
        if (this.matrix.equals(this.transposedMatrix())) {
            isSymmetric = true;
        } else {
            isSymmetric = false;
        }
        return isSymmetric;
    }

    /**
     * Exercício 3 alínea n) - retornar contagem de elementos não nulos da diagonal.
     *
     * @return int count.
     */
    public int countNonNullElementsOfDiagonal() {
        int count = 0;
        if (!isSquare()) {
            count = -1;
        } else {
            MatrixVersionII matrixInitial = new MatrixVersionII(this.matrix);
            Vector diagonal = matrixInitial.getMainDiagonal();
            for (int i = 0; i < diagonal.numberOfLength(); i++) {
                if (diagonal.valueOnPosition(i) != 0) {
                    count++;
                }
            }
        }
        return count;
    }

    private int getValue(int i, int j) {
        if (i < 0 || j < 0 || i > this.matrix.length || j > matrix[i].numberOfLength()) {
            throw new ArrayIndexOutOfBoundsException("Out of bounds.");
        }
        return this.matrix[i].valueOnPosition(j);
    }

    /**
     * Obtém a diagonal principal da matriz.
     *
     * @return diagonal principal da matriz.
     */
    public Vector getMainDiagonal() {
        Vector result;
        if (isSquare()) {
            result = new Vector();
            for (int i = 0; i < this.matrix.length; i++) {
                result.addElement(getValue(i, i));
            }
            return result;
        } else {
            throw new IllegalArgumentException("Matrix should be square or rectangular.");
        }
    }

    /**
     * Obtém a diagonal secundária da matriz.
     *
     * @return diagonal secundária da matriz.
     */
    private Vector getSecondaryDiagonal() {
        Vector result;
        if (isSquare()) {
            result = new Vector();
            for (int i = 0; i < this.matrix.length; i++) {
                result.addElement(getValue(i, (this.matrix.length - i - 1)));
            }
            return result;
        } else {
            throw new IllegalArgumentException("Matrix should be square or rectangular.");
        }
    }

    /**
     * Exercício 3 alínea o) verificar se diagonal principal e secundária são iguais.
     *
     * @return true ou false.
     */
    public boolean mainDiagonalEqualsSecondary() {
        boolean areEquals = false;
        MatrixVersionII initialMatrix = new MatrixVersionII(this.matrix);
        if (initialMatrix.getMainDiagonal().equals(initialMatrix.getSecondaryDiagonal())) {
            areEquals = true;
        }
        return areEquals;
    }

    public double avgOfDigits() {
        int sum = 0;
        int count = 0;
        double avg;
        for (Vector vector :
                this.matrix) {
            sum += vector.sumElements();
            count++;
        }
        avg = sum * 1.0 / count;
        return avg;
    }

    public Vector valuesWithMoreDigits() {
        double averageOfDigits = this.avgOfDigits();
        Vector counts = new Vector();
        for (Vector vector :
                this.matrix) {
            vector.countEveryDigitElement(averageOfDigits);


        }
        return counts;
    }

    /**
     * Exercício 5 alínea r) inverter linhas de cada vector dentro da matriz.
     */
    public void invertMatrixLines() {
        for (int i = 0; i < this.matrix.length; i++) {
            Vector perLine = new Vector(this.matrix[i].toArray());
            Vector result = perLine.invertReturn();
            this.matrix[i] = result;
        }
    }

    /**
     * Exercício 5 alínea s) inverter colunas dentro da matriz.
     */
    public void invertMatrixColumns() { //Fazer de inverter colunas.
        for (int i = 0; i < this.matrix.length; i++) {
            Vector perLine = new Vector(this.matrix[i].toArray());
            Vector result = perLine.invertReturn();
            this.matrix[i] = result;
        }
    }

    /**
     * Exercicio 3 alínea t) rodar a matriz 90º para a direita. É o mesmo que inverter a transposta da matriz.
     */
    public void rotateNinetyDegrees() {
        if (isSquare()) {
            MatrixVersionII rotated = this.transposedMatrix();
            rotated.invertMatrixLines();
            this.matrix = rotated.matrix;
            }
        }
    }




