package com.company;

public class Utilities {
    /**
     * Adicionar um elemento ao vetor.
     *
     * @param v
     * @param number
     * @return
     */
    public static int[] addToVetor(int[] v, int number) {
        int size = 1;
        if (v != null) {
            size += v.length;
        }
        int[] result = new int[size];
        for (int position = 0; position < size - 1; position++) {   //garante que o vetor resultante guarda os elementos do vetor inicial
            result[position] = v[position];
        }
        result[size - 1] = number;     //insere o valor do elemento adicionado ao vetor resultante.
        return result;
    }

    public static int highestOfMatrix(int[][] matrix) {
        int highest = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length != 0) {
                int tempHighest = highestOfVector(matrix[i]);
                if (tempHighest > highest) {
                    highest = tempHighest;
                }

            }
        }
        return highest;
    }

    public static int highestOfVector(int[] vector) {
        int highestNumber = vector[0];
        for (int index = 1; index < vector.length; index++) {
            if (highestNumber < vector[index]) {
                highestNumber = vector[index];
            }

        }
        return highestNumber;
    }

    public static int smallestOfMatrix(int[][] matrix) {
        int smallest = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length != 0) {
                int tempSmallest = smallestOfVector(matrix[i]);
                if (tempSmallest < smallest) {
                    smallest = tempSmallest;
                }

            }
        }
        return smallest;
    }

    public static int smallestOfVector(int[] vector) {
        int smallestNumber = vector[0];
        for (int index = 1; index < vector.length; index++) {
            if (smallestNumber > vector[index]) {
                smallestNumber = vector[index];
            }

        }
        return smallestNumber;
    }

    public static double avgOfVector(int[] vector) {
        double avg;
        int count = 0;
        int sum = 0;
        for (int index = 0; index < vector.length; index++) {
            sum += vector[index];
            count++;
        }
        avg = sum * 1.0 / count * 1.0;
        return avg;
    }

    public static double avgOfMatrix(int[][] matrix) {
        double mean;
        int count = 0;
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                sum += matrix[i][j];
                count++;
            }
        }
        mean = (sum * 1.0) / (count * 1.0);
        return mean;
    }

    public static int arraySum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }





    public static int countDigits(int value) {
        int count = 0, positiveValue = value;
        if (value < 0) {
            positiveValue *= (-1);
        }
        while (positiveValue > 0) {
            positiveValue /= 10;
            count++;
        }
        return count;
    }

    public static double avgOfDigits(int[][] matrix) {
        double avg = 0;
        int[][] countAll = new int[matrix.length][];
        for (int index = 0; index < matrix.length; index++) {
            countAll[index] = new int[matrix[index].length];
            for (int col = 0; col < matrix[index].length; col++) {
                countAll[index][col] = countDigits(matrix[index][col]);
            }

        }
        avg = sumElements(countAll) * 1.0 / totalElements(matrix) * 1.0;
        return avg;

    }

    public static int totalElements(int[][]matrix) {
        int total = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                total++;
            }

        }

        return total;
    }

    public static int sumElements(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                sum += matrix[i][j];
            }

        }

        return sum;
    }
    public static int[] invert(int[] vector) {
        int[] inverted = new int[vector.length];
        for (int i = inverted.length - 1; i >= 0; i--) {
            inverted[(inverted.length-1) - i] = vector[i];
        }

        return inverted;
    }

}

