package com.company;

import java.util.Arrays;

public class Vector {
    //Attributes
    private int[] vector;

    //Constructors
    public Vector() {
        this.vector = new int[0];
    }

    public Vector(int[] vector) {
        this.vector = copyVector(vector);
    }

    public Vector(int value) {
        this.vector = new int[1];
        this.vector[0] = value;
    }
    //Methods Equals


    //Methods

    private boolean isNull() {
        return this.vector == null;
    }

    public boolean isEmpty() {
        return this.vector.length == 0;
    }

    private int[] copyVector(int[] vector) {
        int[] copyVector;
        if (vector == null) {
            copyVector = new int[0];
        } else {
            copyVector = new int[vector.length];
            for (int count = 0; count < vector.length; count++) {
                copyVector[count] = vector[count];
            }
        }
        return copyVector;
    }

    public Vector clone() { //impede referências partilhadas
        Vector clone = new Vector(vector);
        for (int i = 0; i < this.vector.length; i++) {
            clone.vector[i] = this.vector[i];
        }
        return clone;
    }

    public int[] toArray() {
        return this.copyVector(vector);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true; // serem mesma classe
        if (!(o instanceof Vector)) return false; // se são do mesmo tipo de variável

        Vector other = (Vector) o;
        if (this.vector.length == other.vector.length) {
            for (int i = 0; i < vector.length; i++) {
                if (this.vector[i] != other.vector[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void addElement(int value) {
        int size = 1;
        if (this.vector != null) {
            size += this.vector.length;
        }
        int[] result = new int[size];
        for (int position = 0; position < size - 1; position++) {   //garante que o vetor resultante guarda os elementos do vetor inicial
            result[position] = this.vector[position];
        }
        result[size - 1] = value;     //insere o valor do elemento adicionado ao vetor resultante.
        this.vector = result;
    }

    public void removeFirstElement(int value) {
        if (!isInVector(value)) {
            int[] result = new int[this.vector.length];
            result = this.vector;
        } else {
            int i = 0;
            int[] result = new int[this.vector.length - 1];
            boolean existsValue = false;
            for (int index = 0; index < this.vector.length; index++) {
                if (value == this.vector[index] && !existsValue) {
                    existsValue = true;
                } else {
                    result[i] = this.vector[index];
                    i++;
                }
            }
            this.vector = result;
        }

    }

    private boolean isInVector(int value) {
        boolean existsInVector = false;
        for (int i = 0; i < this.vector.length; i++) {
            if (value == this.vector[i] && !existsInVector) {
                existsInVector = true;
            }
        }
        return existsInVector;
    }

    public int valueOnPosition(int position) {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        if (position > this.vector.length) {
            throw new IllegalArgumentException("Position does not exist in this vector.");
        }
        return this.vector[position];
    }

    public int numberOfLength() {
        return this.vector.length;
    }

    public int indexOfValue(int value) {
        int index = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (value == vector[i]) {
                index = i;
            }
        }
        return index;
    }

    public void invert() {
        int[] inverted = new int[this.vector.length];
        for (int i = inverted.length - 1; i >= 0; i--) {
            inverted[(inverted.length - 1) - i] = vector[i];
        }
        this.vector = inverted;
    }

    public Vector invertReturn() {
        int[] invertedVector = new int[this.vector.length];
        for (int i = invertedVector.length - 1; i >= 0; i--) {
            invertedVector[(invertedVector.length - 1) - i] = vector[i];
        }
        Vector vectorResult = new Vector(invertedVector);
        return vectorResult;
    }

    public int highestNumber() {
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        int highestNumber = this.vector[0];
        for (int index = 0; index < this.vector.length; index++) {
            if (highestNumber < this.vector[index]) {
                highestNumber = this.vector[index];
            }

        }
        return highestNumber;
    }

    public int smallestNumber() {
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        int smallestNumber = this.vector[0];
        for (int index = 0; index < this.vector.length; index++) {
            if (smallestNumber > this.vector[index]) {
                smallestNumber = this.vector[index];
            }

        }
        return smallestNumber;
    }

    public double elementsAverage() {
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        double avg;
        int count = 0;
        int sum = 0;
        for (int index = 0; index < this.vector.length; index++) {
            sum += this.vector[index];
            count++;
        }
        avg = sum * 1.0 / count * 1.0;
        return avg;
    }

    public double averageOfEvenElements() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        double avg;
        int count = 0;
        int sum = 0;
        for (int index = 0; index < this.vector.length; index++) {
            if (this.vector[index] % 2 == 0 && this.vector[index] != 0) {
                sum += this.vector[index];
                count++;
            }
        }
        if (count > 0) {
            avg = sum * 1.0 / count * 1.0;
        } else {
            avg = 0;
        }
        return avg;

    }

    public double averageOfOddElements() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        double avg;
        int count = 0;
        int sum = 0;
        for (int index = 0; index < vector.length; index++) {
            if (vector[index] % 2 != 0 && vector[index] != 0) {
                sum += vector[index];
                count++;
            }
        }
        if (count > 0) {
            avg = sum * 1.0 / count * 1.0;
        } else {
            avg = 0;
        }
        return avg;

    }

    public double averageMultiplesOfNumber(int number) {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        double avg;
        int count = 0;
        int sum = 0;
        for (int index = 0; index < this.vector.length; index++) {
            if (this.vector[index] % number == 0 && this.vector[index] != 0) {
                sum += this.vector[index];
                count++;
            }
        }
        if (count > 0) {
            avg = sum * 1.0 / count * 1.0;
        } else {
            avg = 0;
        }
        return avg;

    }

    public void orderNumberByDesc() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        int temp;
        for (int index = 0; index < this.vector.length; index++) {
            for (int index2 = index + 1; index2 < this.vector.length; index2++) {
                if (this.vector[index] < this.vector[index2]) {
                    temp = this.vector[index];
                    this.vector[index] = this.vector[index2];
                    this.vector[index2] = temp;
                }
            }
        }
    }

    public void orderNumberByAsc() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        int temp;
        for (int index = 0; index < this.vector.length; index++) {
            for (int index2 = index + 1; index2 < this.vector.length; index2++) {
                if (this.vector[index] > this.vector[index2]) {
                    temp = this.vector[index];
                    this.vector[index] = this.vector[index2];
                    this.vector[index2] = temp;
                }
            }
        }
    }

    public boolean hasOnlyOneElement() {
        return this.vector.length == 1;
    }

    public boolean hasOnlyEvenElements() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        boolean onlyEven = true;
        for (int index = 0; index < this.vector.length && onlyEven; index++) {
            if (vector[index] % 2 != 0) {
                onlyEven = false;
            }
        }
        return onlyEven;
    }

    public boolean hasOnlyOddElements() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        boolean onlyOdd = true;
        for (int index = 0; index < this.vector.length && onlyOdd; index++) {
            if (vector[index] % 2 == 0) {
                onlyOdd = false;
            }
        }
        return onlyOdd;
    }

    public boolean hasDuplicatedElements() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        boolean duplicatedNumbers = false;
        for (int index = 0; index < this.vector.length; index++) {
            for (int index2 = index + 1; index2 < this.vector.length && !duplicatedNumbers; index2++) {
                if (vector[index] == vector[index2]) {
                    duplicatedNumbers = true;
                }
            }
        }
        return duplicatedNumbers;
    }

    public int countDigits(int value) {
        int count = 0, positiveValue = value;
        if (value < 0) {
            positiveValue *= (-1);
        }
        while (positiveValue > 0) {
            positiveValue /= 10;
            count++;
        }
        return count;
    }

    public int countElements() {
        int count = 0;
        for (int element : this.vector) {
            count++;
        }
        return count;
    }

    public Vector countEveryDigitElement(double avgDigits) {
        Vector counts = new Vector();
        for (int element : this.vector) {
            if (Utilities.countDigits(element) > avgDigits) ;
            counts.addElement(countDigits(element));
        }
        return counts;
    }

    public int sumElements() {
        int sum = 0;
        for (int i = 0; i < this.vector.length; i++) {
            sum += countDigits(vector[i]);
        }
        return sum;
    }

    public int sumElementsOne(int[] vector) {
        int sum = 0;
        for (int i = 0; i < this.vector.length; i++) {
            sum += this.vector[i];
        }
        return sum;
    }

    private int avgOfDigits() {
        double avg;
        int total = this.vector.length;
        int[] countAll = new int[this.vector.length];
        for (int index = 0; index < this.vector.length; index++) {
            countAll[index] = countDigits(vector[index]);
        }
        avg = sumElementsOne(countAll) * 1.0 / total * 1.0;
        return (int) avg;
    }

    private int avgOfEvenDigits() {
        double avg;
        int total = this.vector.length;
        int[] countAll = new int[this.vector.length];
        for (int index = 0; index < this.vector.length; index++) {
            countAll[index] = percentageEven(convertDigitsToArray(vector[index]));
        }
        avg = sumElementsOne(countAll) * 1.0 / total * 1.0;
        return (int) avg;
    }

    public int[] numbersWithMoreDigitsThanTheAverage() {
        if (isNull() || isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Vector not valid.");
        }
        int avgOfDigits = avgOfDigits();
        Vector result = new Vector(); //evitar chamar vector vazios.
        for (int index = 0; index < this.vector.length; index++) {
            if (countDigits(vector[index]) > avgOfDigits) {
                result.addElement(vector[index]);
            }

        }
        return result.toArray();

    }

    public int[] percentageEvenDigitsGreaterThanTheAverage() {
        int[] result = new int[0]; //evitar chamar vector vazios.
        for (int index = 0; index < this.vector.length; index++) {
            if (percentageEven(convertDigitsToArray(vector[index])) > avgOfEvenDigits()) {
                result = Utilities.addToVetor(result, vector[index]);
            }

        }
        return result;
    }

    private int[] convertDigitsToArray(int value) {
        int numberDigits = countDigits(value);
        int[] temp = new int[numberDigits];
        for (int i = (numberDigits - 1); i >= 0; i--) {
            temp[i] = value % 10;
            value /= 10;
        }
        return temp;
    }

    private int percentageEven(int[] temp) {
        double percentageEven;
        int count = 0;
        int numberDigits = temp.length;
        for (int index = 0; index < temp.length; index++) {
            if (temp[index] % 2 == 0 && temp[index] != 0) {
                count++;
            }
        }
        percentageEven = (count * 1.0 / numberDigits * 1.0) * 100;
        return (int) percentageEven;
    }


    /**
     * Exercício 1 alínea u) retorna elementos vetor compostos exclusivamente por
     * algarismos pares
     *
     * @return int[] dos valores do vetor
     */
    public int[] numberWithOnlyEvenDigits() {
        int[] result = new int[0]; //evitar chamar vector vazios.
        for (int index = 0; index < this.vector.length; index++) {
            if (percentageEven(convertDigitsToArray(vector[index])) == 100) {
                result = Utilities.addToVetor(result, vector[index]);
            }

        }
        return result;
    }

    /**
     * Exercício 1 alínea v) retorna elementos do vetor com algarismo sequência crescente.
     *
     * @return int[] do vetor com esses valores.
     */
    public int[] sequenceElements() {
        int[] result = new int[0];
        for (int index = 0; index < this.vector.length; index++) {
            if (isSequence(convertDigitsToArray(vector[index]))) {
                result = Utilities.addToVetor(result, vector[index]);
            }
        }
        return result;

    }

    private boolean isSequence(int[] vector) {
        boolean isSequence = true;
        if (vector.length == 1) {
            isSequence = false;
        }
        for (int index = 0; index < vector.length - 1 && isSequence; index++) {
            if (vector[index] >= vector[index + 1]) {
                isSequence = false;
            }

        }
        return isSequence;
    }

    /**
     * Exercicio 1 alínea w) retorna capicuas existentes no vetor.
     *
     * @return int[] com as capicuas resultantes.
     */
    public int[] getPalindromes() {
        int[] result = new int[0];
        for (int index = 0; index < vector.length; index++) {
            if (isPalindrome(vector[index])) {
                result = Utilities.addToVetor(result, vector[index]);
            }

        }
        return result;
    }

    private boolean isPalindrome(int value) {
        boolean isPalindrome = false;
        if (value <= 9) {
            return isPalindrome = false;
        }
        int copyValue = value;
        int reversedNumber = 0;
        while (copyValue > 0) {
            reversedNumber = (reversedNumber * 10) + (copyValue % 10);
            copyValue /= 10;
            if (reversedNumber == value) {
                isPalindrome = true;
            }
        }
        return isPalindrome;
    }

    public int[] getElementsWithSameDigits() {
        int[] result = new int[0];
        for (int index = 0; index < this.vector.length; index++) {
            if (hasEqualDigits(convertDigitsToArray(vector[index]))) {
                result = Utilities.addToVetor(result, vector[index]);
            }
        }
        return result;
    }

    private boolean hasEqualDigits(int[] vector) {
        boolean equalDigits = true;
        if (vector.length <= 1) {
            equalDigits = false;
        }
        for (int index = 0; index < vector.length - 1 && equalDigits; index++) {
            if (vector[index] != vector[index + 1]) {
                equalDigits = false;
            }

        }
        return equalDigits;
    }

    /**
     * Exercício 1 alínea y) obter números diferentes de armstrong
     *
     * @return int[] com esses números.
     */
    public int[] getNumbersDifferentFromArmstrong() {
        int[] result = new int[0];
        for (int index = 0; index < this.vector.length; index++) {
            if (!isArmstrong(vector[index])) {
                result = Utilities.addToVetor(result, vector[index]);
            }
        }
        return result;

    }

    private boolean isArmstrong(int value) {
        boolean amstrongNumber = false;
        int sum = 0;
        int copyValue = value;
        while (value > 0) {
            sum += (int) Math.pow(value % 10, 3);
            value /= 10;
        }
        if (copyValue == sum) {
            amstrongNumber = true;
        }
        return amstrongNumber;
    }

    public int[] increasingSequence(int size) {
        int[] result = new int[0];
        for (int index = 0; index < this.vector.length; index++) {
            if (isIncreasingSequence(convertDigitsToArray(vector[index]), size)) {
                result = Utilities.addToVetor(result, vector[index]);
            }
        }
        return result;

    }

    private boolean isIncreasingSequence(int[] vector, int size) {
        boolean isIncreasingSequence = true;
        if (size == vector.length) {
            for (int index = 0; index < vector.length - 1 && isIncreasingSequence; index++) {
                if (vector[index] >= vector[index + 1]) {
                    isIncreasingSequence = false;
                }

            }
        } else {
            isIncreasingSequence = false;
        }
        return isIncreasingSequence;
    }

    public Vector appendVector(Vector join) {
        Vector allVector = new Vector();
        if (!isNull()) {
            for (int element :
                    this.vector) {
                allVector.addElement(element);
            }
        }
        if(!join.isNull()) {
            for (int element :
                    join.vector) {
                allVector.addElement(element);
            }
        }
        return allVector;
    }


}
