package com.company.Sudoku;

public class SudokuCell {
    private int number;
    private boolean fixedNumber;


    //Constructors
    public SudokuCell(int newNumber, boolean fixed) {
        if (newNumber < 0 || newNumber > 9) { //fixed number não pode ser 0.
            throw new IllegalArgumentException("Number not in limits.");
        }
        this.number = newNumber;
        this.fixedNumber = fixed;
    }

    //Validation Methods

    /**
     * Check if the cell is fixed
     *
     * @return true or false
     */
    public boolean checkIfIsFixed() {
        return this.fixedNumber;
    }

    /**
     * Check if the cell is empty or not.
     *
     * @return true or false.
     */
    public boolean checkIfIsEmpty() {
        if (this.number == 0 && !fixedNumber) {
            return true;
        } else {
            return false;
        }
    }

    //Bussiness Methods

    /**
     * Remove number in cell if is not fixed.
     */
    public void removeNumber() {
        if (!fixedNumber) {
            this.number = 0;
        } else {
            throw new IllegalArgumentException("Cannot change this number!");
        }
    }

    /**
     * Change or add number in cell if is not fixed.
     */
    public void addOrChangeNumber(int value) {
        if (!fixedNumber) {
            this.number = value;
        } else {
            throw new IllegalArgumentException("Cannot change this number!");
        }
    }

    public int getNumber() {
        return number;
    }

    /**
     * Equals method to compare objects from this class.
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SudokuCell that = (SudokuCell) o;

        return number == that.number;
    }


}
