package com.company.Sudoku;

import java.util.Arrays;

public class SudokuGrid {
    private final SudokuCell[][] grid = new SudokuCell[9][9];
    //quando retiro o new SudokuCell[9][9] dá-me erro nos testes todos (Null Pointer).
    private static final int EMPTY = 0;
    private static final int GRID_SIZE = 9;
    private static final int SECTOR_SIZE = 3;

    public SudokuGrid(int[][] gridGame) {
        this.copy(gridGame);
    }

    //Methods

    /**
     * Equals method to compare objects from this class.
     * @param o other object
     * @return true or false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SudokuGrid that = (SudokuGrid) o;

        return Arrays.deepEquals(grid, that.grid);
    }


    /**
     * This method copies the input from constructor.
     * @param gridGame sudoku grid input
     *
     */
    private void copy(int[][] gridGame) {
        for (int i = 0; i < GRID_SIZE; i++) {
            for (int j = 0; j < GRID_SIZE; j++) {
                if (gridGame[i][j] != EMPTY) {
                    this.grid[i][j] = new SudokuCell(gridGame[i][j], true);
                } else {
                    this.grid[i][j] = new SudokuCell(EMPTY, false);

                }
            }
        }
    }

    /*public int[][] toArray() {

    }

    private void fillGrid() {

    }
    
     */

    /**
     * Add or change number in grid. You need the index from column and row.
     * @param row each row of grid
     * @param col each column of grid
     * @param number input value
     */
    public void addOrChangeNumber(int row, int col, int number) {
        if (!this.grid[row][col].checkIfIsFixed()) {
            this.grid[row][col].addOrChangeNumber(number);
        } else {
            throw new IllegalArgumentException("Cannot change this number.");
        }
    }

    /**
     * Remove number from cell, giving index of row and column.
     * @param row each row of grid
     * @param col each column of grid
     */
    public void removeNumber(int row, int col) {
        if (!this.grid[row][col].checkIfIsFixed()) {
            this.grid[row][col].removeNumber();
        } else {
            throw new IllegalArgumentException("Cannot change this number.");
        }
    }
/*
    public boolean checkColumn(int col, int value) {
        boolean isValid = true;


    }

    public boolean checkRow(int row, int value) {

    }

    public boolean checkSector(int firstRow, int firstCol, int value) {

    }

    public boolean isComplete() {
        boolean isComplete = true;
        int cellProduct = 1;
    }

 */

}
