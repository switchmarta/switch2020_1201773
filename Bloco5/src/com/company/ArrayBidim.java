package com.company;

public class ArrayBidim {
    //Attributes
    int[][] matrix;

    //Constructors
    public ArrayBidim() {
        this.matrix = new int[0][0];

    }

    public ArrayBidim(int[][] matrix) {
        this.matrix = copyMatrix(matrix);
    }


    //Methods
    @Override
    public boolean equals(Object other) {
        if (this.matrix == other) return true;
        if (other == null || this.getClass() != other.getClass()) return false;

        ArrayBidim otherMatrix = (ArrayBidim) other;
        if (this.matrix.length != otherMatrix.matrix.length) return false;
        boolean isEquals = false;

        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                if (this.matrix[i][j] != otherMatrix.matrix[i][j]) {
                    isEquals = false;
                } else {
                    isEquals = true;
                }
            }

        }
        return isEquals;
    }

    private int[][] copyMatrix(int[][] matrix) {
        int[][] copyMatrix;
        if (matrix == null) {
            copyMatrix = new int[0][0];
        } else {
            copyMatrix = new int[matrix.length][];
            for (int i = 0; i < matrix.length; i++) {
                copyMatrix[i] = new int[matrix[i].length];
                for (int j = 0; j < matrix[i].length; j++) {
                    copyMatrix[i][j] = matrix[i][j];
                }

            }
        }
        return copyMatrix;
    }

    public int[][] toArray() {
        int[][] result = new int[this.matrix.length][];
        for (int i = 0; i < this.matrix.length; i++) {
            result[i] = new int[matrix[i].length];
            for (int j = 0; j < this.matrix[i].length; j++) {
                result[i][j] = this.matrix[i][j];

            }

        }
        return result;
    }

    private void checkIsEmpty() {
        if (this.matrix.length == 0) {
            throw new IllegalArgumentException("Matrix is empty.");
        }
    }

    /**
     * Exercício 2 alínea c) Adicionar novo elemento a uma linha da matriz.
     *
     * @param value
     * @param line
     */
    public void addElement(int value, int line) {
        int k = 0;
        if (line >= this.matrix.length || line < 0) {
            throw new IndexOutOfBoundsException("Line invalid.");
        }
        int[] result = new int[this.matrix[line].length + 1];
        for (int element :
                this.matrix[line]) {
            result[k] = element;
            k++;
        }
        result[matrix[line].length] = value;
        this.matrix[line] = result;
    }

    private void removeFirstElementInLine(int value, int line) {
        int[] result = new int[this.matrix[line].length - 1];
        int i = 0;
        boolean existsValue = false;
        for (int element :
                this.matrix[line]) {
            if (value == element && !existsValue) {
                existsValue = true;
            } else {
                result[i] = element;
                i++;
            }
        }
        this.matrix[line] = result;
    }

    /**
     * Exercício 2 alínea d) remover o primeiro elemento com determinado valor das linhas da matriz.
     *
     * @param value
     */
    public void removeFirstElement(int value) {
        checkIsEmpty();
        boolean existsValue = false;
        for (int i = 0; i < this.matrix.length && !existsValue; i++) {
            for (int j = 0; j < matrix[i].length && !existsValue; j++) {
                if (value == matrix[i][j]) {
                    removeFirstElementInLine(value, i);
                    existsValue = true;
                }
            }

        }
    }

    /**
     * Exercício 2 alínea e) - retornar true ou false se a matriz estiver vazia.
     *
     * @return true ou false se vazia ou não.
     */
    public boolean checkIfIsEmpty() {
        if (this.matrix.length == 0) {
            return true;
        }
        return false;
    }

    /**
     * Exercício 2 alínea f) obter máximo de uma matriz
     *
     * @return o máximo.
     */
    public int highest() {
        checkIsEmpty();
        int highest = Utilities.highestOfMatrix(this.matrix);
        return highest;
    }

    /**
     * Exercicio 2 alínea g) retorna o número mais pequeno da matriz.
     *
     * @return mínimo.
     */

    public int smallest() {
        checkIsEmpty();
        int smallest = Utilities.smallestOfMatrix(this.matrix);
        return smallest;

    }

    /**
     * Exercício 2 alínea h) retorna média da matriz.
     *
     * @return mean.
     */
    public double mean() {
        checkIsEmpty();
        double mean = Utilities.avgOfMatrix(this.matrix);
        return mean;
    }

    /**
     * Exercício 2 alínea i)
     *
     * @return
     */
    public Vector sumOfEachLine() {
        checkIsEmpty();
        Vector sum = new Vector();
        for (int i = 0; i < matrix.length; i++) {
            int row = Utilities.arraySum(matrix[i]);
            sum.addElement(row);
        }
        return sum;
    }

    /**
     * Exercício 2 alínea j) soma dos elementos das colunas.
     *
     * @return soma desses elementos.
     */
    public int[] sumOfEachColumn() {
        checkIsEmpty();
        int[] sum = new int[0];
        int k = 0;
        int soma = 0;
        do {

            soma = 0;
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i].length - 1 >= k) {

                    soma = soma + matrix[i][k];
                }
            }
            sum = Utilities.addToVetor(sum, soma);
            k++;
        } while (k < highestColumn());
        return sum;
    }

    private int highestColumn() {
        int highest = matrix[0].length;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length > highest) {
                highest = matrix[i].length;
            }
        }
        return highest;
    }

    /**
     * Exercício 2 alínea k) retorna o índice da linha em que o valor da soma dos elementos é maior.
     *
     * @return int índice
     */
    public int indexOfHighestSum() {
        int[] sumOfAll = this.sumOfEachLine().toArray();
        int index = 0, highest = 0;
        for (int i = 0; i < sumOfAll.length; i++) {
            if (highest < sumOfAll[i]) {
                highest = sumOfAll[i];
                index = i;
            }
        }
        return index;
    }

    /**
     * Exercício 2 alínea l) verificar se é matriz quadrada.
     *
     * @return true ou false;
     */
    public boolean isSquare() {
        checkIsEmpty();
        boolean isSquare = true;
        for (int i = 0; i < this.matrix.length && isSquare; i++) {
            if (this.matrix[i].length != matrix.length) {
                isSquare = false;
            }
        }
        return isSquare;
    }

    private boolean isRectangular() {
        checkIsEmpty();
        boolean isRectangular = true;
        for (int i = 0; i < matrix.length && isRectangular; i++) {
            if (matrix.length == matrix[i].length || matrix[i].length != matrix[i++].length) {
                isRectangular = false;
            }

        }
        return isRectangular;
    }

    /**
     * Exercício 2 alínea m) verifica se matriz é simétrica ou não.
     * @return true ou false se é simétrica ou não.
     */
    public boolean isSymmetric() {
        if(!isSquare()) {
            throw new IllegalArgumentException("Matrix is not square!");
        }
        boolean isSymmetric = true;
        int[][] transpose = transpose(this.matrix);
        //if(this.toArray().equals(transpose) return true;
        for (int i = 0; i < this.matrix.length && isSymmetric; i++) {
            for (int j = 0; j < matrix[i].length && isSymmetric; j++) {
                if (matrix[i][j] != transpose[i][j]) {
                    isSymmetric = false;
                }

            }

        }
        return isSymmetric;
    }

    /**
     * Execício 2 alínea n) contagem de elementos não nulos da digonal da matriz.
     *
     * @return int da contagem
     */
    public int notNullElementsInDiagonal() {
        checkIsEmpty();
        int count = 0;
        if (!isSquare()) {
            count = -1;
        } else {
            for (int i = 0; i < this.matrix.length; i++) {
                for (int j = 0; j < this.matrix[i].length; j++) {
                    if (i == j && matrix[i][j] != 0) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    /**
     * Exercício 2 alínea o - verifica se diagonal principal e secundária são iguais.
     *
     * @return boolean true ou false.
     */
    public boolean checkEqualityOfDiagonalAndSecondary() {
        checkIsEmpty();
        boolean areEqual = false;
        if (!isRectangular() && !isSquare()) {
            areEqual = false;
        } else if (getDiagonalMatrix().equals(getSecondaryDiagonal())) {
            areEqual = true;
        }
        return areEqual;
    }
    private Vector getDiagonalMatrix() {
        Vector diagonal = new Vector();
        for (int i = 0; i < matrix.length; i++) {
            diagonal.addElement(matrix[i][i]);
        }
        return diagonal;
    }
    private Vector getSecondaryDiagonal() {
        Vector secDiagonal = new Vector();
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                if ((i + j) == (this.matrix[i].length - 1)) {//quando a soma dos indices dá igual
                    secDiagonal.addElement(this.matrix[i][j]);//ao tamanho da coluna -1, equivale aos nº da diagonal.
                }
            }
        }
        return secDiagonal;
    }

    /**
     * Exercício 2 alínea p) retornar números com nº algarismos maior que a média de todos os elementos.
     *
     * @return
     */

    public int[] moreDigitsThanAverage() {
        double avgDigits = Utilities.avgOfDigits(matrix);
        Vector result = new Vector();
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                if (Utilities.countDigits(this.matrix[i][j]) > avgDigits) {
                    result.addElement(this.matrix[i][j]);
                }

            }
        }

        return result.toArray();
    }

    /**
     * Exercício 2 alínea r) inverter os elementos da linha.
     */
    public void invertOrderInLine() {
        int[][] result = new int[this.matrix.length][];
        for (int i = 0; i < this.matrix.length; i++) {
            result[i] = Utilities.invert(matrix[i]);
        }
        this.matrix = result;
    }

    /**
     * Exercício 2 alínea s) inverter os elementos das colunas.
     */

    public void invertOrderInColumn() {
        int[][] result = new int[this.matrix.length][];
        for (int i = 0; i < this.matrix.length; i++) {
            result[i] = new int[matrix[i].length];
            for (int j = 0; j < this.matrix[i].length; j++) {
                result[j] = Utilities.invert(matrix[j]);
            }
        }
        this.matrix = result;
    }

    public int[][] transpose(int[][] matrix) {
        if (!isRectangular() && !isSquare()) {
            throw new IllegalArgumentException("Not valid.");
        }
        int[][] result = new int[matrix[0].length][matrix.length];
        for (int line = 0; line < matrix.length; line++) {
            for (int col = 0; col < matrix[line].length; col++) {
                result[col][line] = matrix[line][col];
            }
        }
        return result;
    }

    /**
     * Exercicio 2 alínea t) rodar a matriz 90º para a direita. É o mesmo que inverter a transposta da matriz.
     */
    public void rotateNinetyDegrees() {
        if (!isRectangular() && !isSquare()) {
            throw new IllegalArgumentException("Not valid.");
        }
        int[][] result = transpose(this.matrix);
        int[][] finalResult = new int[result.length][];
        for (int i = 0; i < result.length; i++) {
            finalResult[i] = Utilities.invert(result[i]);
        }
        this.matrix = finalResult;
    }

    /**
     * Exercício 2 alínea u) rodar 180º para a direita.
     */
    public void rotateOneHundredAndEightyDegrees() {
        if (!isRectangular() && !isSquare()) {
            throw new IllegalArgumentException("Not valid.");
        }
        ArrayBidim inverted = new ArrayBidim(this.matrix);
        inverted.invertOrderInLine();
        int[][] invertedMatrix = inverted.toArray();
        for (int i = 0; i < invertedMatrix.length; i++) {
            for (int j = 0; j < invertedMatrix[i].length; j++) {
                invertedMatrix[i][j] = this.matrix[(matrix.length - 1) - i][(matrix[i].length - 1) - j];
            }

        }
        this.matrix = invertedMatrix;

    }

    /**
     * Exercício 2 alínea v) rodar -90º. É o mesmo que inverter a matriz e fazer a transposta da inversão.
     */
    public void rotateMinusNinetyDegrees() {
        if (!isRectangular() && !isSquare()) {
            throw new IllegalArgumentException("Not valid.");
        }
        ArrayBidim inverted = new ArrayBidim(this.matrix);
        inverted.invertOrderInLine();
        int[][] invertedMatrix = inverted.toArray();
        int[][] finalNinety = transpose(invertedMatrix);

        this.matrix = finalNinety;
    }


}

