package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @Test
    public void addAnElementInEmptyVector() {
        //Arrange
        int[] vectorOne = {};
        int value = 5;
        int[] expected = {5};
        Vector v1 = new Vector(vectorOne);
        v1.addElement(value);
        //Act
        int[] result = v1.toArray();
        //Assert
        assertArrayEquals(expected, result);
        assertNotSame(result, v1.toArray());
    }

    @Test
    public void addAnElementFiveInVector() {
        //Arrange
        int[] vectorOne = {1, 2, 3, 4};
        int value = 5;
        int[] expected = {1, 2, 3, 4, 5};
        Vector v1 = new Vector(vectorOne);
        v1.addElement(value);
        //Act
        int[] result = v1.toArray();
        //Assert
        assertArrayEquals(expected, result);
        assertNotSame(result, v1.toArray());
    }

    @Test
    public void addAnElementTenInVector() {
        //Arrange
        int[] vectorOne = {1, 2, 3, 4, 5, 6};
        int value = 10;
        int[] expected = {1, 2, 3, 4, 5, 6, 10};
        Vector v1 = new Vector(vectorOne);
        v1.addElement(value);
        //Act
        int[] result = v1.toArray();
        //Assert
        assertArrayEquals(expected, result);
        assertNotSame(result, v1.toArray());

    }

    @Test
    public void valueDoesNotExistsInVector() {
        //Arrange
        int value = -10;
        int[] vectorOne = {1, 2, 3, 4};
        Vector v1 = new Vector(vectorOne);
        int[] exp = {1, 2, 3, 4};
        v1.removeFirstElement(-10);
        assertArrayEquals(exp, v1.toArray());

    }


    @Test
    public void removeAnElementFromTheVector() {
        //Arrange
        int value = 4;
        int[] vectorOne = {1, 2, 3, 4};
        int[] expected = {1, 2, 3};
        Vector v1 = new Vector(vectorOne);
        v1.removeFirstElement(value);
        //Act
        int[] result = v1.toArray();
        //Assert
        assertArrayEquals(expected, result);
        assertNotSame(result, v1.toArray());


    }

    @Test
    public void removeFirstRepeteadElementFromTheVector() {
        //Arrange
        int value = 2;
        int[] vectorOne = {1, 2, 2, 2, 3, 4};
        int[] expected = {1, 2, 2, 3, 4};
        Vector v1 = new Vector(vectorOne);
        v1.removeFirstElement(value);
        //Act
        int[] result = v1.toArray();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void positionNotInVector() {
        int position = 4;
        int[] vectorOne = {1, 2, 3};
        Vector vector = new Vector(vectorOne);
        assertThrows(IllegalArgumentException.class, () -> {
            vector.valueOnPosition(4);
        });


    }

    @Test
    public void returnValueOnPositionTwo() {
        //Arrange
        int position = 2;
        int[] vectorOne = {10, 20, 30, 80, 91};
        int expected = 30;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.valueOnPosition(position);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void returnValueOnPositionFive() {
        //Arrange
        int position = 5;
        int[] vectorOne = {10, 20, 30, 80, 91, 1, 2, 3, 4};
        int expected = 1;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.valueOnPosition(position);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void returnLengthVector() {
        //Arrange
        int[] vectorOne = {10, 20, 30, 80, 91, 1, 2, 3, 4};
        int expected = 9;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.numberOfLength();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void returnLengthVectorII() {
        //Arrange
        int[] vectorOne = {10};
        int expected = 1;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.numberOfLength();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void returnLengthVectorEmpty() {
        //Arrange
        int[] vectorOne = {};
        int expected = 0;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.numberOfLength();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void highestNumberTen() {
        //Arrange
        int[] vectorOne = {1, 2, 10, 1, 2, 9, 4, 5};
        int expected = 10;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.highestNumber();
        //Assert
        assertEquals(expected, result);
    }


    @Test
    public void smallestNumberNullVector() {
        int[] vectorOne = null;
        Vector vector = new Vector(vectorOne);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            vector.smallestNumber();
        });
    }

    @Test
    public void smallestNumberMinusOne() {
        //Arrange
        int[] vectorOne = {-1, 4, 5, 7, 0};
        int expected = -1;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.smallestNumber();
        assertEquals(expected, result);
    }

    @Test
    public void smallestNumberZero() {
        //Arrange
        int[] vectorOne = {10, 0, 5, 8, 0};
        int expected = 0;
        Vector vector = new Vector(vectorOne);
        //Act
        int result = vector.smallestNumber();
        assertEquals(expected, result);
    }

    @Test
    public void averageOfElementsWithNegativeValues() {
        //Arrange
        int[] vectorOne = {7, -1, 2, 3, 10};
        Vector vector = new Vector(vectorOne);
        double expected = 4.2;
        //Act
        double result = vector.elementsAverage();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageOfSeveralElements() {
        //Arrange
        int[] vectorOne = {7, 1, 2, 3, 10, 1, 2, 4, 6, 87, 532, 134, 743};
        Vector vector = new Vector(vectorOne);
        double expected = 117.8;
        //Act
        double result = vector.elementsAverage();
        //Assert
        assertEquals(expected, result, 0.1);

    }

    @Test
    public void averageOfEvenNumbers() {
        int[] vectorOne = {1, 2, 6, 7, 8, 10, 50};
        Vector vector = new Vector(vectorOne);
        double expected = 15.2;
        //Act
        double result = vector.averageOfEvenElements();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageOfEvenNumbersWithNegativeElements() {
        int[] vectorOne = {1, 2, 6, 7, -8, 10, 50, 13, -2};
        Vector vector = new Vector(vectorOne);
        double expected = 9.6;
        //Act
        double result = vector.averageOfEvenElements();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageWithNoEvenElements() {
        int[] vectorOne = {1, 3, -7, -11, 9, 65};
        Vector vector = new Vector(vectorOne);
        double expected = 0;
        //Act
        double result = vector.averageOfEvenElements();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageOfOddNumbersWithNegativeElements() {
        int[] vectorOne = {1, 2, 6, 7, -8, 10, 50, 13, -2};
        Vector vector = new Vector(vectorOne);
        double expected = 7;
        //Act
        double result = vector.averageOfOddElements();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageWithNoOddElements() {
        int[] vectorOne = {2, 6, -8, 10, 50, -2};
        Vector vector = new Vector(vectorOne);
        double expected = 0;
        //Act
        double result = vector.averageOfOddElements();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageWithMultiplesOfSeven() {
        //Arrange
        int number = 7;
        int[] vectorOne = {7, 14, -8, 10, 35, 0, -21};
        Vector vector = new Vector(vectorOne);
        double expected = 8.7;
        //Act
        double result = vector.averageMultiplesOfNumber(number);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void averageWithNoMultiples() {
        //Arrange
        int number = -7;
        int[] vectorOne = {2, 5, -8, 10, 50, 0, 34};
        Vector vector = new Vector(vectorOne);
        double expected = 0;
        //Act
        double result = vector.averageMultiplesOfNumber(number);
        //Assert
        assertEquals(expected, result, 0.1);
    }


    @Test
    public void orderAnUnorderedVectorByAsc() {
        int[] vectorOne = {1, 8, 3, 5, 4, 3, 2};
        Vector vector = new Vector(vectorOne);
        int[] expected = {1, 2, 3, 3, 4, 5, 8};
        vector.orderNumberByAsc();
        int[] result = vector.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void orderAnUnorderedVectorByAscWithNegativeValues() {
        int[] vectorOne = {1, 8, 3, -55, 4, 3, 2, -4};
        Vector vector = new Vector(vectorOne);
        int[] expected = {-55, -4, 1, 2, 3, 3, 4, 8};
        vector.orderNumberByAsc();
        int[] result = vector.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void alreadyOrderedVectorByAsc() {
        int[] vectorOne = {-1, 0, 10, 40, 45, 80};
        Vector vector = new Vector(vectorOne);
        int[] expected = {-1, 0, 10, 40, 45, 80};
        vector.orderNumberByAsc();
        int[] result = vector.toArray();
        assertArrayEquals(expected, result);
        assertNotSame(vectorOne, result);
    }

    @Test
    public void orderAnUnorderedVectorByDesc() {
        int[] vectorOne = {1, 8, 3, 5, 4, 3, 2};
        Vector vector = new Vector(vectorOne);
        int[] expected = {8, 5, 4, 3, 3, 2, 1};
        vector.orderNumberByDesc();
        int[] result = vector.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void alreadyOrderedVectorByDesc() {
        int[] vectorOne = {40, 21, 3, 2, 0, -10, -12 - 15};
        Vector vector = new Vector(vectorOne);
        int[] expected = {40, 21, 3, 2, 0, -10, -12 - 15};
        vector.orderNumberByDesc();
        int[] result = vector.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    public void vectorEmpty() {
        int[] vectorOne = {};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.isEmpty();
        assertTrue(result);
    }

    @Test
    public void vectorFilled() {
        int[] vectorOne = {1, 2, 3};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.isEmpty();
        assertFalse(result);
    }

    @Test
    public void vectorHasOnlyOneElement() {
        //Arrange
        int[] vectorOne = {1};
        Vector vector = new Vector(vectorOne);
        //Act
        boolean result = vector.hasOnlyOneElement();
        //Assert
        assertTrue(result);
    }

    @Test
    public void vectorHasMoreThanOneElement() {
        //Arrange
        int[] vectorOne = {1, 2, 3};
        Vector vector = new Vector(vectorOne);
        //Act
        boolean result = vector.hasOnlyOneElement();
        //Assert
        assertFalse(result);
    }

    @Test
    public void vectorHasOnlyEvenNumbers() {
        int[] vectorOne = {2, 4, 6, 8, 10};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.hasOnlyEvenElements();
        assertTrue(result);
    }

    @Test
    public void vectorHasEvenAndOddNumbers() {
        int[] vectorOne = {2, 4, 6, 8, 10, 11, 13, 15};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.hasOnlyEvenElements();
        assertFalse(result);
    }

    @Test
    public void vectorHasOnlyOddNumbers() {
        int[] vectorOne = {11, 13, 15};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.hasOnlyOddElements();
        assertTrue(result);
    }

    @Test
    public void vectorHasBothTypeOfNumbers() {
        int[] vectorOne = {11, 13, 15, -2, -4};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.hasOnlyOddElements();
        assertFalse(result);
    }

    @Test
    public void vectorHasDuplicatedNumbers() {
        int[] vectorOne = {11, 1, 15, 11, -4};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.hasDuplicatedElements();
        assertTrue(result);
    }

    @Test
    public void vectorHasNotDuplicatedNumbers() {
        int[] vectorOne = {-11, 11, -1, 0, -4};
        Vector vector = new Vector(vectorOne);
        boolean result = vector.hasDuplicatedElements();
        assertFalse(result);
    }

    @Test
    public void elementsWithMoreDigitsThanTheAverage() {
        //Arrange
        int[] expectedArray = {123, 1234, 6789};
        int[] vectorOne = {123, 12, 1, 1234, 6789, 4};
        Vector vector = new Vector(vectorOne);
        int[] result = vector.numbersWithMoreDigitsThanTheAverage();
        //Assert
        assertArrayEquals(expectedArray, result);
    }

    @Test
    public void elementsWithSameDigits() {
        //Arrange
        int[] vectorOne = {1, 1, 1, 1, 6, 4};
        Vector vector = new Vector(vectorOne);
        int[] expectedArray = {};
        //Act
        int[] result = vector.numbersWithMoreDigitsThanTheAverage();
        //Assert
        assertArrayEquals(expectedArray, result);
    }

    @Test
    public void percentageEvenDigits() {
        //Arrange
        int[] vectorOne = {123, 122, 156, 1666, 6, 4112};
        Vector vector = new Vector(vectorOne);
        int[] expectedArray = {122, 1666, 6};
        //Act
        int[] result = vector.percentageEvenDigitsGreaterThanTheAverage();
        //Assert
        assertArrayEquals(expectedArray, result);
    }

    @Test
    public void numbersWithOnlyEvenDigits() {
        //Arrange
        int[] vectorOne = {123, 122, 156, 666, 6, 4112};
        Vector vector = new Vector(vectorOne);
        int[] expectedArray = {666, 6};
        //Act
        int[] result = vector.numberWithOnlyEvenDigits();
        //Assert
        assertArrayEquals(expectedArray, result);
    }

    @Test
    public void numbersWithOnlyEvenDigitsII() {
        //Arrange
        int[] vectorOne = {1, 122, 156, 6, 6846, 4112, 862};
        Vector vector = new Vector(vectorOne);
        int[] expectedArray = {6, 6846, 862};
        //Act
        int[] result = vector.numberWithOnlyEvenDigits();
        //Assert
        assertArrayEquals(expectedArray, result);
    }

    @Test
    public void sequenceDigits() {
        //Arrange
        int[] vectorOne = {1, 122, 156, 6, 6846, 4112, 862};
        Vector vector = new Vector(vectorOne);
        int[] expected = {156};
        //Act
        int[] result = vector.sequenceElements();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void sequenceDigitsWithNegativeValues() {
        //Arrange
        int[] vectorOne = {1, -321, 156, 6, 6846, -98740, 862};
        Vector vector = new Vector(vectorOne);
        int[] expected = {-321, 156, -98740}; //Como o nº é negativo a sequência aparece ao contrário por ser negativo.
        //Act
        int[] result = vector.sequenceElements();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void noSequenceDigits() {
        //Arrange
        int[] vectorOne = {1, -897, 6, 6846, 3769, 862};
        Vector vector = new Vector(vectorOne);
        int[] expected = {};
        //Act
        int[] result = vector.sequenceElements();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void invertVector() {
        //Arrange
        int[] vectorOne = {1, -897, 6, 6846, 3769, 862};
        Vector vector = new Vector(vectorOne);
        int[] expected = {862, 3769, 6846, 6, -897, 1};
        Vector exp = new Vector(expected);
        //Act
        vector.invert();
        //Assert
        assertEquals(exp, vector);
    }

    @Test
    public void countEveryDigit() {
        double avgDigits = 2.5;
        int[] array = {12, 245, 34};
        Vector original = new Vector(array);
        int[] arrayExp = {3};
        Vector exp = new Vector(arrayExp);
        //ACT
        Vector result = original.countEveryDigitElement(avgDigits);
        //ASSERT
        assertEquals(exp, result);

    }

    @Test
    public void sumElements() {
        int[] array = {123, 456, 7};
        Vector vector = new Vector(array);
        int expected = 7;
        //ACT
        int result = vector.sumElements();
        //ASSERT
        assertEquals(expected, result);

    }

    @Test
    public void palindromes() {
        //Arrange
        int[] vectorOne = {212, -344, 1, 1001, 789, 5};
        Vector vector = new Vector(vectorOne);
        int[] expected = {212, 1001};
        //Act
        int[] result = vector.getPalindromes();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void noPalindromes() {
        //Arrange
        int[] vectorOne = {0, -344, 1, 735, 789, 5};
        Vector vector = new Vector(vectorOne);
        int[] expected = {};
        //Act
        int[] result = vector.getPalindromes();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void elementsWithEqualDigits() {
        //Arrange
        int[] vectorOne = {0, -444, 1, 735, 789, 5};
        Vector vector = new Vector(vectorOne);
        int[] expected = {-444};
        //Act
        int[] result = vector.getElementsWithSameDigits();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void noElementsWithEqualDigits() {
        //Arrange
        int[] vectorOne = {0, 987654, 1, 735, 789, 5};
        Vector vector = new Vector(vectorOne);
        int[] expected = {};
        //Act
        int[] result = vector.getElementsWithSameDigits();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void elementsWithEqualDigitsII() {
        //Arrange
        int[] vectorOne = {0, 222, 1, 735, 789, 55};
        Vector vector = new Vector(vectorOne);
        int[] expected = {222, 55};
        //Act
        int[] result = vector.getElementsWithSameDigits();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void elementsDifferentFromArmstrong() {
        //Arrange
        int[] vectorOne = {153, 370, 21, 153, 22, 407};
        Vector vector = new Vector(vectorOne);
        int[] expected = {21, 22};
        //Act
        int[] result = vector.getNumbersDifferentFromArmstrong();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void elementsArmstrongOnly() {
        //Arrange
        int[] vectorOne = {153, 370, 370, 153, 407, 407};
        Vector vector = new Vector(vectorOne);
        int[] expected = {};
        //Act
        int[] result = vector.getNumbersDifferentFromArmstrong();
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void increasingSequence() {
        //Arrange
        int size = 3;
        int[] vectorOne = {134, 271, 32, 789, 1, 2};
        Vector vector = new Vector(vectorOne);
        int[] expected = {134, 789};
        //Act
        int[] result = vector.increasingSequence(size);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void noIncreasingSequence() {
        //Arrange
        int size = 2;
        int[] vectorOne = {174, 271, 32, 781, 181, 290};
        Vector vector = new Vector(vectorOne);
        int[] expected = {};
        //Act
        int[] result = vector.increasingSequence(size);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void increasingSequenceWithNegativeValues() {
        //Arrange
        int size = 3;
        int[] vectorOne = {-741, 271, 32, 781, 181, 290};
        Vector vector = new Vector(vectorOne);
        int[] expected = {-741};
        //Act
        int[] result = vector.increasingSequence(size);
        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void joinTwoVectors() {
        int[] first = {1, 2, 3, 4};
        Vector join = new Vector(first);
        int[] second = {4, 5, 6};
        Vector original = new Vector(second);
        int[] exp = {4, 5, 6,1,2,3,4};
        Vector expected = new Vector(exp);
        //Act
        Vector result = original.appendVector(join);
        assertEquals(expected,result);
    }

}