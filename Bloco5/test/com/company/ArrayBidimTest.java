package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayBidimTest {

    @Test
    public void checkToArray() {
        int[][] initialArray = {{1, 2, 3}, {4, 5, 6}};
        ArrayBidim array = new ArrayBidim(initialArray);
        int[][] expected = {{1, 2, 3}, {4, 5, 6}};
        //Assert
        assertArrayEquals(expected, array.toArray());
    }

    @Test
    public void checkEquals() {
        int[][] initialArray = {{1, 2, 3}, {4, 5, 6}};
        ArrayBidim array = new ArrayBidim(initialArray);
        int[][] expected = {{1, 2, 3}, {4, 5, 6}};
        ArrayBidim expectedArray = new ArrayBidim(expected);
        //Assert
        assertEquals(expectedArray, array);
    }

    @Test
    public void addElementOnLineOne() {
        int value = 7;
        int line = 1;
        int[][] initialArray = {{1, 2, 3}, {4, 5, 6}};
        ArrayBidim array = new ArrayBidim(initialArray);
        int[][] expected = {{1, 2, 3}, {4, 5, 6, 7}};
        //Act
        array.addElement(value, line);
        //Assert
        assertArrayEquals(expected, array.toArray());
    }

    @Test
    public void addElementOnEmptyMatrix() {
        int value = 7;
        int line = 0;
        int[][] initialArray = {{}};
        ArrayBidim array = new ArrayBidim(initialArray);
        int[][] expected = {{7}};
        //Act
        array.addElement(value, line);
        //Assert
        assertArrayEquals(expected, array.toArray());
    }

    @Test
    public void addElementOnLineThatDoesntExist() {
        int value = 7;
        int line = 2;
        int[][] initialArray = {{1, 2, 3}, {4, 5, 6}};
        ArrayBidim array = new ArrayBidim(initialArray);
        assertThrows(IndexOutOfBoundsException.class, () -> {
            array.addElement(value, line);
        });

    }

    @Test
    public void removeElementFromSecondLine() {
        //Arrange
        int value = -4;
        int[][] arrayTest = {{1, 2, 3}, {-4, 8, 9}, {-1, -4, 7}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int[][] expected = {{1, 2, 3}, {8, 9}, {-1, -4, 7}};
        //Act
        test.removeFirstElement(value);
        //Assert
        assertArrayEquals(expected, test.toArray());
    }

    @Test
    public void removeElementFromSecondLineLeavingLineEmpty() {
        //Arrange
        int value = -4;
        int[][] arrayTest = {{1, 2, 3}, {-4}, {-1, -4, 7}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int[][] expected = {{1, 2, 3}, {}, {-1, -4, 7}};
        //Act
        test.removeFirstElement(value);
        //Assert
        assertArrayEquals(expected, test.toArray());
    }

    @Test
    public void elementIsNotInTheMatrix() {
        //Arrange
        int value = 12;
        int[][] arrayTest = {{0, 2, 3}, {-4, 8, 9}, {-1, -4, 7}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int[][] expected = {{0, 2, 3}, {-4, 8, 9}, {-1, -4, 7}};
        //Act
        test.removeFirstElement(value);
        //Assert
        assertArrayEquals(expected, test.toArray());
    }

    @Test
    public void emptyMatrix() {
        //Arrange
        int[][] arrayTest = {};
        ArrayBidim test = new ArrayBidim(arrayTest);
        //Act
        boolean result = test.checkIfIsEmpty();
        //Assert
        assertTrue(result);
    }

    @Test
    public void notEmptyMatrix() {
        //Arrange
        int[][] arrayTest = {{1, 2}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        //Act
        boolean result = test.checkIfIsEmpty();
        //Assert
        assertFalse(result);
    }

    @Test
    public void highest() {
        //Arrange
        int[][] arrayTest = {{1, 34, 1}, {-5, 8, 90}, {0, -3, 8}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int expected = 90;
        //Act
        int result = test.highest();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void highestWithNegativeElements() {
        //Arrange
        int[][] arrayTest = {{0, -34, -1}, {-5, -8, -90}, {0, -3, -8}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int expected = 0;
        //Act
        int result = test.highest();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void checkEmptyMatrix() {
        int[][] initialArray = {};
        ArrayBidim array = new ArrayBidim(initialArray);
        assertThrows(IllegalArgumentException.class, () -> {
            array.highest();
        });

    }

    @Test
    public void smallest() {
        //Arrange
        int[][] arrayTest = {{1, 34, 1}, {-4, 8, 90}, {0, -3, 8}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int expected = -4;
        //Act
        int result = test.smallest();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void smallestNegativeElements() {
        //Arrange
        int[][] arrayTest = {{0, -34, -1}, {-5, -8, -90}, {-2, -3, -8}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int expected = -90;
        //Act
        int result = test.smallest();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void avgMatrix() {
        //Arrange
        int[][] arrayTest = {{1, 2, 3}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        double expected = 2;
        //Act
        double result = test.mean();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void avgMatrixWithNegativeValues() {
        //Arrange
        int[][] arrayTest = {{0, 2, 0}, {-8, -9, -4}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        double expected = -3.1;
        //Act
        double result = test.mean();
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void sumOfEachLineOfMatrix() {
        //Arrange
        int[][] arrayTest = {{0, 2, 0}, {-8, -9, -4}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int[] expected = {2, -21};
        Vector arrayExpected = new Vector(expected);
        //Act
        Vector result = test.sumOfEachLine();
        //Assert
        assertEquals(arrayExpected, result);
    }

    @Test
    public void sumOfEachLineOfMatrixII() {
        //Arrange
        int[][] arrayTest = {{0, 2, 1}, {-8, -9, -4}, {1, -3, 9, 7}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int[] expected = {3, -21, 14};
        Vector arrayExpected = new Vector(expected);
        //Act
        Vector result = test.sumOfEachLine();
        //Assert
        assertEquals(arrayExpected, result);
    }

    @Test
    public void sumOfEachColumn() {
        int[][] matrix = {{1, 2, 3, 4}, {4, 5, 6, 7, 8}, {7, 8}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        int[] expected = {12, 15, 9, 11, 8};
        int[] result = testMatrix.sumOfEachColumn();
        assertArrayEquals(expected, result);
    }

    @Test
    public void sumOfEachColumnII() {
        int[][] matrix = {{1, 2, 3}, {4, 5, 6, 7, 8}, {7, 8, 1}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        int[] expected = {12, 15, 10, 7, 8};
        int[] result = testMatrix.sumOfEachColumn();
        assertArrayEquals(expected, result);
    }

    @Test
    public void indexOfTheHighestSum() {
        //Arrange
        int[][] arrayTest = {{8, 2, 0}, {10, 30, 16}, {-8, -9, -4}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int expected = 1;
        //Act
        int result = test.indexOfHighestSum();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void indexOfTheHighestSumII() {
        //Arrange
        int[][] arrayTest = {{8, 2, 0}, {10, 30, 16}, {-8, -9, -4}, {50, 50}, {}};
        ArrayBidim test = new ArrayBidim(arrayTest);
        int expected = 3;
        //Act
        int result = test.indexOfHighestSum();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void isSquareMatrix() {
        int[][] matrix = {{1, 2}, {3, 4}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        boolean result = testMatrix.isSquare();
        assertTrue(result);
    }

    @Test
    public void isNotSquareMatrix() {
        int[][] matrix = {{1, 2, 3}, {3, 4, 5}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        boolean result = testMatrix.isSquare();
        assertFalse(result);
    }
    @Test
    public void isSymmetricMatrix() {
        int[][] matrix = {{1, 1, -1}, {1, 2, 0}, {-1,0,5}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        boolean result = testMatrix.isSymmetric();
        assertTrue(result);
    }
    @Test
    public void isNotSymmetricMatrix() {
        int[][] matrix = {{2, 1, 5}, {1, 2, 5}, {9,4,5}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        boolean result = testMatrix.isSymmetric();
        assertFalse(result);
    }
    @Test
    public void isNotASquareMatrix() {
        int[][] initialArray = {{1, 2, 3}, {4, 5, 6}};
        ArrayBidim array = new ArrayBidim(initialArray);
        assertThrows(IllegalArgumentException.class, () -> {
            array.isSymmetric();
        });

    }

    @Test
    public void threeNonNullElementsInDiagonal() {
        int[][] matrix = {{1, 2, 3}, {3, 4, 5}, {6, 7, 8}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        int expected = 3;
        int result = testMatrix.notNullElementsInDiagonal();
        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void isNotSquare() {
        int[][] matrix = {{1, 2}, {3, 4}, {6, 7}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        int expected = -1;
        int result = testMatrix.notNullElementsInDiagonal();
        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void twoNonNullElementsInDiagonal() {
        int[][] matrix = {{0, 2, -7, -2}, {3, 4, 1, -8}, {6, 7, 1, 12}, {-1, 0, 0, 0}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        int expected = 2;
        int result = testMatrix.notNullElementsInDiagonal();
        //Assert
        assertEquals(expected, result);

    }

    @Test
    public void equalsMainDiagonalAndSecondary() {
        int[][] matrix = {{0, 2, -7, 0},
                {3, 4, 4, -8},
                {6, 1, 1, 12},
                {0, -2, 0, 0}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        boolean result = testMatrix.checkEqualityOfDiagonalAndSecondary();
        //Assert
        assertTrue(result);

    }

    @Test
    public void checkNonSquareOrRectMatrix() {
        int[][] matrix = {{0, 2, -7, 0}, {3, 4, 4, -8}, {0, -2, 0}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        boolean result = testMatrix.checkEqualityOfDiagonalAndSecondary();
        //Assert
        assertFalse(result);

    }

    @Test
    public void mainAndSecondaryAreNotEquals() {
        int[][] matrix = {{1, 2, -7, 0}, {3, 4, 4, -8}, {0, -2, 0, 9}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        boolean result = testMatrix.checkEqualityOfDiagonalAndSecondary();
        //Assert
        assertFalse(result);

    }

    @Test
    public void moreDigitsThanAverage() {
        int[][] matrix = {{1, 22, 44, 123}, {1, 2, 3, 66, 77}, {123, 456, 1, 2}, {1, 1, 1, 1}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        int[] result = testMatrix.moreDigitsThanAverage();
        int[] expected = {22, 44, 123, 66, 77, 123, 456};
        assertArrayEquals(expected, result);
    }

    @Test
    public void moreDigitsThanAverageII() {
        int[][] matrix = {{1, 2}, {1, 2, 3}, {1, 1, 1, 2}, {1, 1, 1, 1}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        int[] result = testMatrix.moreDigitsThanAverage();
        int[] expected = {};
        assertArrayEquals(expected, result);
    }

    @Test
    public void invertedLinesMatrix() {
        int[][] matrix = {{1, 2}, {1, 2, 3}, {12, 1, 11, 2}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.invertOrderInLine();
        int[][] expected = {{2, 1}, {3, 2, 1}, {2, 11, 1, 12}};
        assertArrayEquals(expected, testMatrix.toArray());
    }

    @Test
    public void invertedColumnsMatrix() {
        int[][] matrix = {{1, 2, 9},
                {1, 2, 3},
                {12, 1, 11}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.invertOrderInColumn();
        int[][] expected = {{9, 2, 1},
                {3, 2, 1},
                {11, 1, 12}};
        assertArrayEquals(expected, testMatrix.toArray());
    }

    @Test
    public void invertedColumnsMatrixII() {
        int[][] matrix = {{1, 2, 9},
                {1, 2, 3},
                {12, 1, 11}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.invertOrderInColumn();
        int[][] expected = {{9, 2, 1},
                {3, 2, 1},
                {11, 1, 12}};
        assertArrayEquals(expected, testMatrix.toArray());
    }

    @Test
    public void rotateNinetyDegreesSquareMatrix() {
        int[][] matrix = {{1, 2, 9},
                {1, 2, 3},
                {12, 1, 11}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.rotateNinetyDegrees();
        int[][] expected = {{12, 1, 1},
                {1, 2, 2},
                {11, 3, 9}};
        assertArrayEquals(expected, testMatrix.toArray());
    }

    @Test
    public void rotateNinetyDegreesRectMatrix() {
        int[][] matrix = {{1, 2},
                {1, 3},
                {12, 11}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.rotateNinetyDegrees();
        int[][] expected = {{12, 1, 1},
                {11, 3, 2}};
        assertArrayEquals(expected, testMatrix.toArray());
    }

    @Test
    public void rotateOneHundredEightySquareMatrix() {
        int[][] matrix = {{1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.rotateOneHundredAndEightyDegrees();
        int[][] expected = {{9, 8, 7},
                {6, 5, 4}, {3,2,1}};
        assertArrayEquals(expected, testMatrix.toArray());
    }
    @Test
    public void rotateOneHundredEightyRectMatrix() {
        int[][] matrix = {{1, 2},
                {4, 5},
                {7, 8}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.rotateOneHundredAndEightyDegrees();
        int[][] expected = {{8,7},
                {5,4}, {2,1}};
        assertArrayEquals(expected, testMatrix.toArray());
    }
    @Test
    public void rotateMinusNinetySquareMatrix() {
        int[][] matrix = {{1, 2,3},
                {4, 5,6},
                {7, 8,9}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.rotateMinusNinetyDegrees();
        int[][] expected = {{3,6,9},
                {2,5,8}, {1,4,7}};
        assertArrayEquals(expected, testMatrix.toArray());
    }
    @Test
    public void rotateMinusNinetyRectMatrix() {
        int[][] matrix = {{1, 2},
                {4, 5},
                {7, 8}};
        ArrayBidim testMatrix = new ArrayBidim(matrix);
        testMatrix.rotateMinusNinetyDegrees();
        int[][] expected = {{2,5,8},
                {1,4,7}};
        assertArrayEquals(expected, testMatrix.toArray());
    }



}
