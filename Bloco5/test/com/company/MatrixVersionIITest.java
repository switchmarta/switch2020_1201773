package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MatrixVersionIITest {
    @Test
    void matrixIIEmptyConstructor() {
        //Arrange
        Vector[] expected = {};
        Vector[] initialMatrix = {};
        MatrixVersionII originalVector = new MatrixVersionII(initialMatrix);
        MatrixVersionII expected1 = new MatrixVersionII(expected);
        //Act
        //Assert
        assertEquals(expected1, originalVector);
    }
    @Test
    void nullMatrix() {
        int[][] initial = null;
        int[][] expected = {};
        MatrixVersionII initialMatrix = new MatrixVersionII(initial);
        MatrixVersionII expected1 = new MatrixVersionII(expected);

        assertEquals(expected1,initialMatrix);
    }

    @Test
    void addFourToIndexTwo() {
        int index = 2;
        int value = 5;
        int[][] initialMatrix = {{1,2,3},{4,5,6},{1,2}};
        int[][] initialExpected = {{1,2,3},{4,5,6},{1,2,5}};
        MatrixVersionII result = new MatrixVersionII(initialMatrix);
        MatrixVersionII expected = new MatrixVersionII(initialExpected);
        //Act
        result.addElement(value,index);
        // Assert
        assertEquals(expected,result);

    }
    @Test
    void addFiveToEmptyMatrix() {
        int index = 0;
        int value = 5;
        int[][] initialMatrix = {};
        int[][] initialExpected = {{5}};
        MatrixVersionII result = new MatrixVersionII(initialMatrix);
        MatrixVersionII expected = new MatrixVersionII(initialExpected);
        //Act
        result.addElement(value,index);
        // Assert
        assertEquals(expected,result);
    }
    @Test
    void negativeIndex() {
        int index = -1;
        int value = 5;
        int[][] initialMatrix = {{1,2}};
        MatrixVersionII result = new MatrixVersionII(initialMatrix);
        assertThrows(IndexOutOfBoundsException.class, () -> {
           result.addElement(value,index);
        });
    }
    @Test
    void removeFirstFour() {
        int value = 4;
        int[][] initialMatrix = {{1,2,4},{4,5,6}};
        MatrixVersionII result = new MatrixVersionII(initialMatrix);
        int[][] initialExpected = {{1,2},{4,5,6}};
        MatrixVersionII expected = new MatrixVersionII(initialExpected);
        //Act
        result.removeValue(value);
        //Assert
        assertEquals(expected,result);

    }
    @Test
    void valueDoesNotExistInVector() {
        int value = -10;
        int[][] initialMatrix = {{1,2,4},{4,5,6}};
        MatrixVersionII result = new MatrixVersionII(initialMatrix);
        int[][] initialExpected = {{1,2,4},{4,5,6}};
        MatrixVersionII expected = new MatrixVersionII(initialExpected);
        //Act
        result.removeValue(value);
        //Assert
        assertEquals(expected,result);

    }
    @Test
    void highestOfAll() {
        int[][] initialMatrix = {{1,2,4},{4,5,6},{4,19}};
        MatrixVersionII resultHighest = new MatrixVersionII(initialMatrix);
        int initialExpected = 19;
        //Act
        int result = resultHighest.highest();
        //Assert
        assertEquals(initialExpected,result);

    }
    @Test
    void highestWithNegativeValues() {
        int[][] initialMatrix = {{-1,-90,-4},{-7,-5},{-19,-3}};
        MatrixVersionII resultHighest = new MatrixVersionII(initialMatrix);
        int initialExpected = -1;
        //Act
        int result = resultHighest.highest();
        //Assert
        assertEquals(initialExpected,result);

    }
    @Test
    void smallestWithNegativeNumbers() {
        int[][] initialMatrix = {{-1,-90,-4},{-7,-5},{-19,-3}};
        MatrixVersionII smallests = new MatrixVersionII(initialMatrix);
        int initialExpected = -90;
        //Act
        int result = smallests.smallest();
        //Assert
        assertEquals(initialExpected,result);

    }
    @Test
    void smallestOfAll() {
        int[][] initialMatrix = {{10,20,30},{123,4567},{18384,8287}};
        MatrixVersionII smallests = new MatrixVersionII(initialMatrix);
        int initialExpected = 10;
        //Act
        int result = smallests.smallest();
        //Assert
        assertEquals(initialExpected,result);

    }
    @Test
    void smallestWhenEmpty() {
        int[][] initialMatrix = {{10,20,10},{ },{18384,8287}};
        MatrixVersionII smallests = new MatrixVersionII(initialMatrix);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
           int result = smallests.smallest();
        });

    }
    @Test
    void meanOfMatrix() {
        int[][] initialMatrix = {{10,20,10},{ },{18384,8287}};
        MatrixVersionII mean = new MatrixVersionII(initialMatrix);
        double expected = 3.0;
        //Act
        double result = mean.avg();
        //Assert
        assertEquals(expected,result,0.1);

    }
    /*@Test
    void meanWithNegativeValues() {
        int[][] initialMatrix = {{-10,20,10},{1},{18384,-8287}};
        MatrixVersionII mean = new MatrixVersionII(initialMatrix);
        double expected = 1686.3;
        //Act
        double result = mean.avg();
        //Assert
        assertEquals(expected,result, 0.1);

    }

     */
    /*@Test
    void sumOfLines() {
        int[][] initialMatrix = {{-10,20,10},{1},{18384,-8287}};
        MatrixVersionII resultant = new MatrixVersionII(initialMatrix);
        int[] sum = {20,1,10097};
        Vector initialExpected = new Vector(sum);
        //Act
        Vector result = resultant.linesSum();
        //Assert
        assertEquals(initialExpected,result);

    }

     */
    /* @Test
    void sumOfLinesII() {
        int[][] initialMatrix = {{1,2,3,4},{ },{50,40}};
        MatrixVersionII resultant = new MatrixVersionII(initialMatrix);
        int[] sum = {10,0,90};
        Vector initialExpected = new Vector(sum);
        //Act
        Vector result = resultant.linesSum();
        //Assert
        assertEquals(initialExpected,result);

    }

     */
    @Test
    void indexOfHighest() {
        int[][] initialMatrix = {{1,2,3,4},{0},{50,40}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        int expected = 2;
        //Act
        int result = initial.indexOfHighestSum();
        //Assert
        assertEquals(expected,result);

    }
    @Test
    void indexOfHighestII() {
        int[][] initialMatrix = {{100},{1,1,0},{50,40}, {1,0,0}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        int expected = 2;
        //Act
        int result = initial.indexOfHighestSum();
        //Assert
        assertEquals(expected,result);

    }
    @Test
    public void isNotSquare() {
        int[][] initialMatrix = {{100},{1,1,0},{50,40}, {1,0,0}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        //Act
        boolean result = initial.isSquare();
        //Assert
        assertFalse(result);

    }
    @Test
    public void isSquare() {
        int[][] initialMatrix = {{100,1,1},{1,1,0},{50,40,10}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        //Act
        boolean result = initial.isSquare();
        //Assert
        assertTrue(result);

    }
    @Test
    public void isSymmetric() {
        int[][] matrix = {{1, 1, -1}, {1, 2, 0}, {-1,0,5}};
        MatrixVersionII testMatrix = new MatrixVersionII(matrix);
        //ACT
        boolean result = testMatrix.isSymmetric();
        //ASSERT
        assertTrue(result);
    }
    @Test
    public void isNotSymmetric() {
        int[][] initialMatrix = {{100,1,1},{1,1,0},{50,40,10}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        //Act
        boolean result = initial.isSymmetric();
        //Assert
        assertFalse(result);

    }


    @Test
    public void getMainDiagonal() {
        int[][] initialMatrix = {{3,2,3},
                {4,5,6},
                {9,8,9}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        int[] other = {3,5,9};
        Vector expected = new Vector (other);
        //Act
        Vector result = initial.getMainDiagonal();
        //Assert
        assertEquals(expected,result);
    }
    @Test
    public void diagonalsEquals() {
        int[][] initialMatrix = {{3,2,3},
                                 {4,5,6},
                                 {9,8,9}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        //Act
        boolean result = initial.mainDiagonalEqualsSecondary();
        //Assert
        assertTrue(result);
    }
    @Test
    public void diagonalsNotEquals() {
        int[][] initialMatrix = {{1,2,3},
                {4,5,6},
                {7,8,9}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        //Act
        boolean result = initial.mainDiagonalEqualsSecondary();
        //Assert
        assertFalse(result);
    }
    @Test
    public void twoElementsNotNull() {
        int[][] initialMatrix = {{1,2,3},
                                {4,5,6},
                                {7,8,0}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        int expected = 2;
        //Act
        int result = initial.countNonNullElementsOfDiagonal();
        //Assert
        assertEquals(expected,result);
    }
    @Test
    public void zeroElementsNotNull() {
        int[][] initialMatrix = {{0,2,3},
                {4,0,6},
                {7,8,0}};
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        int expected = 0;
        //Act
        int result = initial.countNonNullElementsOfDiagonal();
        //Assert
        assertEquals(expected,result);
    }

    @Test
    public void invertedLines() {
        //ARRANGE
        int[][] initialMatrix = {{0,2,3},
                {4,0,6},
                {7,8,0}};
        int[][] expected = {{3,2,0},{6,0,4},{0,8,7}};
        MatrixVersionII expectedOne = new MatrixVersionII(expected);
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        //ACT
        initial.invertMatrixLines();
        //ASSERT
        assertEquals(expectedOne,initial);
    }
    @Test
    public void invertedLinesII() {
        //ARRANGE
        int[][] initialMatrix = {{0,9,9},
                {4},
                {7,8,0,1}};
        int[][] expected = {{9,9,0},{4},{1,0,8,7}};
        MatrixVersionII expectedOne = new MatrixVersionII(expected);
        MatrixVersionII initial = new MatrixVersionII(initialMatrix);
        //ACT
        initial.invertMatrixLines();
        //ASSERT
        assertEquals(expectedOne,initial);
    }
    @Test
    public void ninetyDegreesRotated() {
        int[][] matrix = {{1, 2, 9},
                {1, 2, 3},
                {12, 1, 11}};
        MatrixVersionII testMatrix = new MatrixVersionII(matrix);
        testMatrix.rotateNinetyDegrees();
        int[][] expected = {{12, 1, 1},
                {1, 2, 2},
                {11, 3, 9}};
        MatrixVersionII expectedMatrix = new MatrixVersionII(expected);

        assertEquals(testMatrix, expectedMatrix);
    }
    @Test
    public void ninetyDegreesRotatedPositive() {
        int[][] matrix = {{1, 2},
                {1, 3},
                {12, 11}};
        MatrixVersionII testMatrix = new MatrixVersionII(matrix);
        testMatrix.rotateNinetyDegrees();
        int[][] expected = {{12, 1, 1},
                {11, 3, 2}};
        MatrixVersionII expectedMatrix = new MatrixVersionII(expected);

        assertEquals(testMatrix, expectedMatrix);
    }






}
