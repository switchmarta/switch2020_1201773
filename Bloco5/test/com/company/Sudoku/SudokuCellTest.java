package com.company.Sudoku;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SudokuCellTest {
    @Test
    void inputInvalidNumberLessThanZero() {
        assertThrows(IllegalArgumentException.class, () -> {
            SudokuCell test = new SudokuCell(-1,false);
        });
    }

    @Test
    void inputInvalidNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            SudokuCell test = new SudokuCell(10,true);
        });
    }

    @Test
    void checkTrueForFixed() {
        SudokuCell test = new SudokuCell(5,true);
        assertTrue(test.checkIfIsFixed());

    }
    @Test
    void checkFalseForFixed() {
        SudokuCell test = new SudokuCell(2,false);
        assertFalse(test.checkIfIsFixed());

    }

    @Test
    void trueForEmpty() {
        SudokuCell test = new SudokuCell(0,false);
        assertTrue(test.checkIfIsEmpty());
    }
    @Test
    void falseForEmpty() {
        SudokuCell test = new SudokuCell(1,false);
        assertFalse(test.checkIfIsEmpty());
    }
    @Test
    void TrueForFixedNumber() {
        SudokuCell test = new SudokuCell(4,true);
        assertFalse(test.checkIfIsEmpty());
    }

    @Test
    void removeNonFixedNumber() {
        SudokuCell test = new SudokuCell(6,false);
        SudokuCell expected = new SudokuCell(0,false);
        test.removeNumber();
        assertEquals(expected,test);
    }
    @Test
    void throwExceptionForTryingRemoveFixedNumber() {
        SudokuCell test = new SudokuCell(3,true);
        assertThrows(IllegalArgumentException.class, () -> {
            test.removeNumber();
        });
    }

    @Test
    void changeNonFixedNumber() {
        SudokuCell test = new SudokuCell(6,false);
        SudokuCell expected = new SudokuCell(8,false);
        test.addOrChangeNumber(8);
        assertEquals(expected,test);
    }
    @Test
    void throwExceptionForTryingChangeFixedNumber() {
        SudokuCell test = new SudokuCell(6,true);
        assertThrows(IllegalArgumentException.class, () -> {
            test.addOrChangeNumber(8);
        });
    }
}