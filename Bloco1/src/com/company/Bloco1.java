package com.company;


import java.util.Scanner;

public class Bloco1 {

    public static void main(String[] args) {
       /* Temperatura myTemperatura= new Temperatura(670);
        System.out.println(myTemperatura.converterTemperaturaFahrenheit());*/
        Predio myAlturaPredio = new Predio(-9);
        System.out.println(myAlturaPredio.obterAltura());
    }

    public static void exercicio3() {
        double height, radius, volume;
        //Iniciar a ler.
        Scanner ler = new Scanner(System.in);
        //Ler pelo teclado
        System.out.println("Introduz altura: ");
        height = ler.nextDouble();
        System.out.println("Introduz raio: ");
        radius = ler.nextDouble();

        //Processamento
        volume = (Math.PI * Math.pow(radius, 2) * height) * 1000;
        //Imprimir para fora
        System.out.println("O volume do cilindro em litros é de " + String.format("%.2f", volume) + " litros");
    }

    public static double exercicio3_V2(double height, double radius) {

        double volume;

        //Processamento. No raio^2 poderíamos colocar math.pow().
        volume = (Math.PI * radius * radius * height) * 1000;

        return volume;
    }

    public static int exercicio4(int time) {
        //Estrutura dados
        int distance, airSpeed;

        //Processamento. Para saber distância do relâmpago, devemos multiplicar o tempo com a velocidade do ar.
        airSpeed = 340;// 1224000/3600. Passar velocidade km/h para m/s.
        distance = time * airSpeed;

        return distance;
    }

    public static double exercicio5(int time) {
        //Declarar variáveis
        double buildingHeight;
        double gravity = 9.8;

        //Processamento
        buildingHeight = (gravity * time * time) / 2;
        //Saída dados
        return buildingHeight;
    }

    public static double exercicio6(double humanHeight, double buildingShadow, double humanShadow) {
        //Declarar variáveis
        double buildingHeight;

        //Processamento
        buildingHeight = (buildingShadow / humanShadow) * humanHeight;

        return buildingHeight;


    }

    public static double exercicio7_V2() {
        //Declarar variáveis
        double manelTime = 14530; // (4*60*60 + 2*60 + 10)
        double zeTime = 3900; // (60*60 + 5*60)
        double manelDistance = 42195;
        double manelSpeed, zeSpeed;

        //Processamento
        manelSpeed = (manelDistance / manelTime);
        zeSpeed = (manelSpeed * zeTime);

        return zeSpeed;


    }

    public static double exercicio8() {
        //Declarar variáveis
        double a = 40;
        double b = 60;
        double y;
        double c;

        //Processamento
        y = Math.cos(Math.toRadians(60));
        c = Math.sqrt(a * a + b * b - 2 * a * b * y);

        return c;


    }

    public static void exercicio9() {
        double a, b, perimeter;
        //Iniciar a ler.
        Scanner ler = new Scanner(System.in);
        //Ler pelo teclado
        System.out.println("Introduz o valor de a: ");
        a = ler.nextDouble();
        System.out.println("Introduz o valor de b: ");
        b = ler.nextDouble();

        //Processamento
        perimeter = (2 * a + 2 * b);

        //Saída perímetro
        System.out.println("O perímetro do retângulo é " + String.format("%.2f", perimeter) + ".");
    }

    public static double exercicio9_V2(double a, double b) {
        double perimeter;
        perimeter = (2 * a + 2 * b);
        return perimeter;
    }

    public static void exercicio10() {
        double c1, c2, hipotenusa;
        //Iniciar a ler.
        Scanner ler = new Scanner(System.in);
        //Ler pelo teclado
        System.out.println("Introduz o valor do cateto 1: ");
        c1 = ler.nextDouble();
        System.out.println("Introduz o valor do cateto 2: ");
        c2 = ler.nextDouble();

        //Processamento
        hipotenusa = Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));

        //Resultado hipotenusa
        System.out.println("A hipotenusa é " + String.format("%.2f", hipotenusa));
    }

    public static double exercicio10_V2(double c1, double c2) {
        double hipotenusa;
        hipotenusa = Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));

        return hipotenusa;
    }

    public static void exercicio11() {
        double x, result;
        //Iniciar a ler.
        Scanner ler = new Scanner(System.in);
        //Ler pelo teclado
        System.out.println("Introduz o valor de x: ");
        x = ler.nextDouble();

        //Processamento
        result = (Math.pow(x, 2) - 3 * x + 1);

        //Apresentar resultado
        System.out.println("O resultado da expressão é " + String.format("%.2f", result));
    }

    public static double exercicioOnze(double x) {
        double result;
        result = (Math.pow(x, 2) - 3 * x + 1);
        return result;

    }

    public static void exercicio12() {
        double celsius, fahrenheit;
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a temperatura em Celsius:");
        celsius = ler.nextDouble();

        //Processamento
        fahrenheit = 32 + (1.8 * celsius);

        System.out.println("A temperatura correspondente em Fahrenheit é: " + fahrenheit);
    }

    public static double exercicio12_V2(double celsius) {
        double fahrenheit;
        fahrenheit = 32 + (9.0 / 5.0 * celsius);

        return fahrenheit;
    }

    public static void exercicio13() {
        int h, m, total;
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique as horas:");
        h = ler.nextInt();
        System.out.println("Indique os minutos:");
        m = ler.nextInt();

        //Processamento
        total = (h * 60) + m;

        System.out.println("O total dos minutos equivale a " + total);
    }

    public static int exercicio13_V2(int h, int m) {
        int total;
        total = (h * 60) + m;

        return total;
    }


}
