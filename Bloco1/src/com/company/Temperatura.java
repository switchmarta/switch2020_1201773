package com.company;

public class Temperatura {
    private double celsius;

    //Construtor
    public Temperatura(double celsius) {
        this.celsius = celsius;

    }
    //Método que converte temperatura.
    public double converterTemperaturaFahrenheit() {
        double fahrenheit = 32 + (1.8 * celsius);
        return fahrenheit;
    }

}
