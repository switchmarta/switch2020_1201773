package com.company;

public class Predio {
    private double gravidade = 9.8;
    private double tempoPedra = -1;

    //Construtor
    public Predio(double tempoPedra) {
        if(!isNumberValid(tempoPedra)) {
            throw new IllegalArgumentException("Valor Inválido!");
        }
        this.tempoPedra = tempoPedra;
    }
    //Método que é utilizado para calcular
    public double obterAltura() {
        double altura = (gravidade * Math.pow(tempoPedra, 2)) / 2;
        return altura;
    }
    //Método validação
    private boolean isNumberValid(double number) {
        return number > 0;
    }
}
