package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class Bloco1Test {

    @Test
    public void exercicio3_V2() {
        //arrange
        double radius = 2;
        double height = 4;
        double expected = 50265.48;

        //act
        double result = Bloco1.exercicio3_V2(height, radius);

        //assert
        assertEquals(expected, result, 0.01);


    }

    @Test
    public void exercicio4() {

        //arrange
        int time = 10;
        int expected = 3400;

        //act
        int result = Bloco1.exercicio4(time);

        //assert
        assertEquals(expected, result, 0.01);


    }

    @Test
    public void exercicio5() {
        //arrange
        int time = 4;
        double expected = 78.4;

        //act
        double result = Bloco1.exercicio5(time);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio6() {
        //arrange
        double humanHeight = 2;
        double buildingShadow = 40;
        double humanShadow = 7;
        double expected = 11.43;

        //act
        double result = Bloco1.exercicio6(humanHeight, buildingShadow, humanShadow);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio7_V2() {
        //arrange
        double expected = 11325.5677907777;

        //act
        double result = Bloco1.exercicio7_V2();

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void exercicio8() {
        //arrange
        double expected = 52.915;

        //act
        double result = Bloco1.exercicio8();

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void exercicio9_V2() {
        double a = 10;
        double b = 12;
        double expected = 44;

        double result = Bloco1.exercicio9_V2(a, b);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio10_V2() {
        double c1 = 10;
        double c2 = 12;
        double expected = 15.62;

        double result = Bloco1.exercicio10_V2(c1, c2);

        assertEquals(expected, result, 0.01);
    }

    //Verificar testes da formula resolvente!!
    @Test
    public void exercicioOnzePositivo() {
        double expected = 71;


        double result = Bloco1.exercicioOnze(10);

        assertEquals(expected, result, 0.1);

    }

    @Test
    public void exercicio12_V2() {
        double celsius = 20;
        double result;
        double expected = 68;

        result = Bloco1.exercicio12_V2(celsius);

        assertEquals(expected, result, 0.1);


    }

    @Test
    public void exercicio13_V2() {
        int h = 4;
        int m = 30;
        int result;
        int expected = 270;

        result = Bloco1.exercicio13_V2(h, m);

        assertEquals(expected, result);
    }

}

