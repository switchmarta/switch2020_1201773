package com.company;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        exercicio3();
        //exercicio4();
        //exercicio5();
        //exercicio6();
    }

        public static void exercicio3() {
            //Estrutura de dados
            double altura, raio, volume;

            //Iniciar leitura e leitura dos dados
            Scanner ler = new Scanner(System.in);

            System.out.println("Introduza a altura em metros");
            altura = ler.nextDouble();
            System.out.println("Introduza o raio em metros");
            raio = ler.nextDouble();

            //Processamento. No raio^2 poderíamos colocar math.pow().
            volume = getVolumeCilindro(altura, raio); //para poder transformar o volume em litros.

            //Saída dados
            System.out.println ("O vasilhame pode levar até " + String.format("%.2f", volume) + " litros");
        }

    private static double getVolumeCilindro(double altura, double raio) {
        return (Math.PI * raio * raio * altura) * 1000;
    }

    public static void exercicio4() {
        //Exercício 4
        //Estrutura dados
        int tempo, distancia;
        int velocidadeAr = 340; // 1224000/3600. Passar velocidade km/h para m/s.

        //Iniciar leitura e leitura dos dados
        System.out.println("Introduza o tempo em segundos");
        Scanner ler = new Scanner(System.in);
        tempo = ler.nextInt();

        //Processamento. Para saber distância do relâmpago, devemos multiplicar o tempo com a velocidade do ar.
        distancia = getDistance(tempo, velocidadeAr);

        //Saída dados
        System.out.println("A distância é de " + distancia);
    }

    public static int getDistance(int tempo, int velocidadeAr) {
        return tempo * velocidadeAr;
    }
    public static void exercicio5() {
        //Declarar variáveis
        int tempo;
        double alturaPredio;
        double gravidade = 9.8;

        //Iniciar leitura e leitura dos dados
        System.out.println("Introduza o tempo em segundos");
        Scanner ler = new Scanner(System.in);
        tempo = ler.nextInt();

        //Processamento
        alturaPredio = getAlturaPredio(tempo, gravidade);
        //Saída dados
        System.out.println("A altura do prédio é de " + alturaPredio + " metros");
    }

    public static double getAlturaPredio(int tempo, double gravidade) {
        return (gravidade * tempo * tempo) / 2;
    }
    public static void exercicio6() {
        //Declarar variáveis
        double hPredio, hHumano, sombraPredio, sombraHumano;

        //Iniciar leitura e leitura dos dados
        System.out.println("Introduza a sua altura em metros");
        Scanner ler = new Scanner(System.in);
        hHumano = ler.nextDouble();
        System.out.println("Introduza o comprimento da sua sombra");
        sombraHumano = ler.nextDouble();
        System.out.println("Introduza o comprimento da sombra do prédio");
        sombraPredio = ler.nextDouble();

        //Processamento
        hPredio = gethPredio(hHumano, sombraPredio, sombraHumano);

        //Saída dados
        System.out.println("A altura do prédio é " + hPredio + " metros");


    }

    public static double gethPredio(double hHumano, double sombraPredio, double sombraHumano) {
        return (sombraPredio / sombraHumano) * hHumano;
    }
    public static void exercicio7() {

    }
}
