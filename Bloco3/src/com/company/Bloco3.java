package com.company;


import java.util.Scanner;

public class Bloco3 {

    public static void main(String[] args) {
        //exercicioUm();
        //exercicioDois(15);
        //exercicioDois();
        //exercicioTres();
        //exercicioOito();
        //exercicioNove();
        //exercicioDez();
        //exercicioOnze(5);
        //exercicioDoze(9,6,1);
        //exercicioTreze();
        //exercicioCatorze();
        //exercicioDezasseteAlineaB();
        exercicioDezoito();
        //dezoitoObterSomaPonderada(14307523);
        //exercicioDezanove();

    }

    //Problema 1 é para fazer fatorial de um número.
    public static void exercicioUm() {
        //Declarar variáveis
        int num;
        int res = 1;


        //Ler número introduzido.
        Scanner ler = new Scanner(System.in);
        System.out.println("Digite um número:");
        num = ler.nextInt();
        res = exercicioUmModularizado(num);
        //Escrever resultado
        System.out.println("O resultado é: " + res);
    }

    public static int exercicioUmModularizado(int num) {
        int res = 1;
        int x;
        //Garantir que função é finita é impossibilitar nº negativos.
        if (num < 0) {
            return 0; //Quando número é negativo ou zero!
        }
        for (x = num; x > 0; x--) {
            res = res * x;
        }
        return res;
    }

    public static void exercicioDois() {
        //Leitura de quantos alunos existem e as suas notas.
        Scanner ler = new Scanner(System.in);
        System.out.println("Digite o número de alunos:");
        int numeroAlunos = ler.nextInt();
        double[] notasAlunos = new double[numeroAlunos];
        System.out.println("Colocar notas dos alunos por ordem:");
        for (int i = 0; i < notasAlunos.length; i++) {
            notasAlunos[i] = ler.nextInt();
        }
        String percentagemMediaNotas = obterPercentagemPositivasMediaNegativas(numeroAlunos, notasAlunos);
        System.out.println(percentagemMediaNotas); //mudar isto!
    }

    public static String obterPercentagemPositivasMediaNegativas(int alunos, double[] notasAlunos) {
        int countPositive = 0;
        int countNegative = 0;
        double soma = 0;
        if (alunos < 0) { //Garantir que o nº colocado não seja 0, uma vez que a turma tem que ter alunos.
            return "Colocar nº alunos válido!";
        }
        for (int i = 0; i < alunos; i++) {
            double notas = notasAlunos[i];
            //guardar array numa variável para "gravar". neste passo é necessário?
            if (notas > 9.50) {
                countPositive++; //Contar quantas positivas houve no total.
            } else {
                soma = soma + notasAlunos[i]; //Soma das negativas que houve na turma.
                countNegative++;
            }
        }
        double media = soma / countNegative;
        double percentagemPositivos = (double) (countPositive * 100) / (notasAlunos.length);
        return "A média das negativas é : " + String.format("%.2f", media) + " e a percentagem dos positivos é de " + String.format("%.2f", percentagemPositivos) + " %";
    }

    //Resolvido com Math.Random.
    public static String exercicioDoisComNotasAleatorias(int alunos, double[] notasAlunos) {
        int min = 0;
        int max = 20;
        double range = max - min;
        int countPositive = 0;
        int countNegative = 0;
        double soma = 0;
        if (alunos <= 0) {
            return "Número de alunos inválido!";
        }
        for (int i = 0; i < notasAlunos.length; i++) {
            notasAlunos[i] = (Math.random() * range) + min;

            if (notasAlunos[i] > 9.50) {
                countPositive++;
            } else {
                soma = soma + notasAlunos[i];
                countNegative++;
            }
        }
        double media = soma / countNegative;
        double percentagemPositivos = (double) (countPositive * 100) / (notasAlunos.length);
        return "A média das negativas é : " + String.format("%.2f", media) + " e a percentagem dos positivos é de " + String.format("%.2f", percentagemPositivos) + " %";
    }

    public static void exercicioTres() {
        //Introduzir tamanho da sequência e declarar variáveis.
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir tamanho sequência:");
        int tamanho = ler.nextInt();
        int[] sequenciaNumero = new int[tamanho];
        System.out.println("Colocar os valores da sequência:");
        //Colocar valores para o array da sequência.
        for (int i = 0; i < tamanho; i++) {
            sequenciaNumero[i] = ler.nextInt();
            if (sequenciaNumero[tamanho - 1] > 0) {
                System.out.println("Colocar último nº negativo!");
                return;
            }
        }
        String percentagemPares = obterPercentagemPares(tamanho, sequenciaNumero);
        System.out.println(percentagemPares);
    }

    //Resolvido com array.
    public static String obterPercentagemPares(int tamanho, int[] sequenciaNumero) {
        //adicionar a validação para nº negativo (ver resolução discord) Não utilizar arrays.
        int contagemPares = 0, contagemImpares = 0;
        int somaImpares = 0;
        for (int i = 0; i < tamanho; i++) {
            if (sequenciaNumero[i] % 2 == 0) {
                contagemPares = contagemPares + 1;
            } else {
                somaImpares = somaImpares + sequenciaNumero[i];
                contagemImpares = contagemImpares + 1;
            }
        }
        double resultadoPares = (contagemPares * 100) / sequenciaNumero.length;
        double resultadoImpares = somaImpares / contagemImpares;
        return "A percentagem de números pares é de: " + String.format("%.2f", resultadoPares) + " e a média dos ímpares é: " + String.format("%.1f", resultadoImpares);

    }

    public static int exercicioQuatroAlineaA(int lim1, int lim2) {
        int multiplos = 0;
        for (int num = lim1; num <= lim2; num++) {
            int numero = num;
            if (numero % 3 == 0) {
                multiplos += 1;
            }
        }
        return multiplos;
    }

    public static int exercicioQuatroAlineaB(int numeroDado, int lim1, int lim2) {
        int multiplos = 0;

        for (int num = lim1; num <= lim2; num++) {
            int numero = num;
            if (numero % numeroDado == 0) {
                multiplos += 1;
            }
        }
        return multiplos;
    }

    public static String exercicioQuatroAlineaC(int lim1, int lim2) {
        int tresMultiplos = exercicioQuatroAlineaB(3, lim1, lim2);
        int cincoMultiplos = exercicioQuatroAlineaB(5, lim1, lim2);
        return "A quantidade de múltiplos de 3 é: " + tresMultiplos + " e de 5 são: " + cincoMultiplos;
    }

    public static String exercicioQuatroAlineaD(int numeroDado, int numeroDado2, int lim1, int lim2) {
        int numeroDadoMultiplos = exercicioQuatroAlineaB(numeroDado, lim1, lim2);
        int numeroDadoDoisMultiplos = exercicioQuatroAlineaB(numeroDado2, lim1, lim2);
        return "A quantidade de múltiplos de " + numeroDado + " é: " + numeroDadoMultiplos + " e de " + numeroDado2 + " é: " + numeroDadoDoisMultiplos;
    }

    public static int exercicioQuatroAlineaE(int numeroDado, int numeroDado2, int lim1, int lim2) {
        int numeroDadoMultiplos = exercicioQuatroAlineaB(numeroDado, lim1, lim2);
        int numeroDadoDoisMultiplos = exercicioQuatroAlineaB(numeroDado2, lim1, lim2);
        return numeroDadoMultiplos + numeroDadoDoisMultiplos;
    }

    public static int exercicioCincoAlineaASoma(int algarismo, int limit1, int limit2) {
        int soma = 0;
        for (int i = limit1; i <= limit2; i++) {
            if (i % 2 == algarismo) {
                soma += i;
            }
        }
        return soma;
    }

    public static int exercicioCincoAlineaBQuantidade(int numDado, int algarismo, int limit1, int limit2) {
        int quantidade = 0;
        for (int i = limit1; i <= limit2; i++) {
            if (i % numDado == algarismo) {
                quantidade += 1;
            }
        }
        return quantidade;
    }

    public static int exercicioCincoAlineaC(int limit1, int limit2) {
        int somaImpares = exercicioCincoAlineaASoma(1, limit1, limit2);
        return somaImpares;
    }

    public static int exercicioCincoAlineaD(int limit1, int limit2) {
        int quantidadeImpares = exercicioCincoAlineaBQuantidade(2, 1, limit1, limit2);
        return quantidadeImpares;
    }

    public static int exercicioCincoAlineaE(int numDado, int limit1, int limit2) {
        int soma = 0;
        int min = 0, max = 0;
        if (limit1 > limit2) {
            max = limit1;
            min = limit2;
        } else {
            max = limit2;
            min = limit1;
        }
        for (int i = min; i <= max; i++) {
            if (i % numDado == 0) {
                soma += i;
            }
        }
        return soma;
    }

    public static int exercicioCincoAlineaF(int numDado, int limit1, int limit2) {
        int produto = 1; //Se iniciar com 0, o produto vai dar sempre 0.
        for (int i = limit1; i <= limit2; i++) {
            if (i % numDado == 0) {
                produto = produto * i;
            }
        }
        return produto;

    }

    public static double exercicioCincoAlineaG(int numDado, int limit1, int limit2) {
        int somaMultiplos = exercicioCincoAlineaE(numDado, limit1, limit2);
        int quantidadeMultiplos = exercicioCincoAlineaBQuantidade(numDado, 0, limit1, limit2);
        double mediaMultiplos = somaMultiplos / quantidadeMultiplos * 1.0;
        return mediaMultiplos;

    }

    public static String exercicioCincoAlineaH(int x, int y, int limit1, int limit2) {
        double mediaMultiplosX = exercicioCincoAlineaG(x, limit1, limit2);
        double mediaMultiplosY = exercicioCincoAlineaG(y, limit1, limit2);
        return "A média dos múltiplos do valor " + x + " é " + mediaMultiplosX + " e a média dos múltiplos do valor de " + y + " é " + mediaMultiplosY;
    }

    public static int exercicioSeisAlineaA(int num) {
        int quantidade = 1;
        while (num > 9) {
            num /= 10;

            quantidade += 1;
        }
        return quantidade;
    }

    public static int exercicioSeisAlineaBQuantidadePares(int num) {
        int quantidadePares = 0;
        while (num > 0) {
            if ((num % 10) % 2 == 0 && (num % 10) != 0) {
                quantidadePares += 1;
            }
            num /= 10;
        }
        return quantidadePares;
    }

    public static int exercicioSeisAlineaC(int num) {
        int quantidadeImpares = 0;
        while (num > 0) {
            if ((num % 10) % 2 == 1 && (num % 10) != 0) {
                quantidadeImpares += 1;
            }
            num /= 10;
        }
        return quantidadeImpares;
    }

    public static int exercicioSeisAlineaD(int num) {
        int somaDigitosNumero = 0;
        while (num > 0) {
            somaDigitosNumero += (num % 10);
            num /= 10;
        }
        return somaDigitosNumero;
    }

    public static int exercicioSeisAlineaE(int num) {
        int somaDigitosNumPares = 0;
        while (num > 0) {
            if ((num % 10) % 2 == 0 && (num % 10) != 0) {
                somaDigitosNumPares += (num % 10);
            }
            num /= 10;
        }
        return somaDigitosNumPares;
    }

    public static int exercicioSeisAlineaF(int num) {
        int somaDigitosNumImpares = 0;
        while (num > 0) {
            if ((num % 10) % 2 != 0 && (num % 10) != 0) {
                somaDigitosNumImpares += (num % 10);
            }
            num /= 10;
        }
        return somaDigitosNumImpares;
    }

    public static double exercicioSeisAlineaG(int num) {
        int somaDigitos = exercicioSeisAlineaD(num);
        int quantidadeDigitos = exercicioSeisAlineaA(num);
        double mediaDigitosNum = somaDigitos / (quantidadeDigitos * 1.0);

        return mediaDigitosNum;
    }

    public static double exercicioSeisAlineaH(int num) {
        int somaDigitosPares = exercicioSeisAlineaE(num);
        int quantidadeDigitos = exercicioSeisAlineaA(num);
        double mediaDigitosNumPares = somaDigitosPares / (quantidadeDigitos * 1.0);

        return mediaDigitosNumPares;
    }

    public static double exercicioSeisAlineaI(int num) {
        int somaDigitosImpares = exercicioSeisAlineaF(num);
        int quantidadeDigitos = exercicioSeisAlineaA(num);
        double mediaDigitosNumPares = somaDigitosImpares / (quantidadeDigitos * 1.0);

        return mediaDigitosNumPares;
    }

    public static int exercicioSeisAlineaJ(int num) {
        int numRevertido = 0;
        while (num > 0) {
            numRevertido = (numRevertido * 10) + (num % 10);
            num /= 10;
        }
        return numRevertido;

    }

    public static boolean exercicioSeteAlineaA(int num) {
        boolean capicua = false;
        int numRevertido = 0;
        while (num > 0) {
            numRevertido = (numRevertido * 10) + (num % 10);
            num /= 10;
            if (numRevertido == num) {
                capicua = true;
            }
        }
        return capicua;
    }

    public static boolean exercicioSeteAlineaB(int num) {
        boolean armstrongNum = false;
        int somaAlgarismo = 0;
        int numOriginal = num;
        while (num > 0) {
            somaAlgarismo = somaAlgarismo + (int) Math.pow(num % 10, 3);
            num /= 10;
        }
        if (numOriginal == somaAlgarismo) {
            armstrongNum = true;
        }
        return armstrongNum;
    }

    public static int exercicioSeteAlineaC(int limit1, int limit2) {
        int resultado = -1;
        for (int i = limit1; i <= limit2; i++) {
            if (exercicioSeteAlineaA(i)) {
                resultado = i;
                return resultado;
            }
        }
        return resultado;
    }

    public static int exercicioSeteAlineaD(int limit1, int limit2) {
        int maiorCapicua = 0;
        for (int i = limit1; i <= limit2; i++) {
            if (exercicioSeteAlineaA(i) && i > maiorCapicua) {
                maiorCapicua = i;
            }
        }
        return maiorCapicua;

    }

    public static int exercicioSeteAlineaE(int limit1, int limit2) {
        int countCapicuas = 0;
        for (int i = limit1; i <= limit2; i++) {
            if (exercicioSeteAlineaA(i)) {
                countCapicuas++;
            }
        }
        return countCapicuas;
    }

    public static int exercicioSeteAlineaF(int limit1, int limit2) {
        int resultado = -1;
        for (int i = limit1; i <= limit2; i++) {
            if (exercicioSeteAlineaB(i)) {
                resultado = i;
                return resultado;
            }
        }
        return resultado;
    }

    public static int exercicioSeteAlineaG(int limit1, int limit2) {
        int countArmstrong = 0;
        for (int i = limit1; i <= limit2; i++) {
            if (exercicioSeteAlineaB(i)) {
                countArmstrong++;
            }
        }
        return countArmstrong;
    }

    public static void exercicioOito() {
        int num, soma = 0, i;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir um número positivo: ");
        num = ler.nextInt();
        int menorNum = num;
        if (num <= 0) {
            System.out.println("Número inválido!");
        }
        do {
            System.out.println("Introduzir um número positivo para soma: ");
            i = ler.nextInt();
            soma += i;
            if (i < menorNum) {
                menorNum = i;
            }

        } while (soma <= num);

        System.out.println(menorNum);
    }

    public static void exercicioNove() {
        int totalFuncionarios = 0, hExtras, salarioBase;
        double mediaSalario, salarioMensal, somaSalario = 0;
        do {
            Scanner ler = new Scanner(System.in);
            System.out.println("Quantas horas extras fez?");
            hExtras = ler.nextInt();
            if (hExtras < -1) {
                System.out.println("Número inválido!");
            } else if (hExtras >= 0) {
                System.out.println("Qual salário base?");
                salarioBase = ler.nextInt();
                if (salarioBase <= 0) {
                    System.out.println("Erro salário!");
                }
                salarioMensal = salarioBase + (hExtras * salarioBase * 0.02);
                System.out.print("O salário mensal é: " + salarioMensal + " ");
                somaSalario += salarioMensal;
                totalFuncionarios += 1;
            }

        } while (hExtras != -1);
        mediaSalario = somaSalario / totalFuncionarios;
        System.out.println("A média salários é " + mediaSalario);


    }

    public static void exercicioDez() {
        int num, produto = 1, i;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduzir um número positivo: ");
        num = ler.nextInt();
        int maiorNum = 0;
        if (num <= 0) {
            System.out.println("Número inválido!");
        }
        do {
            System.out.println("Introduzir um número positivo para multiplicar: ");
            i = ler.nextInt();
            produto *= i;
            if (i > maiorNum) {
                maiorNum = i;
            }

        } while (produto <= num);

        System.out.println(maiorNum);

    }

    public static int exercicioOnze(int num) {
        int quantidade = 0;
        if (num < 1 || num > 20) {
            System.out.println("Número não está dentro intervalo.");
        }
        for (int i = 0; i <= 10; i++) {
            for (int j = i; j <= 10; j++) {
                if ((i + j) == num) {
                    quantidade++;
                }
            }
        }
        return quantidade;
    }

    public static String exercicioDoze(int a, int b, int c) {
        double delta = Math.pow(b, 2) - (4 * a * c);
        double resultado, resultadoDois;
        String valores = " ";

        if (a == 0 || b == 0 || c == 0) {
            valores = "Não é função quadrática";
        } else {
            if (delta > 0) {
                resultado = (-b - (Math.sqrt(delta))) / (2 * a);
                resultadoDois = (-b + (Math.sqrt(delta))) / (2 * a);
                valores = String.format("%.2f", resultado) + " + " + String.format("%.2f", resultadoDois);
            } else if (delta == 0) {
                resultado = (-b - (Math.sqrt(delta))) / (2 * a);
                valores = String.format("%.2f", resultado);
            } else {
                valores = "Resultado com raízes imaginárias";
            }
        }
        return valores;
    }

    public static void exercicioTreze() {
        int num;
        do {
            Scanner ler = new Scanner(System.in);
            System.out.println("Introduza código: ");
            num = ler.nextInt();
            System.out.println(exercicioTrezeParaTeste(num));
        } while (num != 0);
    }

    public static String exercicioTrezeParaTeste(int codigo) {
        String resultado = " ";
        if (codigo == 1) {
            resultado = "Alimento Não Perecível";
        } else if (codigo >= 2 && codigo <= 4) {
            resultado = "Alimento Perecível";
        } else if (codigo == 5 || codigo == 6) {
            resultado = "Vestuário";
        } else if (codigo == 7) {
            resultado = "Higiene Pessoal";
        } else if (codigo >= 8 && codigo <= 15) {
            resultado = "Limpeza e Utensílios Domésticos";
        } else
            resultado = "Código Inválido";

        return resultado;
    }

    public static void exercicioCatorze() {
        char codigo;
        char codigoDois;
        double num, resultado = 0;
        do {
            Scanner ler = new Scanner(System.in);
            System.out.println("Introduza valor em Euros: ");
            num = ler.nextInt();
            if (num > 0) {
                System.out.println("Qual opção câmbio: ");
                codigo = ler.next().charAt(0);
                codigoDois = Character.toUpperCase(codigo);
                switch (codigoDois) {
                    case 'D':
                        resultado = (1.534 * num);
                        break;
                    case 'L':
                        resultado = (0.774 * num);
                        break;
                    case 'I':
                        resultado = (161.480 * num);
                        break;
                    case 'C':
                        resultado = (9.593 * num);
                        break;
                    case 'F':
                        resultado = (1.601 * num);
                        break;
                    default:
                        System.out.println("Código Inválido!");
                }
            }
            System.out.println(resultado);
        } while (num >= 0);
    }

    public static double exercicioDezasseis(double salarioBruto) {
        double salarioLiquido, imposto;
        if (salarioBruto <= 0) {
            salarioLiquido = -1;
        } else if (salarioBruto <= 500) {
            imposto = salarioBruto * 0.1;
            salarioLiquido = salarioBruto - imposto;
        } else if (salarioBruto > 500 && salarioBruto <= 1000) {
            imposto = salarioBruto * 0.15;
            salarioLiquido = salarioBruto - imposto;
        } else {
            imposto = salarioBruto * 0.2;
            salarioLiquido = salarioBruto - imposto;
        }
        return salarioLiquido;
    }

    public static String exercicioDezasseteAlineaARaca(double peso) {
        String resultado;
        if (peso <= 10.0) {
            resultado = "É de raça pequena";
        } else if (peso > 10.0 && peso <= 25.0) {
            resultado = "É de raça média";
        } else if (peso > 25.0 && peso <= 45.0) {
            resultado = "É de raça grande";
        } else {
            resultado = "É de raça gigante";
        }
        return resultado;
    }

    public static String exercicioDezasseteAlineaAComida(double peso, double comida) {
        String resultado;
        if (peso <= 10.0) {
            if (comida == 100.0) {
                resultado = "A quantidade é adequada para o animal.";
            } else {
                resultado = "Quantidade não adequada. Tenha isso em atenção!";
            }
        } else if (peso > 10.0 && peso <= 25.0) {
            if (comida == 250.0) {
                resultado = "A quantidade é adequada para o animal.";
            } else {
                resultado = "Quantidade não adequada. Tenha isso em atenção!";
            }

        } else if (peso > 25.0 && peso <= 45.0) {
            if (comida == 300.0) {
                resultado = "A quantidade é adequada para o animal.";
            } else {
                resultado = "Quantidade não adequada. Tenha isso em atenção!";
            }

        } else {
            if (comida == 500.0) {
                resultado = "A quantidade é adequada para o animal.";
            } else {
                resultado = "Quantidade não adequada. Tenha isso em atenção!";
            }
        }
        return resultado;
    }

    public static void exercicioDezasseteAlineaB() {
        double peso, comida;
        String comidaAdequada, tipoRaca;
        do {
            Scanner ler = new Scanner(System.in);
            System.out.println("Indique o peso do canídeo em Kg");
            peso = ler.nextDouble();
            if (peso > 0) {
                tipoRaca = exercicioDezasseteAlineaARaca(peso);
                System.out.println("Indique a ração que dá em gramas");
                comida = ler.nextDouble();
                comidaAdequada = exercicioDezasseteAlineaAComida(peso, comida);
                System.out.println(tipoRaca + ". " + comidaAdequada);
            } else {
                System.out.println("O peso tem que ser positivo.");
            }
        } while (peso >= 0);
    }

    public static void exercicioDezoito() {
        String numeroCC;
        int somaPonderada;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o nº do CC");
        numeroCC = ler.nextLine();
        String numero = "";
        for (int i = 0; i < 10; i++) {
            if (numeroCC.charAt(i) != ' ') { //PARA IR BUSCAR À STRING OS NUMEROS DO CC ATÉ AO Nº CONTROLO.
                numero += numeroCC.charAt(i); //GUARDAR ESSES NºS.
            }
        }
        int numeroFinal = Integer.parseInt(numero); //PASSAR STRING PARA INTEIRO.
        somaPonderada = dezoitoObterSomaPonderada(numeroFinal);
        if ((somaPonderada % 11) == 0) {
            System.out.println("O número está correto");
        } else {
            System.out.println("O número está errado");
        }
    }

    public static int dezoitoObterSomaPonderada(int numCC) {
        int somaPonderada = 0;
        for (int i = 0; i < 10; i++) {
            somaPonderada += (numCC % 10) * (i + 1);
            numCC /= 10;
        }
        return somaPonderada;
    }

    public static void exercicioDezanove() {
        int sequencia, numPar = 0, numImpar = 0;
        String numOrganizado;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a sequencia");
        sequencia = ler.nextInt();
        if (sequencia < 0) {
            System.out.println("Sequência inválida");
        }
        numOrganizado = obterSequenciaNumImparNumParOrganizado(sequencia);
        System.out.println(numOrganizado);
    }

    public static String obterSequenciaNumImparNumParOrganizado(int sequencia) {
        int numPar = 0, numImpar = 0;
        while (sequencia > 0) {
            if ((sequencia % 10) % 2 == 0 && (sequencia % 10) != 0) {
                numPar = (numPar * 10) + (sequencia % 10);
            } else {
                numImpar = (numImpar * 10) + (sequencia % 10);
            }
            sequencia /= 10;
        }
        return numImpar + "" + numPar;
    }
}

