import com.company.Bloco3;
import org.junit.Test;

import static org.junit.Assert.*;

public class Bloco3Test {
    //Testes exercicio 1
    @Test
    public void testIntroduzirNumerosNegativos() {
        //Arrange
        int expected = 0;
        //Act
        int result = Bloco3.exercicioUmModularizado(-2);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testFatorialDeCinco() {
        //Arrange
        int expected = 120;
        //Act
        int result = Bloco3.exercicioUmModularizado(5);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testFatorialDeDez() {
        //Arrange
        int expected = 3628800;
        //Act
        int result = Bloco3.exercicioUmModularizado(10);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercicio 2
    @Test
    public void testQuandoAlunosTemNumNegativo() {
        //Arrange
        String expected = "Colocar nº alunos válido!";
        double[] notasAlunos = {9, 14.5, 11, 5};
        //Act
        String result = Bloco3.obterPercentagemPositivasMediaNegativas(-2, notasAlunos);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testPercentagemMediaComQuatroEntradas() {
        //Arrange
        String expected = "A média das negativas é : 7,00 e a percentagem dos positivos é de 50,00 %";
        double[] notasAlunos = {9, 14.5, 11, 5};
        //Act
        String result = Bloco3.obterPercentagemPositivasMediaNegativas(4, notasAlunos);
        //Assert
        assertEquals(expected, result);
    }

    //Testes exercicio 3
    @Test
    public void testPercentagemNumParesMediaImpares() {
        //Arrange
        String expected = "A percentagem de números pares é de: 50,00 e a média dos ímpares é: 4,0";
        int[] sequenciaNum = {2, 3, 5, -2};
        //Act
        String result = Bloco3.obterPercentagemPares(4, sequenciaNum);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 4 alínea A
    @Test
    public void testParaSeteMultiplosDeTres() {
        //Arrange
        int expected = 7;
        //Act
        int result = Bloco3.exercicioQuatroAlineaA(10, 30);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testParaDoisMultiplosDeTres() {
        //Arrange
        int expected = 2;
        //Act
        int result = Bloco3.exercicioQuatroAlineaA(10, 15);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 4 alínea B
    @Test
    public void testParaMultiplosNumQuatro() {
        //Arrange
        int expected = 5;
        //Act
        int result = Bloco3.exercicioQuatroAlineaB(4, 10, 30);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 4 alínea C
    @Test
    public void testParaMultiplosTresCinco() {
        //Arrange
        String expected = "A quantidade de múltiplos de 3 é: 7 e de 5 são: 5";
        //Act
        String result = Bloco3.exercicioQuatroAlineaC(10, 30);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 4 alínea D
    @Test
    public void testParaMultiplosQuatroSeis() {
        //Arrange
        String expected = "A quantidade de múltiplos de 4 é: 5 e de 6 é: 4";
        //Act
        String result = Bloco3.exercicioQuatroAlineaD(4, 6, 10, 30);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercício 4 alínea E
    @Test
    public void testParaSomaMultiplos() {
        //Arrange
        int expected = 9;
        //Act
        int result = Bloco3.exercicioQuatroAlineaE(4, 6, 10, 30);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 5 alínea A
    @Test
    public void testSomarParesZeroEDez() {
        //Arrange
        int expected = 90;
        //Act
        int result = Bloco3.exercicioCincoAlineaASoma(0, 10, 20);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSomarParesTrintaETrintaCinco() {
        //Arrange
        int expected = 96;
        //Act
        int result = Bloco3.exercicioCincoAlineaASoma(0, 30, 35);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 5 alínea B
    @Test
    public void testQuantidadePares() {
        //Arrange
        int expected = 8;
        //Act
        int result = Bloco3.exercicioCincoAlineaBQuantidade(2, 0, 15, 30);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuantidadeParesNoveDoze() {
        //Arrange
        int expected = 2;
        //Act
        int result = Bloco3.exercicioCincoAlineaBQuantidade(2, 0, 9, 12);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 5 alínea C
    @Test
    public void testSomaImparesTrintaCinquentaCinco() {
        //Arrange
        int expected = 559;
        //Act
        int result = Bloco3.exercicioCincoAlineaC(30, 55);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSomaImparesUmCinco() {
        //Arrange
        int expected = 9;
        //Act
        int result = Bloco3.exercicioCincoAlineaC(1, 5);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 5 alínea D
    @Test
    public void testQuantidadeImparesEntreTrintaCinquentaCinco() {
        //Arrange
        int expected = 13;
        //Act
        int result = Bloco3.exercicioCincoAlineaD(30, 55);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuantidadeImparesDoisADez() {
        //Arrange
        int expected = 4;
        //Act
        int result = Bloco3.exercicioCincoAlineaD(2, 10);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 5 alínea E
    @Test
    public void testSomaMultiplosTres() {
        //Arrange
        int expected = 450;
        //Act
        int result = Bloco3.exercicioCincoAlineaE(3, 55, 20);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSomaMultiplosDez() {
        //Arrange
        int expected = 140;
        //Act
        int result = Bloco3.exercicioCincoAlineaE(10, 55, 20);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 5 alínea F
    @Test
    public void testProdutoMultiplosDois() {
        //Arrange
        int expected = 1680;
        //Act
        int result = Bloco3.exercicioCincoAlineaF(2, 10, 15);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testProdutoMultiplosCinco() {
        //Arrange
        int expected = 10;
        //Act
        int result = Bloco3.exercicioCincoAlineaF(5, 8, 10);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercicio 5 alínea G
    @Test
    public void testMediaMultiplos() {
        //Arrange
        double expected = 25.0;
        //Act
        double result = Bloco3.exercicioCincoAlineaG(2, 20, 30);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Teste exercício 5 alínea H
    @Test
    public void testMediaMultiplosXY() {
        //Arrange
        String expected = "A média dos múltiplos do valor 2 é 6.0 e a média dos múltiplos do valor de 3 é 6.0";
        //Act
        String result = Bloco3.exercicioCincoAlineaH(2, 3, 2, 10);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercício 6 alínea A
    @Test
    public void testQuandoNumCentoQuarentaQuatro() {
        //Arrange
        int expected = 3;
        //Act
        int result = Bloco3.exercicioSeisAlineaA(144);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuandoNumMilSeicentosCinquenta() {
        //Arrange
        int expected = 6;
        //Act
        int result = Bloco3.exercicioSeisAlineaA(160435);
        //Assert
        assertEquals(expected, result);
    }

    //Testes exercício 6 alínea B
    @Test
    public void testQuandoExistemQuatroPares() {
        //Arrange
        int expected = 4;
        //Act
        int result = Bloco3.exercicioSeisAlineaBQuantidadePares(144223);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuandoExisteUmPar() {
        //Arrange
        int expected = 1;
        //Act
        int result = Bloco3.exercicioSeisAlineaBQuantidadePares(140);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 6 alínea C
    @Test
    public void testQuandoExisteUmImpar() {
        //Arrange
        int expected = 1;
        //Act
        int result = Bloco3.exercicioSeisAlineaC(140);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuandoExistemSeteImpares() {
        //Arrange
        int expected = 7;
        //Act
        int result = Bloco3.exercicioSeisAlineaC(14337991);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 6 alínea D
    @Test
    public void testQuandoSomaCinco() {
        //Arrange
        int expected = 5;
        //Act
        int result = Bloco3.exercicioSeisAlineaD(140);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuandoSomaDezasseis() {
        //Arrange
        int expected = 16;
        //Act
        int result = Bloco3.exercicioSeisAlineaD(14533);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 6 alínea E
    @Test
    public void testQuandoSomaParesOito() {
        //Arrange
        int expected = 8;
        //Act
        int result = Bloco3.exercicioSeisAlineaE(14522);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuandoSomaZeroPares() {
        //Arrange
        int expected = 0;
        //Act
        int result = Bloco3.exercicioSeisAlineaE(13599);
        //Assert
        assertEquals(expected, result);
    }

    //Testes exercícios alínea F
    @Test
    public void testQuandoSomaVinteImpares() {
        //Arrange
        int expected = 20;
        //Act
        int result = Bloco3.exercicioSeisAlineaF(11499);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuandoSomaCatorzeImpares() {
        //Arrange
        int expected = 14;
        //Act
        int result = Bloco3.exercicioSeisAlineaF(277);
        //Assert
        assertEquals(expected, result);
    }

    //Testes exercício 6 alínea G
    @Test
    public void testMediaDeCentoQuarentaQuatro() {
        //Arrange
        double expected = 3;
        //Act
        double result = Bloco3.exercicioSeisAlineaG(144);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaDeSessentaMilSeiscentosSessentaSeis() {
        //Arrange
        double expected = 4.8;
        //Act
        double result = Bloco3.exercicioSeisAlineaG(60666);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Testes exercício 6 alínea H
    @Test
    public void testMediaParesUm() {
        //Arrange
        double expected = 3.4;
        //Act
        double result = Bloco3.exercicioSeisAlineaH(6566655);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaParesDois() {
        //Arrange
        double expected = 3.1;
        //Act
        double result = Bloco3.exercicioSeisAlineaH(884352617);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    //Testes exercício 6 alínea I
    @Test
    public void testMediaImparesUm() {
        //Arrange
        double expected = 1.8;
        //Act
        double result = Bloco3.exercicioSeisAlineaI(884352617);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testMediaImparesDois() {
        //Arrange
        double expected = 2.9;
        //Act
        double result = Bloco3.exercicioSeisAlineaI(6556655);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    // Testes exercício 6 alínea J
    @Test
    public void testNumeroInvertidoUm() {
        //Arrange
        int expected = 5442;
        //Act
        int result = Bloco3.exercicioSeisAlineaJ(2445);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testNumeroInvertidoDois() {
        //Arrange
        int expected = 7654321;
        //Act
        int result = Bloco3.exercicioSeisAlineaJ(1234567);
        //Assert
        assertEquals(expected, result);
    }

    //Testes exercício 7 alínea A
    @Test
    public void testQuandoCapicuaTrue() {
        //Arrange
        //Act
        boolean result = Bloco3.exercicioSeteAlineaA(122221);
        //Assert
        assertTrue(result);
    }

    @Test
    public void testQuandoCapicuaFalse() {
        //Arrange
        //Act
        boolean result = Bloco3.exercicioSeteAlineaA(123123);
        //Assert
        assertFalse(result);
    }
    @Test
    public void testQuandoCapicuaFalseII() {
        //Arrange
        //Act
        boolean result = Bloco3.exercicioSeteAlineaA(1);
        //Assert
        assertFalse(result);
    }

    //Testes exercício 7 alínea B
    @Test
    public void testNumArmstrong() {
        //Arrange
        //Act
        boolean result = Bloco3.exercicioSeteAlineaB(153);
        //Assert
        assertTrue(result);
    }

    @Test
    public void testNumNaoArmstrong() {
        //Arrange
        //Act
        boolean result = Bloco3.exercicioSeteAlineaB(388);
        //Assert
        assertFalse(result);
    }

    //Testes exercício 7 alínea C
    @Test
    public void testPrimeiraCapicuaOnze() {
        //Arrange
        int expected = 11;
        //Act
        int result = Bloco3.exercicioSeteAlineaC(10, 20);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testNaoHaCapicua() {
        //Arrange
        int expected = -1;
        //Act
        int result = Bloco3.exercicioSeteAlineaC(12, 20);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 7 alínea D
    @Test
    public void testMaiorCapicua() {
        //Arrange
        int expected = 33;
        //Act
        int result = Bloco3.exercicioSeteAlineaD(10, 33);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testNaoHaCapicuaD() {
        //Arrange
        int expected = 0; //quando não existem capicuas
        //Act
        int result = Bloco3.exercicioSeteAlineaD(0, 10);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 7 alínea E
    @Test
    public void testConteTresCapicuas() {
        //Arrange
        int expected = 3; //11,22,33 = 3;
        //Act
        int result = Bloco3.exercicioSeteAlineaE(10, 33);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testConteSeteCapicuas() {
        //Arrange
        int expected = 7; //33,44,55,66,77,88,99 = 7;
        //Act
        int result = Bloco3.exercicioSeteAlineaE(33, 100);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 7 alínea F
    @Test
    public void testRetornePrimeiroArmstrong() {
        //Arrange
        int expected = 153;
        //Act
        int result = Bloco3.exercicioSeteAlineaF(153, 371);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRetorneMenosUmArmstrong() {
        //Arrange
        int expected = -1; //Não existe nº armstrong;
        //Act
        int result = Bloco3.exercicioSeteAlineaF(90, 99);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 7 alínea G
    @Test
    public void testRetorneTresArmstrong() {
        //Arrange
        int expected = 3;
        //Act
        int result = Bloco3.exercicioSeteAlineaG(153, 371);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRetorneZeroArmstrong() {
        //Arrange
        int expected = 0; //Não existe nº armstrong
        //Act
        int result = Bloco3.exercicioSeteAlineaG(10, 15);
        //Assert
        assertEquals(expected, result);
    }

    //testes exercicio 11
    @Test
    public void testNumManeirasDiferentesCinco() {
        //Arrange
        int expected = 3;
        //Act
        int result = Bloco3.exercicioOnze(5);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testNumManeirasDiferentesDez() {
        //Arrange
        int expected = 6;
        //Act
        int result = Bloco3.exercicioOnze(10);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 12
    @Test
    public void testQuandoRaizDupla() {
        //Arrange
        String expected = "-0,33";
        //Act
        String result = Bloco3.exercicioDoze(9, 6, 1);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testQuandoNaoQuadratica() {
        //Arrange
        String expected = "Não é função quadrática";
        //Act
        String result = Bloco3.exercicioDoze(9, 0, 1);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRaizesImaginarias() {
        //Arrange
        String expected = "Resultado com raízes imaginárias";
        //Act
        String result = Bloco3.exercicioDoze(12, 4, 1);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRaizesReaisDistintas() {
        //Arrange
        String expected = "-4,14 + -0,36";
        //Act
        String result = Bloco3.exercicioDoze(2, 9, 3);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 13
    @Test
    public void testAlimentoPerecivel() {
        //Arrange
        String expected = "Alimento Perecível";
        //Act
        String result = Bloco3.exercicioTrezeParaTeste(3);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testLimpezaUtensilios() {
        //Arrange
        String expected = "Limpeza e Utensílios Domésticos";
        //Act
        String result = Bloco3.exercicioTrezeParaTeste(11);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testVestuario() {
        //Arrange
        String expected = "Vestuário";
        //Act
        String result = Bloco3.exercicioTrezeParaTeste(5);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testZeroCodigoInvalido() {
        //Arrange
        String expected = "Código Inválido";
        //Act
        String result = Bloco3.exercicioTrezeParaTeste(0);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 14 É muito parecido ao 13.
    // Testes exercicio 16
    @Test
    public void testSalarioBrutoNegativo() {
        //Arrange
        double expected = -1;
        //Act
        double result = Bloco3.exercicioDezasseis(-19);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testSalarioBrutoQuatrocentosSessenta() {
        //Arrange
        double expected = 414;
        //Act
        double result = Bloco3.exercicioDezasseis(460);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testSalarioBrutoOitocentos() {
        //Arrange
        double expected = 680;
        //Act
        double result = Bloco3.exercicioDezasseis(800);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testSalarioBrutoMilDuzentos() {
        //Arrange
        double expected = 960;
        //Act
        double result = Bloco3.exercicioDezasseis(1200);
        //Assert
        assertEquals(expected, result, 0.1);
    }

    // Testes exercicio 17
    @Test
    public void testRacaPequena() {
        //Arrange
        String expected = "É de raça pequena";
        //Act
        String result = Bloco3.exercicioDezasseteAlineaARaca(10);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRacaMedia() {
        //Arrange
        String expected = "É de raça média";
        //Act
        String result = Bloco3.exercicioDezasseteAlineaARaca(23);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRacaGrande() {
        //Arrange
        String expected = "É de raça grande";
        //Act
        String result = Bloco3.exercicioDezasseteAlineaARaca(41);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testRacaGigante() {
        //Arrange
        String expected = "É de raça gigante";
        //Act
        String result = Bloco3.exercicioDezasseteAlineaARaca(60);
        //Assert
        assertEquals(expected, result);
    }

    //Teste exercício 18 da soma ponderada.
    @Test
    public void testSomaPonderadaUm() {
        //Arrange
        int expected = 132;
        //Act
        int result = Bloco3.dezoitoObterSomaPonderada(143075233);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSomaPonderadaDois() {
        //Arrange
        int expected = 187;
        //Act
        int result = Bloco3.dezoitoObterSomaPonderada(145268667);
        //Assert
        assertEquals(expected, result);
    }

    // Testes exercício 19
    @Test
    public void testSequenciaUm() {
        //Arrange
        String expected = "91937264";
        //Act
        String result = Bloco3.obterSequenciaNumImparNumParOrganizado(47639192);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSequenciaDois() {
        //Arrange
        String expected = "513788462";
        //Act
        String result = Bloco3.obterSequenciaNumImparNumParOrganizado(726431885);
        //Assert
        assertEquals(expected, result);
    }

}

